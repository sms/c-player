
ELEMENTS="faac faac x264"

  for el in $ELEMENTS
  do
    printf "checking for $el "
    if gst-inspect-1.0 $el 2&1> /dev/null
    then
      echo "$el was found."
    else
      echo "gstreamer-plugin: $el not found, things will not work"
    fi
  done

