/*
 ** client.c -- a stream socket client demo
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <arpa/inet.h>

#define PORT "9999" // the port client will be connecting to 

#define MAXDATASIZE 1000 // max number of bytes we can get at once 

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET) {
    return &(((struct sockaddr_in*)sa)->sin_addr);
  }

  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

  int 
setNonblocking(int fd)
{
  int flags;

  /* If they have O_NONBLOCK, use the Posix way to do it */
  /* Fixme: O_NONBLOCK is defined but broken on SunOS 4.1.x and AIX 3.2.5. */
  if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
    flags = 0;
  return fcntl(fd, F_SETFL, flags | SOCK_NONBLOCK);
} 

  extern int
open_client_socket(char *host, char *port)
{
  int sockfd;  
  char buf[MAXDATASIZE];
  struct addrinfo hints, *servinfo, *p;
  int rv;
  char s[INET6_ADDRSTRLEN];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if ((rv = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  // loop through all the results and connect to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((sockfd = socket(p->ai_family, p->ai_socktype,
            p->ai_protocol)) == -1) {
      perror("client: socket");
      continue;
    }

    if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(sockfd);
      perror("client: connect");
      continue;
    }

    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to connect\n");
    return 2;
  }

  //set nonblocking
  fcntl(sockfd, F_SETFL, O_NONBLOCK);

  inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
      s, sizeof s);
  printf("client: connecting to %s\n", s);

  freeaddrinfo(servinfo); // all done with this structure

  return sockfd;
}

  void
readsock(int sock)
{
  int n, i;
  char *result;
  char buf[MAXDATASIZE];
  i=0;
  // read
  while ( i < 3 )
  {
    i++;
    n = recv(sock, buf, MAXDATASIZE-1, 0);
    printf("numbytes: %i\n ", n);
    if (n == 0)
    {
      //exit(1);
      printf("socket dissapeared");
    } else if (n < 0) {

      switch (errno){
        case EAGAIN: 
          printf("wouldblock\n ");
          break; 
        default:
          printf("error: %i\n ", errno);
          break;
      }
      } else {
        buf[n] = '\0';
        printf("client: received '%s'\n",buf);
      }
    
    sleep(1);
  }
}


  extern char *
sendcommand(int sock, char *cmd)
{
  int numbytes;
  char *result;
  char buf[MAXDATASIZE];
  printf("sending: %s", cmd);
  // write
  write(sock, cmd, sizeof(&cmd));
  sleep(1); 
  readsock(sock);
  //    sleep(1);
  //    readsock(sock);

}

int main(int argc, char *argv[])
{
  int sock;  
  //char buf[MAXDATASIZE];

  if (argc != 2) {
    fprintf(stderr,"usage: client hostname\n");
    exit(1);
  }

  sock = open_client_socket("127.0.0.1", "9999");
  sendcommand(sock, "hello1\n");
  sendcommand(sock, "hello2\n");  
  sendcommand(sock, "hello3\n");
  sendcommand(sock, "hello4\n");  


  close(sock);

  return 0;
}
