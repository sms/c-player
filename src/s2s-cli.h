/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//std libs
#include <stdio.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

// readline 
#include <readline/readline.h>
#include <readline/history.h>


//glib stuff
#include <glib.h>
#include <glib/gprintf.h>

// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>

//rtsp 
#ifdef RTSP
#include <gst/rtsp-server/rtsp-server.h>
#endif


// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>


/* global variables */
extern char *xmalloc PARAMS((size_t));

/* ---- s2s-cli ---- */

/* structure for the commands */
typedef struct{
  char *name;                   /* User printable name of the function. */
  rl_icpfunc_t *func;           /* Function to call to do the job. */
  char *doc;                    /* Documentation for this function.  */
} COMMAND;

/* main stream data structure */
struct streamMainData{
  int streamNumber;
  char *streamName;

  GstElement *videoInput;
  GstElement *audioInput;

  GstElement *pipeline;

  GstElement *videoOutput;
  GstElement *audioOutput;
} mainData[10];


/* output data structeres */
typedef struct outputRtp {
  int streamNumber;
  GstBus     *bus;
  GstElement *pipeline;
  GstElement *vsource, *asource,  *vqueue, *aqueue, *videoscale;
  GstElement *decodebin, *vencoder, *aencoder, *audioresample,  *rtph264pay, *rtpopuspay;
  GstElement *rtpbin, *vudpsink_rtp, *vudpsink_rtcp,*vudpsrc_rtcp, *audpsink_rtp, *audpsink_rtcp,*audpsrc_rtcp;
  GMainLoop  *loop;
} outputRtp;

typedef struct outputRtmp {
  int streamNumber;
  GstBus *bus;
  GstElement *pipeline;
  GstElement *vsource, *asource,  *vqueue, *aqueue, *videoscale;
  GstElement *decodebin, *vencoder, *aencoder, *audioresample, *flvmux, *rtmpsink;
  GMainLoop *loop;
} outputRtmp;


/* input data structures */
typedef struct inputRtp {

  int streamNumber;

  int baseport;
  GstElement *bin;
  GstElement *pipeline, *vudpsrc_rtp, *vudpsrc_rtcp, *vudpsink_rtcp, *audpsrc_rtp, *audpsrc_rtcp, *audpsink_rtcp;
  GstElement *rtpbin, *rtph264depay, *rtpopusdepay, *queue1, *queue2, *vdecoder, *adecoder, *vsink, *asink;
  GstElement *vrtpqueue, *vrtcpqueue, *artpqueue, *artcpqueue, *vrtptee, *vrtcptee, *artptee, *artcptee, 
	     *vrtpMulticastSink, *vrtcpMulticastSink, *artpMulticastSink, *artcpMulticastSink;
//  GMainLoop  *loop;
  GstPad* srcpad;
  GstCaps *vsrccaps, *asrccaps;

  struct rtpbinRecieveData *next;

} inputRtp;


/* structure for input names, the function to call if selected, and a discription */
typedef struct{
  char *name;                   /* User printable name of the function. */
  rl_icpfunc_t *func;           /* Function to call to do the job. */
  char *doc;                    /* Documentation for this function.  */
} INPUT;

/* input types and functions array*/
int input_set_test();
int input_set_vtest();
int input_set_atest();
int input_set_v4l();
int input_set_gphoto();
int input_set_ipcam();
int input_set_alsa();
int input_set_jack();
int input_set_rtp();
int input_set_vudp();
int input_set_audp();
int input_set_dvgrab();
int input_set_uri();

static INPUT inputs[] = {
  { "test", input_set_test, "Set video and audio input to test source" },
  { "vtest", input_set_vtest, "Set video input to test src"},
  { "atest", input_set_atest, "Set audio input to test src"},
  { "v4l", input_set_v4l, "Set video input to v4l"},
  { "gphoto", input_set_gphoto, "Set video input to gphoto"},
  { "ipcam", input_set_ipcam, "Set video input to ip camera (http soup source)"},
  { "alsa", input_set_alsa, "Set audio input to alsa"},
  { "jack", input_set_jack, "Set audio input to jack"},
  { "rtp", input_set_rtp, "Set video and audio input to rtp"},
  { "v-udp", input_set_vudp, "Set video input to udp NOT IMPLEMENTED"},
  { "a-udp", input_set_audp, "Set audio input to udp NOT IMPLEMENTED"},
  { "dvgrab", input_set_dvgrab, "Set input to dvgrab NOT IMPLEMENTED"},
  { "uri", input_set_uri, "Set input to uri"}
};

/* structure for output names, the function to call if selected, and a discription */
typedef struct{
  char *name;                   /* User printable name of the function. */
  rl_icpfunc_t *func;           /* Function to call to do the job. */
  char *doc;                    /* Documentation for this function.  */
} OUTPUT;

/* output types and functions */
int output_set_screen();
int output_set_rtp();
int output_set_snowmix();
int output_set_alsa();
int output_set_rtmp();
#ifdef RTSP
int output_set_rtsp();
#endif 
int output_set_jack();
int output_set_icecast();

static OUTPUT outputs[] = {
  { "screen", output_set_screen, "Sets output to imagesink" },
  { "rtp", output_set_rtp, "Sets output to rtp" },
  { "snowmix", output_set_snowmix, "Sets output to snowmix" },
  { "alsa", output_set_alsa, "Sets output to alsa" },
  { "rtmp", output_set_rtmp, "Sets output to rtmp" },
#ifdef RTSP
  { "rtsp", output_set_rtsp, "Creates an rtsp server as an output"},
#endif
  { "jack", output_set_jack, "Sets output to jack"},
  { "icecast", output_set_icecast, "Sets output to icecast"}
};

/* shell commands and functions array*/
int stream_add PARAMS((char *));
int stream_remove PARAMS((char *));
int stream_start PARAMS((char *));
int stream_stop PARAMS((char *));
int stream_status PARAMS((char *));
int stream_set_input PARAMS((char *));
int stream_set_output PARAMS((char *));
int help();
int quit();

static COMMAND commands[] = {
  { "addStream", stream_add, "Create a new stream" },
  { "removeStream", stream_remove, "Remove existing stream"},
  { "startStrean", stream_start, "Start a stream"},
  { "stopStream", stream_stop, "Stop a stream"},
  { "status", stream_status, "Outputs status of one or more streams"},
  { "setInput",	stream_set_input, "Set the input of a stream"},
  { "setOutput", stream_set_output, "Set the output of a stream"},
  { "help", help, "Shows available commands"},
  { "quit", quit, "Exit"}
};


