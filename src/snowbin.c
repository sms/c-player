/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef SNOWBIN
#ifdef CLI
#include "s2s-cli.h"
#endif

#include "common.h"

// for snowaudiosink
#define MAXDATASIZE 1000 // max number of bytes we can get at once 

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET) {
    return &(((struct sockaddr_in*)sa)->sin_addr);
  }

  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


  static int
open_client_socket(char *host, char *port)
{
  int sockfd;  
  char buf[MAXDATASIZE];
  struct addrinfo hints, *servinfo, *p;
  int rv;
  char s[INET6_ADDRSTRLEN];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if ((rv = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  // loop through all the results and connect to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((sockfd = socket(p->ai_family, p->ai_socktype,
            p->ai_protocol)) == -1) {
      perror("client: socket");
      continue;
    }

    if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(sockfd);
      perror("client: connect");
      continue;
    }

    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to connect\n");
    return 2;
  }

  //set nonblocking
  //  fcntl(sockfd, F_SETFL, O_NONBLOCK);

  inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
      s, sizeof s);
  printf("client: connecting to %s\n", s);

  freeaddrinfo(servinfo); // all done with this structure

  return sockfd;
}


  int
get_vars (snowMixer *mixer, snowFeed *feed, PlayerData *data)
{
  FILE *fp;
  char line[1035];
  regex_t regex;
  int retsnow;
  char msgbuf[100];
  size_t     nmatch = 3;
  regmatch_t pmatch[3];
  char key[100], value[100];
  char getvars[100];
  /* Compile regular expression */
  //  retsnow = regcomp(&regex, "^Snowmix[[:print:]]*", 0);
  //  retsnow = regcomp(&regex, "^[_[:alnum:]]*[[:punct:]][[:blank:]][[:alnum:]]*", REG_EXTENDED);
  retsnow = regcomp(&regex, "^\([_[:alnum:]]*\)[[:punct:]][[:blank:]]\([[:print:]]*\)", REG_EXTENDED);


  if (retsnow) {
    fprintf(stderr, "Could not compile regex\n");
    exit(1);
  }

  /* Open the command for reading. */
  snprintf(getvars, sizeof(getvars), "./getvars.sh %u", data->feedid);
  printf(">>>>> executing: %s \n ", getvars);
  fp = popen(getvars, "r");
  if (fp == NULL) {
    printf("Failed to run command\n" );
    exit(1);
  }

  while (fgets(line, sizeof(line)-1, fp) != NULL) {

    retsnow = regexec(&regex, line, nmatch, pmatch, 0);


    if (!retsnow) {
      //puts("Match");
      //      printf("%s m1: %s m2: %s m3: %s", line, pmatch[0], pmatch[1], pmatch[2]);
      int n = 0;

      /* 
         printf("With the whole expression, "
         "a matched substring \"%.*s\" is found at position %d to %d.\n",
         pmatch[n].rm_eo - pmatch[n].rm_so, &line[pmatch[n].rm_so],
         pmatch[n].rm_so, pmatch[n].rm_eo - 1);
         */  
      n = 1;
      //      snprintf(key, sizeof(&key), "%.*s",pmatch[n].rm_eo - pmatch[n].rm_so, &line[pmatch[n].rm_so],pmatch[n].rm_so, pmatch[n].rm_eo - 1);
      //                                      |             lenght               |    start?             |
      snprintf(key, sizeof(key), "%.*s",pmatch[n].rm_eo - pmatch[n].rm_so, &line[pmatch[n].rm_so]);


      n = 2;
      //      snprintf(value, sizeof(&value), "%.*s",pmatch[n].rm_eo - pmatch[n].rm_so, &line[pmatch[n].rm_so],pmatch[n].rm_so, pmatch[n].rm_eo - 1);

      snprintf(value, sizeof(value), "%.*s",pmatch[n].rm_eo - pmatch[n].rm_so, &line[pmatch[n].rm_so]);
      //      printf("keyvalue:  %s %s  \n ", key, value);

      switchs(key){
        cases("host")
          mixer->host = strdup(value);
        break;
        cases("port")
          mixer->port = strdup(value);
        break;
        cases("feed_id")
          feed->id = atoi(value);
        break;
        cases("feed_rate")
          feed->audio_rate = atoi(value);
        break;
        cases("feed_width")
          feed->width = atoi(value);
        break;
        cases("feed_height")
          feed->height = atoi(value);
        break;
        cases("feed_control_pipe")
          feed->control_pipe = strdup(value);
        break;
        cases("ctrsocket")
          //  strncpy(mixer->ctrlsocket, value, 100);
          mixer->control_socket = strdup(value);
        break;
        cases("system_width")
          mixer->width = atoi(value);
        break;
        cases("system_height")
          mixer->height = atoi(value);
        break;
        cases("ratefraction")
          //          strncpy(mixer->ratefraction, value, 100);
          mixer->ratefraction = strdup(value);
        break;
        cases("snowmix")
          //          strncpy(mixer->state, value, 100);
          mixer->state = strdup(value);
        break;
        cases("channels")
          mixer->audio_channels = atoi(value);
        break;
        cases("feed_channels")
          feed->audio_channels = atoi(value);
        break;
        cases("rate")
          mixer->audio_rate = atoi(value);
        break;
        defaults
          printf("FIX ME: no match for key of %s=%s \n", key, value);
        break;

      } switchs_end;
      //      printf("%u", __done); 


    }
    else if (retsnow == REG_NOMATCH) {
      //puts("No match");
      printf("FIX ME: no match for line: %s", line);
    }
    else {
      regerror(retsnow, &regex, msgbuf, sizeof(msgbuf));
      fprintf(stderr, "Regex match failed: %s\n", msgbuf);
      exit(1);
    }

  }

  /* Free compiled regular expression if you want to use the regex_t again */
  regfree(&regex);
  /* close */
  pclose(fp);

  return 0;
}




  extern void
print_snowMixer (snowMixer *m)
{
  printf("\n \n snowMixer: \n host %s \n port %s \n control_socket %s \n state %s \n width %u \n height %u \n ratefraction %s \n audio_rate %u \n audio_channels %u \n",
      m->host,
      m->port,
      m->control_socket,
      m->state,
      m->width,
      m->height,
      m->ratefraction,
      m->audio_rate,
      m->audio_channels);
}
  extern void
print_snowFeed (snowFeed *f)
{
  printf("\n \n snowFeed \nid %u \n control_pipe %s \n width %u \n height %u \n audio_channels %u \n audio_rate %u \n ",
      f->id,
      f->control_pipe,
      f->width,
      f->height,
      f->audio_channels,
      f->audio_rate);
}



  extern GstElement *
get_snowvideobin (snowData *sd)
{
  GstElement *videorate, *videoconvert, *capsfilter,
             *videoscale, *shmsink, *queue;
  GstPad *vpad, *vghostpad;
  GstElement *snowvideobin;
  char *feedname = "feed1";
  snowFeed *feed;
  //snowMixer *mixer;
  feed = sd->currentFeed;
  //mixer = sd->mixer;
  printf("======= get snowvideobin ===============");
  print_snowFeed(feed);
  snowvideobin = gst_bin_new ("Snowvideobin");

  videorate = gst_element_factory_make ("videorate", "videorate");
#ifdef GST1
  videoconvert = gst_element_factory_make ("videoconvert", "videoconvert");

#else
  videoconvert =
    gst_element_factory_make ("ffmpegcolorspace", "videoconvert");

#endif

  videoscale = gst_element_factory_make ("videoscale", "videoscale");
  queue = gst_element_factory_make ("queue", "queue");


#ifdef GST1
  GstCaps *caps = gst_caps_new_simple ("video/x-raw",
      "format", G_TYPE_STRING, "BGRA",
      //"interlace-mode", G_TYPE_STRING, "progressive",
      "width", G_TYPE_INT, feed->width,
      "height", G_TYPE_INT, feed->height,
      //"pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
      "framerate", GST_TYPE_FRACTION, 25, 1,
      NULL);
#else

  GstCaps *caps = gst_caps_new_simple ("video/x-raw-rgb",
      "bpp", G_TYPE_INT, 32,
      "depth", G_TYPE_INT, 32,
      "endianness", G_TYPE_INT, 4321,
      "red_mask", G_TYPE_INT, 65280,
      "green_mask", G_TYPE_INT, 16711680,
      "blue_mask", G_TYPE_INT, -16777216,
      "interlaced", G_TYPE_BOOLEAN, FALSE,
      "width", G_TYPE_INT, feed->width,
      "height", G_TYPE_INT, feed->height,
      "pixel-aspect-ratio",
      GST_TYPE_FRACTION, 4, 3,
      "framerate", GST_TYPE_FRACTION, 25, 1,
      NULL);
#endif

  capsfilter = gst_element_factory_make ("capsfilter", "capsfilter");

  g_object_set (videoscale, "add-borders", TRUE, NULL);
  g_object_set (G_OBJECT (capsfilter), "caps", caps, NULL);

  shmsink = gst_element_factory_make ("shmsink", "shmsink");

  remove (feed->control_pipe);

  g_object_set (shmsink, "socket-path", feed->control_pipe, NULL);
#ifndef GST1 //gst0.10
  g_object_set (shmsink, "shm-size", "23224320", NULL);

#endif
  g_object_set (shmsink, "wait-for-connection", FALSE, NULL);
#ifdef GST1
  /* add and link elements in the bin */
  gst_bin_add_many (GST_BIN (snowvideobin), queue, videorate, videoscale,
      videoconvert, capsfilter, shmsink, NULL);

  if( !gst_element_link_many (queue, videorate, videoscale, videoconvert,capsfilter, shmsink, NULL)) {printf("linking snowbin failed");}

#else //gst0.10
  gst_bin_add_many (GST_BIN (snowvideobin), queue, videorate, videoscale,
      videoconvert, capsfilter, shmsink, NULL);

  gst_element_link_many (queue, videorate, videoscale, videoconvert,
      capsfilter, shmsink, NULL);
#endif

  vpad = gst_element_get_static_pad (queue, "sink");
  vghostpad = gst_ghost_pad_new ("sink", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (snowvideobin, vghostpad);

  gst_object_unref (vpad);

  return snowvideobin;
}

  extern GstElement *
get_jacksink (int feedid)
{
  GstElement *js;
  char *feedname = "feedfuck";
  js = gst_element_factory_make ("jackaudiosink", "jackaudiosink");

  /* check for jackportpattern */
  if (g_object_class_find_property
      (G_OBJECT_GET_CLASS (js), "port-pattern"))
  {

    char *portpattern = "feedfuck/audio_in";
    asprintf (&portpattern, "feed%u/audio_in", feedid);
    asprintf (&feedname, "feed%u", feedid);
    g_print ("snowaudiobin: feed: %u portpattern is: : %s : \n", feedid, portpattern);

    g_object_set (G_OBJECT (js), "port-pattern", portpattern,
        NULL);
  }
  g_object_set (G_OBJECT (js), "client-name", feedname,
      NULL);
  return js;
}

  extern GstElement *
get_snowaudiosink (int feedid)
{
  int socket = 1;
  GstElement *fdsink;
  char *feedname = "feedfuck";
  char *startaudio = "audio feed ctr isaudio 1 \n asdfasdfasdfsadfsadfasdfsadf";
  asprintf (&feedname, "feed%u", feedid);
  asprintf (&startaudio, "audio feed ctr isaudio %u \n", feedid);

//  printf("creating snowaudiosink \n \n \n "); 
  // file descriptor sink
  socket = open_client_socket("localhost", "9999" );

  if (socket == 1)
  {
    printf("Connection failed");
    exit(10);
  }

  // tell snowmix to start accepting audio
  write(socket, startaudio, strlen(startaudio));

  fdsink     = gst_element_factory_make("fdsink","fdsink");

  g_object_set( G_OBJECT (fdsink), "fd", socket, NULL);

  free(feedname);
  free(startaudio);
  return fdsink;
}


  extern int
snowbin_init_snowData (PlayerData *data) 
{

  struct snowData *sd;
  struct snowMixer *mixer;
  struct snowFeed *feed;
  sd = (snowData*)malloc(sizeof(struct snowData));
  mixer = (snowMixer*)malloc(sizeof(struct snowMixer));
  mixer->host = NULL; //host
  mixer->port = NULL; //port
  mixer->control_socket = NULL; //ctrsocket
  mixer->state = NULL; //snowmix
  mixer->ratefraction = NULL; //ratefraction
  mixer->width = 0;
  mixer->height = 0;
  feed = (snowFeed*)malloc(sizeof(struct snowFeed));
  feed->id = 0;
  feed->control_pipe = NULL; //feed_control_pipe
  feed->width = 0;
  feed->height = 0;
//  get_vars(mixer, feed, data);

  // set audiosink
  sd->audiosink = SNOWMIX;
  //  print_snowMixer(snow->mixer);
  //  print_snowFeed(snow->currentFeed);

  sd->currentFeed = feed;
  sd->mixer = mixer;
  data->snow = sd;
  return 0;
}

extern void
snowbin_get_vars(PlayerData *data)
{
  struct snowMixer *mixer;
  struct snowFeed *feed;
  mixer = data->snow->mixer;
  feed = data->snow->currentFeed;
  get_vars(mixer, feed, data);
}

  extern GstElement *
get_snowaudiobin (snowData *sd)
{
  GstElement *audiosink, *audioconvert, *capsfilter;
  GstPad *apad, *aghostpad;
  GstElement *snowaudiobin;
  char *feedname = "feedfuck";
  asprintf (&feedname, "feed%u", sd->currentFeed->id);
  //common
  snowaudiobin = gst_bin_new ("snowaudiobin");
  audioconvert = gst_element_factory_make ("audioconvert", "audioconvert");

  printf("========snowaudiobin thinks audiosink is: %u  \n", sd->audiosink);

  if (sd->audiosink == JACK)
  {

    printf(">>>> JACK \n ");
    //jack
    audiosink = get_jacksink(sd->currentFeed->id);
    if (audiosink == NULL) { e_exit(" could not create element jacksink "); }
    gst_bin_add_many (GST_BIN (snowaudiobin), audioconvert, audiosink,
        NULL);
    gst_element_link_many (audioconvert, audiosink, NULL);


  } else if (sd->audiosink == SNOWMIX)
  {
    // snow
    printf ("SNOW");
    audiosink = get_snowaudiosink(sd->currentFeed->id);
    if (audiosink == NULL) { e_exit(" could not create element snowaudiosink "); }

    GstCaps *caps = gst_caps_new_simple ("audio/x-raw",
        "rate", G_TYPE_INT, 4800,
        "channels", G_TYPE_INT, 2,
        "format", G_TYPE_STRING, "S16LE",
        NULL);

    capsfilter = gst_element_factory_make ("capsfilter", "capsfilter123");
    g_object_set (G_OBJECT (capsfilter), "caps", caps, NULL);


    gst_bin_add_many (GST_BIN (snowaudiobin), audioconvert, capsfilter, audiosink,
        NULL);
    gst_element_link_many (audioconvert, capsfilter, audiosink, NULL);
  } else if (sd->audiosink == FAKE)
  {
    // fake
    audiosink = gst_element_factory_make ("fakesink", "fakesink123");

    gst_bin_add_many (GST_BIN (snowaudiobin), audioconvert, audiosink,
        NULL);
    gst_element_link_many (audioconvert, audiosink, NULL);
  } else if (sd->audiosink == AUTO)
  {
    // auto
    audiosink = gst_element_factory_make ("autoaudiosink", "autoaudiosink123");

    gst_bin_add_many (GST_BIN (snowaudiobin), audioconvert, audiosink,
        NULL);
    gst_element_link_many (audioconvert, audiosink, NULL);
  }


  // common
  apad = gst_element_get_static_pad (audioconvert, "sink");
  aghostpad = gst_ghost_pad_new ("sink", apad);

  gst_pad_set_active (aghostpad, TRUE);
  gst_element_add_pad (snowaudiobin, aghostpad);

  gst_object_unref (apad);

  return snowaudiobin;

}
#endif
