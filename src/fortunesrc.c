/* s2s-input.c inputs for s2s
 * 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>

//std libs
#include <stdio.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

//#include "cli-common.h"

extern GstElement * 
get_fortune_src()
{
  GstElement *fortunebin = gst_bin_new("FortuneBin");
  GstElement *videotestsrc = gst_element_factory_make("videotestsrc", "videotestsrc");
  GstElement *textoverlay = gst_element_factory_make("textoverlay", "textoverlay");
  char text[5000];
  int patterns[] = {0, 1, 10, 11, 12, 13, 18, 20, 21, 22};
  int rndPattern;
  int rndSpeed;

  // create random numbers
  time_t seconds;
  time(&seconds);
  srand((unsigned int) seconds);
  rndPattern = rand() % 10 + 0;

  time(&seconds);
  srand((unsigned int) seconds);
  rndSpeed = rand() % 25 + 0;

  // get text from fortune 
  FILE *fp;
  char path[1035];
  fp = popen("/usr/games/fortune", "r");
  if (fp == NULL) 
  {
    printf("Failed to run command\n" );
    exit(1);
  }

  while (fgets(path, sizeof(path)-1, fp) != NULL) 
  {
    strncat(text, path, 50000);
  }

  pclose(fp);

  // set element properties 
  g_object_set(G_OBJECT(videotestsrc), "pattern", patterns[rndPattern], NULL);
  g_object_set(G_OBJECT(videotestsrc), "horizontal-speed", rndSpeed, NULL);
  g_object_set(G_OBJECT(videotestsrc), "is-live", TRUE, NULL);
  g_object_set(G_OBJECT(textoverlay), "text", text, NULL);
  g_object_set(G_OBJECT(textoverlay), "valignment", 4, NULL);
  // add elements to bin and link them 
  gst_bin_add_many(GST_BIN(fortunebin), videotestsrc, textoverlay, NULL);
  gst_element_link(videotestsrc, textoverlay); 

  // create pads for the bin and return the bin 
  GstPad *vpad, *vghostpad;
  vpad = gst_element_get_static_pad (textoverlay, "src");
  vghostpad = gst_ghost_pad_new ("src", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (fortunebin, vghostpad);

  //mainData[numStreams].videoInput = fortunebin;

  return fortunebin;
  

}


