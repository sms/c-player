/* bigscreen, a tool to stream the output of you monitor to another computer 
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* options:
 * -s send mode 
 * -r revieving mode
 * -p baseport
 */


#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

//glib stuff
#include <glib.h>
#include <glib/gprintf.h>

// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>


#define DEFAULT_LATENCY_MS              50

typedef struct rtpbinSendData {
  GstElement *pipeline;
  GstElement *vsource, *asource,  *vqueue, *aqueue, *videoscale, *videoconvert;
  GstElement *decodebin, *vencoder, *aencoder, *audioresample,  *rtph264pay, *rtpopuspay;
  GstElement *rtpbin, *vudpsink_rtp, *vudpsink_rtcp,*vudpsrc_rtcp, *audpsink_rtp, *audpsink_rtcp,*audpsrc_rtcp;
  GMainLoop  *loop;
} rtpbinSendData; 
  
typedef struct globalData {
  GstElement *videoInput;
  GstElement *audioInput;
} globalData;


static gboolean
bus_call( GstBus *bus,GstMessage *msg, gpointer data )
{
  GMainLoop *loop = ( GMainLoop* )data;
  switch( GST_MESSAGE_TYPE( msg )) {
   case GST_MESSAGE_EOS:
     g_print ( "End-of-stream\n" );
     g_main_loop_quit( loop );
     break;
   case GST_MESSAGE_ERROR: {
     gchar *debug;
     GError* err;
     gst_message_parse_error( msg,&err,&debug );
     g_print ( "Error: %s\n", err ->message );
     g_error_free( err );
     g_main_loop_quit ( loop );
     break;
     }
   default:
    break;
   }
  return TRUE;
}

static void 
on_pad_added ( GstElement *element, GstPad *pad, gpointer data )
{
  GstPad *sinkpad;
  GstElement* elem = ( GstElement* ) data;
  g_print ( "Dynamic pad created, linking parser/decoder\n" );
  sinkpad = gst_element_get_static_pad( elem , "sink" );
  gst_pad_link ( pad, sinkpad );
  gst_object_unref( sinkpad );
}

static void 
new_decoded_pad (GstElement *element , GstPad * pad,  rtpbinSendData * data)
{
  GstCaps *caps;
  gchar *str;
  caps = gst_pad_get_current_caps(pad);
  str = gst_caps_to_string(caps);
  if (g_str_has_prefix(str,"video/")){
    printf("new decoded video pad with caps %s\n", str);

    GstPad *sinkpad;
    sinkpad = gst_element_get_static_pad( data->vqueue , "sink" );
    gst_pad_link ( pad, sinkpad );
    gst_object_unref( sinkpad );

  }
  
  if (g_str_has_prefix(str,"audio/")){
    printf("new decoded audio pad with caps %s\n", str);
    GstPad *sinkpad;
    sinkpad = gst_element_get_static_pad( data->aqueue , "sink" );
    gst_pad_link ( pad, sinkpad );
    gst_object_unref( sinkpad );
 }
}

static int 
rtpbinTransmitter()
{
  printf("starting transmisson to the bigscreen! \n");
  gboolean ret =  FALSE;

  rtpbinSendData data; 

  gst_init ( NULL, NULL );
  data.loop = g_main_loop_new( NULL,FALSE );
  
  data.pipeline      = gst_pipeline_new( "video-send" );
  data.vsource       = gst_element_factory_make ("ximagesrc", "videotestsrc");
  data.asource	     = gst_element_factory_make ("audiotestsrc", "audiotestsrc");
  data.videoscale    = gst_element_factory_make ("videoscale", "videoscale");
  data.videoconvert  = gst_element_factory_make ("videoconvert", "videoconvert");
//  data.decodebin     = gst_element_factory_make ( "decodebin", "decodebin" );
  data.vqueue        = gst_element_factory_make ( "queue", "vqueue" );
  data.aqueue        = gst_element_factory_make ( "queue", "aqueue" );
  data.vencoder      = gst_element_factory_make ( "x264enc", "vencoder" );
  data.audioresample = gst_element_factory_make ( "audioresample", "audioresample");
  data.aencoder      = gst_element_factory_make ( "opusenc", "aencoder" );
  data.rtph264pay    = gst_element_factory_make ( "rtph264pay", "rtph264pay" );
  data.rtpopuspay    = gst_element_factory_make ( "rtpopuspay", "rtpopuspay" );
  data.rtpbin	= gst_element_factory_make ( "rtpbin", "rtpbin" );
  data.vudpsink_rtp   = gst_element_factory_make ( "udpsink", "vudpsink-send data" );
  data.vudpsink_rtcp  = gst_element_factory_make ( "udpsink", "vudpsink-rtcp" );
  data.vudpsrc_rtcp	  = gst_element_factory_make ( "udpsrc", "vudpsrc-rtcp" );
  data.audpsink_rtp   = gst_element_factory_make ( "udpsink", "audpsink-send data" );
  data.audpsink_rtcp  = gst_element_factory_make ( "udpsink", "audpsink-rtcp" );
  data.audpsrc_rtcp	  = gst_element_factory_make ( "udpsrc", "audpsrc-rtcp" );
  
  if( !data.pipeline || !data.vqueue || !data.aqueue || !data.audioresample || !data.videoscale || !data.videoconvert ||
      !data.vencoder ||  !data.aencoder || !data.rtph264pay || !data.rtpopuspay || !data.rtpbin || 
      !data.vudpsink_rtp || !data.vudpsink_rtcp || !data.vudpsrc_rtcp ||
      !data.audpsink_rtp || !data.audpsink_rtcp || !data.vudpsrc_rtcp ) { 
	g_print ( "One element could not be created \n" );
  	return -1;
  }
  
//  g_object_set( G_OBJECT ( data.source ), "location", "/home/yids/src/cpu/testclips/sound_in_sync_test.mp4", NULL);
// g_object_set( G_OBJECT ( data.source ), "location", "/home/yids/Downloads/Better.Call.Saul.S01E05.HDTV.x264-LOL[ettv]/better.call.saul.105.hdtv-lol.mp4", NULL);




  g_object_set( G_OBJECT ( data.vencoder ), "bitrate",500 , NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "speed-preset", 2, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "tune",0x00000004 , NULL);
 
  g_object_set( G_OBJECT ( data.vudpsink_rtp ), "host", "127.0.0.1", NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtp ), "port", 10000, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtcp ), "host","127.0.0.1", NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtcp ), "port", 10001, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtcp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.vudpsrc_rtcp ), "port", 10002, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtp ), "host", "127.0.0.1", NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtp ), "port", 10003, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtcp ), "host","127.0.0.1", NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtcp ), "port", 10004, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtcp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.audpsrc_rtcp ), "port", 10005, NULL ); 

  //gst_bus_add_watch( gst_pipeline_get_bus( GST_PIPELINE( pipeline ) ),
  		//bus_call,loop );
  gst_bin_add_many( GST_BIN( data.pipeline ), data.vsource, data.asource, data.vqueue, data.aqueue, data.videoconvert, 
  	data.vencoder, data.aencoder, data.audioresample, data.rtph264pay, data.rtpopuspay, data.rtpbin, 
        data.vudpsink_rtp, data.vudpsink_rtcp, data.vudpsrc_rtcp,      
        data.audpsink_rtp, data.audpsink_rtcp, data.audpsrc_rtcp, NULL);
//  ret = gst_element_link_many( source, ffmpegcs, queue, tee, NULL );
//  ret = gst_element_link_many( tee,  queuesave, /*decoder,*/ffmpegcsd, sink, NULL );
 
//  gst_bin_add_many( GST_BIN( pipeline ), rtph264pay,rtpbin,udpsink_rtp, udpsink_rtcp, udpsrc_rtcp, NULL );
//  if ( !gst_element_link_many( data.vsource, data.vencoder, NULL )) {printf("failed to link  source to decodebin\n");}
//  g_signal_connect( data.decodebin, "pad-added", G_CALLBACK( new_decoded_pad ), &data );
  if ( !gst_element_link_many (data.vsource, data.vqueue, data.videoconvert, data.vencoder, data.rtph264pay, NULL)) {printf("failed to link encoder to h264 payloader\n");}
  if ( !gst_element_link_pads_filtered( data.rtph264pay, "src", data.rtpbin, "send_rtp_sink_0", NULL )) {printf("failed to link payloader to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtp_src_0", data.vudpsink_rtp, "sink", NULL )) {printf("failed to link rtpbin to rtp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtcp_src_0", data.vudpsink_rtcp,"sink", NULL )) {printf("failed to link rtpbin to rtcp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.vudpsrc_rtcp, "src", data.rtpbin, "recv_rtcp_sink_0", NULL )) {printf("failed to link rtcp udpsrc to rtpbin\n");}
  
//  g_signal_connect( decodebin, "pad-added", G_CALLBACK( new_decoded_pad ), aencoder  );
  if ( !gst_element_link_many (data.asource, data.audioresample ,data.aencoder, data.rtpopuspay, NULL)) {printf("failed to link encoder to opus payloader");} 
  if ( !gst_element_link_pads_filtered( data.rtpopuspay, "src", data.rtpbin, "send_rtp_sink_1", NULL )) {printf("failed to link payloader to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtp_src_1", data.audpsink_rtp, "sink", NULL )) {printf("failed to link rtpbin to rtp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtcp_src_1", data.audpsink_rtcp,"sink", NULL )) {printf("failed to link rtpbin to rtcp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.audpsrc_rtcp, "src", data.rtpbin, "recv_rtcp_sink_1", NULL )) {printf("failed to link rtcp udpsrc to rtpbin\n");}
 
  g_print ( "Setting to sending data\n" );
  gst_element_set_state ( data.pipeline, GST_STATE_PLAYING );
  gst_element_set_locked_state( data.vudpsink_rtcp, TRUE );
  
  gchar *dump_name = g_strconcat ("gst-launch.", "lala" , NULL);
  GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (data.pipeline), GST_DEBUG_GRAPH_SHOW_ALL, dump_name); 
  
  g_main_loop_run( data.loop );
  
  
  
  g_print ( "returned ,stopping playback\n" );
  gst_element_set_state( data.pipeline, GST_STATE_NULL );
  
  g_print ( "Deleting pipeline\n" );
  gst_object_unref( GST_OBJECT ( data.pipeline ) );
  
  return 0;  
}


static int 
rtpbinReciever()
{
  
  GstElement *pipeline, *vudpsrc_rtp, *vudpsrc_rtcp, *vudpsink_rtcp, *audpsrc_rtp, *audpsrc_rtcp, *audpsink_rtcp;
  GstElement *rtpbin, *rtph264depay, *rtpopusdepay, *queue1, *queue2, *vdecoder, *adecoder, *vsink, *asink;
  GMainLoop  *loop;
  gboolean ret =  FALSE;
  GstPad* srcpad;
  GstCaps *vsrccaps, *asrccaps;
  gchar* name;
  char buff[1024];
  FILE* fp = NULL;
  int len = 0;
  
  gchar *vcaps = "application/x-rtp,media=(string)video,clock-rate=(int)90000,encoding-name=(string)H264"; 
  gchar *acaps = "application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00"; 
  
  loop = g_main_loop_new( NULL,FALSE );
  
  pipeline      = gst_pipeline_new( "video-recv" );//dshowvideosrc
  vudpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp video" );
  vudpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp video" );
  vudpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp video " );
  audpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp audio" );
  audpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp audio" );
  audpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp audio " );
  rtpbin        = gst_element_factory_make ( "rtpbin", "gstrtpbin" );
  rtph264depay  = gst_element_factory_make ( "rtph264depay", "rtph264depay" );
  rtpopusdepay  = gst_element_factory_make ( "rtpopusdepay", "rtpopusdepay" );
  queue1        = gst_element_factory_make ( "queue", "queue1" );
  queue2        = gst_element_factory_make ( "queue", "queue2" );
  vdecoder      = gst_element_factory_make ( "avdec_h264", "vdecoder" );
  adecoder	= gst_element_factory_make ( "opusdec", "adecoder" );
  vsink	        = gst_element_factory_make ( "xvimagesink", "xvimagesink" );//directdrawsink
  asink		= gst_element_factory_make ( "alsasink", "alsasink" );

  
  
  if( !pipeline || !vudpsrc_rtp || !vudpsrc_rtcp || !vudpsink_rtcp ||
      !audpsrc_rtp || !audpsrc_rtcp || !audpsink_rtcp || !rtpbin || 
      !rtph264depay || !rtpopusdepay || !queue1 || !queue2 || !vdecoder ||  
      !adecoder || !vsink || !asink  ) {
  	g_print ( "One element could not be created \n" );
  	return -1;
  }

  g_object_set( G_OBJECT ( vudpsrc_rtp ),   "port", 10000, NULL );
  g_object_set( G_OBJECT ( vudpsrc_rtcp ),  "port", 10001, NULL );
  g_object_set( G_OBJECT ( vudpsink_rtcp ), "host", "127.0.0.1", NULL );
  g_object_set( G_OBJECT ( vudpsink_rtcp ), "port", 10002, NULL );

  g_object_set( G_OBJECT ( audpsrc_rtp ),   "port", 10003, NULL );
  g_object_set( G_OBJECT ( audpsrc_rtcp ),  "port", 10004, NULL );
  g_object_set( G_OBJECT ( audpsink_rtcp ), "host", "127.0.0.1", NULL );
  g_object_set( G_OBJECT ( audpsink_rtcp ), "port", 10005, NULL );
 
//  g_object_set( G_OBJECT ( rtpbin ), "latency", 400, NULL );
  
//  srcpad = gst_element_get_static_pad( udpsrc_rtp, "src" );
//  srccaps = gst_pad_get_caps( srcpad );
//  name = gst_caps_to_string( srccaps );
/*
  fp = fopen( "video.caps", "rb" );
  if( !fp ) {
  	printf( "can't open video.caps\n" );
  	return -1;
  }
  
  len = fread( buff, sizeof( char ), sizeof( buff ), fp );
  buff[0x108] = '\0';
*/
  vsrccaps = gst_caps_from_string( vcaps );
  printf("video caps: %s\n", vcaps);
  g_object_set( G_OBJECT ( vudpsrc_rtp ), "caps", GST_CAPS(vsrccaps) , NULL );
  
  asrccaps = gst_caps_from_string( acaps );
  printf("audio caps: %s\n", acaps);
  g_object_set( G_OBJECT ( audpsrc_rtp ), "caps", GST_CAPS(asrccaps) , NULL );
  
  gst_bin_add_many( GST_BIN( pipeline ), vudpsrc_rtp, vudpsrc_rtcp, vudpsink_rtcp,
					 audpsrc_rtp, audpsrc_rtcp, audpsink_rtcp,
				         rtpbin, rtph264depay, rtpopusdepay, queue1, 
					 queue2, vdecoder, adecoder, vsink, asink, NULL);

  if ( !gst_element_link_pads_filtered( vudpsrc_rtp, "src", rtpbin, "recv_rtp_sink_%u", NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( vudpsrc_rtcp, "src", rtpbin, "recv_rtcp_sink_%u", NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( rtpbin, "send_rtcp_src_%u", vudpsink_rtcp, "sink", NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}
  g_signal_connect( rtpbin, "pad-added", G_CALLBACK( on_pad_added ), rtph264depay  );

  if ( !gst_element_link_pads_filtered( audpsrc_rtp, "src", rtpbin, "recv_rtp_sink_%u", NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( audpsrc_rtcp, "src", rtpbin, "recv_rtcp_sink_%u", NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( rtpbin, "send_rtcp_src_%u", audpsink_rtcp, "sink", NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}
  g_signal_connect( rtpbin, "pad-added", G_CALLBACK( on_pad_added ), rtpopusdepay  );
  
  if ( !gst_element_link_many( rtph264depay, queue1, vdecoder,  vsink, NULL )) { printf("failed to link rtph264depay, queue, vdecoder,  vsink\n");}
  if ( !gst_element_link_many( rtpopusdepay, queue2, adecoder,  asink, NULL )) { printf("failed to link rtpopusdepay, queue, adecoder,  asink\n");}

   g_print ( "Setting to recv video\n" );

  gst_element_set_state ( pipeline, GST_STATE_PAUSED );
  gst_element_set_state ( pipeline, GST_STATE_PLAYING );

  g_main_loop_run( loop );
  
  g_print ( "returned ,stopping playback\n" );

  g_print ( "Deleting pipeline\n" );
  gst_object_unref( GST_OBJECT ( pipeline ) );



return 0;

}

static void 
getArgs(int argc, char *argv)
{
                          //while 
}

int
main (int argc, char *argv[])
{
   printf("starting s2s, delivering your streams recursively \n");
/* Initialize GTK */
  //gtk_init (&argc, &argv);

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  //globalData global; 
  //global.videoInput = gst_element_factory_make ("videotestsrc", "videotestsrc");
  //global.audioInput = gst_element_factory_make ("audiotestsrc", "audiotestsrc"); 


  int opt=0; 
  while ((opt = getopt (argc, argv, "srp:")) != -1)
    {
      switch (opt)
        {
        case 's':
          printf("starting in sending mode\n");
	  rtpbinTransmitter();
          break;
        case 'r':
   	  rtpbinReciever();
          break;
        case 'p':
	  printf("baseport is %s\n", optarg);
          //port = optarg;
          break;
        case '?':
          /* Case when user enters the command as
           * $ ./cmd_exe -i
           */
          if (optopt == 'u')
            {
              printf ("\nMissing mandatory input option \n");
            }
          else
            {
              printf
                ("\nInvalid option received  \n -i playuri \n -f feedname \n -s sink \n");
            }
          break;
        }                       //switch
    }  

  
  return 0;
}
