/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

extern void playlist_init (GtkWidget *playlist_box);
extern GtkWidget * playlist_init_gtk ();
extern void playlist_add_item (const gchar *);
extern gchar * playlist_get_current ();
// do not use this
//extern gchar *playlist_get_first ();

extern gchar *playlist_get_next ();

// do not use this
//extern gchar *playlist_get_previous ();

extern void playlist_refresh (PlayerData * data);
