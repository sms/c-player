/* get vars from snowmix */
#include "common.h"
#include "switchs.h"
#include <regex.h>        
#include "snowbin.h"

struct snowData *snow;


  int
main (int argc, char *argv[])
{
  GstBin *snowBin;
  GstBin *snowAudioBin;
  /* Initialize GStreamer */
  gst_init (NULL, NULL);


  snow = init_snowData(snow);

  snowBin = get_snowvideobin(snow);
  snowAudioBin = get_snowaudiobin(snow);
  print_snowMixer(snow->mixer);
  print_snowFeed(snow->currentFeed);



}
