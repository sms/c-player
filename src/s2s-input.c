/* s2s-input.c inputs for s2s
 * 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author yids
 */

 
/* input functions */
int input_set_test()
{
  printf("set test\n");
  GstElement *videotestsrc = gst_element_factory_make("videotestsrc", "VideoTest");
  GstElement *audiotestsrc = gst_element_factory_make("audiotestsrc", "AudioTest");

  g_object_set( G_OBJECT(videotestsrc), "is-live", TRUE, NULL);
  g_object_set( G_OBJECT(audiotestsrc), "is-live", TRUE, NULL);

  mainData[numStreams].videoInput = get_fortune_src();
  mainData[numStreams].audioInput = audiotestsrc;
  
  pipe_check(numStreams);
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput );
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].audioInput );
  return 0;  
}

int input_set_vtest()
{
  printf("set vtest\n");
  GstElement *videotestsrc = gst_element_factory_make("videotestsrc", "VideoTest");
  g_object_set( G_OBJECT(videotestsrc), "is-live", TRUE, NULL);
  mainData[numStreams].videoInput = videotestsrc;
  pipe_check(numStreams);
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput );
  return 0;
}

int input_set_atest()
{
  printf("set atest\n");
  GstElement *audiotestsrc = gst_element_factory_make("audiotestsrc", "AudioTest");
  g_object_set( G_OBJECT(audiotestsrc), "is-live", TRUE, NULL);
  mainData[numStreams].audioInput = audiotestsrc;
  pipe_check(numStreams);;
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].audioInput );
  return 0;
}

int input_set_v4l()
{
  printf("set v4l\n");
  char *device;
  GstElement *v4l2src = gst_element_factory_make("v4l2src","V4L");
  device = readline("v4l2 device to use: \n");
  g_object_set(G_OBJECT(v4l2src), "device", device, NULL);
  mainData[numStreams].videoInput = v4l2src;

  pipe_check(numStreams);  
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput );
  return 0;
}

int input_set_gphoto()
{

  mainData[numStreams].videoInput = GST_ELEMENT(get_gphoto_src());
  pipe_check(numStreams);
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput );
  return 0;
}

int input_set_ipcam()
{
  printf("set input to ip cam\n");
  char *url;
  GstElement *ipcambin       = gst_bin_new("IP camera");
  GstElement *souphttpsrc    = gst_element_factory_make("souphttpsrc","souphttpsrc");
  GstElement *multipartdemux = gst_element_factory_make("multipartdemux", "multipartdemux"); 
  GstElement *jpegdec        = gst_element_factory_make("jpegdec", "jpegdec");
  GstElement *videoconvert   = gst_element_factory_make("videoconvert", "videoconvert");
  GstElement *videorate	     = gst_element_factory_make("videorate", "IP Camera");

  url = readline ("enter URL: \n");
  
  /* set the properties */  
  g_object_set(G_OBJECT(souphttpsrc), "location", url, NULL);
  g_object_set(G_OBJECT(souphttpsrc), "is-live", TRUE, NULL);
  
  /* create bin */
  gst_bin_add_many(GST_BIN(ipcambin), souphttpsrc, jpegdec, NULL);

  /* link the elements */
  if( !gst_element_link_many(souphttpsrc, jpegdec, NULL)) { printf("ip cam link failed\n");}
  
  /* add pads to the bin */
  GstPad *vpad, *vghostpad;
  vpad = gst_element_get_static_pad (jpegdec, "src");
  vghostpad = gst_ghost_pad_new ("src", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (ipcambin, vghostpad);

  /* set to input of stream, check if pipeline exists and add the input to the pipelein */
  mainData[numStreams].videoInput = ipcambin;
  pipe_check(numStreams); 
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput );
 
  return 0;
}

int input_set_alsa()
{
  printf("set alsa\n");
  GstElement *alsasrc = gst_element_factory_make("alsasrc", "ALSA");
  g_object_set( G_OBJECT(alsasrc), "is-live", TRUE, NULL);
  mainData[numStreams].audioInput = alsasrc;
  if(mainData[numStreams].pipeline != NULL)
  {
    gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].audioInput );
  } 
  return 0;
}

int input_set_jack()
{
  printf("set jack\n");
  return 0;
}

int 
input_set_rtp()
{
  printf("set rtp\n");
 
//int port = baseport+numStreams*6;
  inputRtpData[numStreams].streamNumber = numStreams; // set the stream number in the rtpdata structure 

  char *input;
  int port;
  char *hostname = "127.0.0.1";
  input = readline("set port (rtp wil use 6 ports)");
  port = atoi(input);
  printf("RTP IN BASEPORT %u\n",port);
  g_free(input);
 
  input = readline("set IP (defaults to 127.0.0.1)");
  if(strncmp(input,"",640) != 0 ) {strncpy(hostname,input,20);}
  printf("RTP IN IP %s\n",hostname);
  g_free(input);

  gchar *vcaps = "application/x-rtp,media=(string)video,clock-rate=(int)90000,encoding-name=(string)H264,payload=(int)96"; 
  gchar *acaps = "application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00,payload=(int)96"; 
  
  // create the elements  
  mainData[numStreams].pipeline           = gst_pipeline_new( "video-recv" );
  inputRtpData[numStreams].vudpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp video" );
  inputRtpData[numStreams].vudpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp video" );
  inputRtpData[numStreams].vudpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp video " );
  inputRtpData[numStreams].audpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp audio" );
  inputRtpData[numStreams].audpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp audio" );
  inputRtpData[numStreams].audpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp audio " );
  inputRtpData[numStreams].rtpbin         = gst_element_factory_make ( "rtpbin", "gstrtpbin" );
  inputRtpData[numStreams].rtph264depay   = gst_element_factory_make ( "rtph264depay", "rtph264depay" );
  inputRtpData[numStreams].rtpopusdepay   = gst_element_factory_make ( "rtpopusdepay", "rtpopusdepay" );
  inputRtpData[numStreams].queue1         = gst_element_factory_make ( "queue2", "queue1" );
  inputRtpData[numStreams].queue2         = gst_element_factory_make ( "queue2", "queue2" );
  inputRtpData[numStreams].vdecoder       = gst_element_factory_make ( "avdec_h264", "vdecoder" );
  inputRtpData[numStreams].adecoder       = gst_element_factory_make ( "opusdec", "adecoder" );
  mainData[numStreams].videoInput         = gst_element_factory_make ( "identity", "RTP video");
  mainData[numStreams].audioInput         = gst_element_factory_make ( "identity", "RTP audio" );

  // set properties 
  g_object_set( G_OBJECT ( inputRtpData[numStreams].vudpsrc_rtp ),   "port", port, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].vudpsrc_rtcp ),  "port", port+1, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].vudpsink_rtcp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].vudpsink_rtcp ), "port", port+2, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].audpsrc_rtp ),   "port", port+3, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].audpsrc_rtcp ),  "port", port+4, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].audpsink_rtcp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].audpsink_rtcp ), "port", port+5, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].rtpbin ), "autoremove", TRUE, NULL );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].rtpbin ), "do-sync-event", TRUE, NULL ); 
  g_object_set( G_OBJECT ( inputRtpData[numStreams].rtpbin ), "async-handling", TRUE, NULL );

  g_object_set( G_OBJECT ( inputRtpData[numStreams].audpsink_rtcp ), "sync", FALSE, NULL);
  g_object_set( G_OBJECT ( inputRtpData[numStreams].vudpsink_rtcp ), "sync", FALSE, NULL);
  g_object_set( G_OBJECT ( inputRtpData[numStreams].audpsink_rtcp ), "async", FALSE, NULL);
  g_object_set( G_OBJECT ( inputRtpData[numStreams].vudpsink_rtcp ), "async", FALSE, NULL);



  // add elements to pipeline  
  gst_bin_add_many( GST_BIN( mainData[numStreams].pipeline ),
					                 inputRtpData[numStreams].vudpsrc_rtp, inputRtpData[numStreams].vudpsrc_rtcp, inputRtpData[numStreams].vudpsink_rtcp,
  					                 inputRtpData[numStreams].audpsrc_rtp, inputRtpData[numStreams].audpsrc_rtcp, inputRtpData[numStreams].audpsink_rtcp,
	                  		                 inputRtpData[numStreams].rtpbin, inputRtpData[numStreams].rtph264depay, inputRtpData[numStreams].rtpopusdepay, 
					                 inputRtpData[numStreams].queue1, inputRtpData[numStreams].queue2, inputRtpData[numStreams].vdecoder, 
							 inputRtpData[numStreams].adecoder, mainData[numStreams].videoInput, mainData[numStreams].audioInput, NULL);

  gst_bin_add( GST_BIN(mainData[numStreams].pipeline),mainData[numStreams].videoInput);
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline),mainData[numStreams].audioInput);

  // set the caps 
  inputRtpData[numStreams].vsrccaps = gst_caps_from_string( vcaps );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].vudpsrc_rtp ), "caps", GST_CAPS(inputRtpData[numStreams].vsrccaps) , NULL );
  
  inputRtpData[numStreams].asrccaps = gst_caps_from_string( acaps );
  g_object_set( G_OBJECT ( inputRtpData[numStreams].audpsrc_rtp ), "caps", GST_CAPS(inputRtpData[numStreams].asrccaps) , NULL );

  // link the elements 
  if ( !gst_element_link_pads_filtered( inputRtpData[numStreams].vudpsrc_rtp, "src", 
				        inputRtpData[numStreams].rtpbin, "recv_rtp_sink_%u", 
				        NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}

  if ( !gst_element_link_pads_filtered( inputRtpData[numStreams].vudpsrc_rtcp, "src", 
				        inputRtpData[numStreams].rtpbin, "recv_rtcp_sink_%u", 
					NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}

  if ( !gst_element_link_pads_filtered( inputRtpData[numStreams].rtpbin, "send_rtcp_src_%u", 
				        inputRtpData[numStreams].vudpsink_rtcp, "sink", 
					NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}

  if ( !gst_element_link_pads_filtered( inputRtpData[numStreams].audpsrc_rtp, "src", 
                                        inputRtpData[numStreams].rtpbin, "recv_rtp_sink_%u", 
					NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}

  if ( !gst_element_link_pads_filtered( inputRtpData[numStreams].audpsrc_rtcp, "src", 
				        inputRtpData[numStreams].rtpbin, "recv_rtcp_sink_%u", 
					NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}
  
  if ( !gst_element_link_pads_filtered( inputRtpData[numStreams].rtpbin, "send_rtcp_src_%u", 
  				        inputRtpData[numStreams].audpsink_rtcp, "sink", 
					NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}
  
  if ( !gst_element_link_many( inputRtpData[numStreams].rtph264depay, 
			       inputRtpData[numStreams].queue1, 
                               inputRtpData[numStreams].vdecoder,  
                               mainData[numStreams].videoInput, 
			       NULL )) { printf("failed to link rtph264depay, queue, vdecoder,  vsink\n");}
  if ( !gst_element_link_many( inputRtpData[numStreams].rtpopusdepay, 
			       inputRtpData[numStreams].queue2, 
                               inputRtpData[numStreams].adecoder, 
                               mainData[numStreams].audioInput, 
			       NULL )) { printf("failed to link rtpopusdepay, queue, adecoder,  asink\n");}

  // connect callbacks 
  g_signal_connect( inputRtpData[numStreams].rtpbin, "pad-added",  G_CALLBACK( on_pad_added ),   inputRtpData[numStreams].rtpopusdepay  );
  g_signal_connect( inputRtpData[numStreams].rtpbin, "pad-added",  G_CALLBACK( on_pad_added ),   inputRtpData[numStreams].rtph264depay  );
  g_signal_connect( inputRtpData[numStreams].rtpbin, "on-timeout", G_CALLBACK( stream_timeout ), &inputRtpData[numStreams]);

 
/*
  GstElement *rx_rtpbin = get_rx_rtpbin();
  GstPad *rtpbinAPad = gst_element_get_static_pad(rx_rtpbin,"asrc");
  GstPad *rtpbinVPad = gst_element_get_static_pad(rx_rtpbin,"vsrc"); 

  GstElement *vinput = gst_element_factory_make("identity", "vinput");
  GstElement *ainput = gst_element_factory_make("identity", "ainput");
  GstPad *vinputPad = gst_element_get_static_pad(vinput, "sink");
  GstPad *ainputPad = gst_element_get_static_pad(ainput, "sink");

  gst_pad_link(rtpbinAPad, ainputPad);
  gst_pad_link(rtpbinVPad, vinputPad);

  mainData[numStreams].videoInput = vinput;
  mainData[numStreams].audioInput = ainput;
  pipe_check(numStreams);
  gst_bin_add_many( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput, mainData[numStreams].audioInput, NULL ); 
  
*/
  return 0;


 
}

int input_set_vudp()
{
  printf("set input to v-udp\n");
  return 0;
}

int input_set_audp()
{
  printf("set input to a-udp\n");
  return 0;
}

int input_set_dvgrab()
{
  printf("set input to dvgrab\n");

  mainData[numStreams].videoInput = GST_ELEMENT(get_dvgrab_src());
  pipe_check(numStreams);
  gst_bin_add( GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput );
  return 0;
}


int input_set_uri()
{
  printf("set input to uri\n");
  GstElement *uridecodebin;
  char *input;

  uridecodebin  = gst_element_factory_make("uridecodebin", "uridecodebin");

  input = readline("set uri: ");
  printf("uri set to %s\n", input);
  
  g_object_set(G_OBJECT(uridecodebin), "uri", input, NULL);
//  g_signal_connect(G_OBJECT(uridecodebin), "pad-added", pad_added_handler, NULL);


  pipe_check(numStreams);
 
//  gst_bin_add(GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoInput);
//  gst_bin_add(GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].audioInput);


  return 0;
}
