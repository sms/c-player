/* s2s-cli, cli for stream2stream, we eventually take over and 
 * leave s2s behind...
 *
 
* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Author yids */ 

#include "s2s-cli.h"
#include "snowbin.h"
#include "fortunesrc.h"
#include "gphotosrc.h"
#include "rtpiobin.h"
#include "common.h" 
#include "dvgrabsrc.h"
#include <signal.h>

/* global variables */
int numStreams = 0;
//char *hostname = "127.0.0.1";
int baseport = 10000;
inputRtp inputRtpData[10];
outputRtp outputRtpData[10];
outputRtmp outputRtmpData[10];
/* global structures */


/* ---------------------------------------- CALLBACK FUNCTIONS -------------------------------------------- */

static void 
stream_timeout(GstElement *bin, guint session, guint ssrc, inputRtp data )
{
  printf("\nStream %u timed out", data.streamNumber);
  GstElement *tmpPipe = mainData[data.streamNumber].pipeline;
  GstElement *tmpVout = mainData[data.streamNumber].videoOutput;
  gst_object_unref(mainData[data.streamNumber].pipeline); 
  gst_object_unref(mainData[data.streamNumber].videoOutput);

  mainData[data.streamNumber].pipeline = tmpPipe; 
  mainData[data.streamNumber].videoOutput = tmpVout;
}

static void 
on_pad_added ( GstElement *element, GstPad *pad, gpointer data )
{
  GstPad *sinkpad;
  GstElement* elem = ( GstElement* ) data;
  sinkpad = gst_element_get_static_pad( elem , "sink" );
  if (GST_PAD_IS_SRC(pad)) {gst_pad_link ( pad, sinkpad );}
  gst_object_unref( sinkpad );
}


/* ---------------------------------------- UTILITY FUNCTIONS ---------------------------------------------- */

int 
pipe_check(int streamNumber)
{

  if(mainData[streamNumber].pipeline == NULL)
  {
    mainData[streamNumber].pipeline = gst_pipeline_new("pipeline");
  }
  return 0;
}

int
link_in_out()
{
  pipe_check(numStreams);
  gst_bin_add_many(GST_BIN(mainData[numStreams].pipeline), mainData[numStreams].videoOutput, mainData[numStreams].audioOutput, NULL);
/*
  GstPad *vinputPad = gst_element_get_static_pad(mainData[numStreams].videoInput, "sink");
  GstPad *voutputPad= gst_element_get_static_pad(mainData[numStreams].videoOutput, "src");
  GstPad *ainputPad = gst_element_get_static_pad(mainData[numStreams].audioInput, "sink");
  GstPad *aoutputPad= gst_element_get_static_pad(mainData[numStreams].audioOutput, "src");
*/

//  gst_element_link_pads(mainData[numStreams].videoInput, "sink", mainData[numStreams].videoOutput, "src");
//  gst_element_link_pads(mainData[numStreams].videoInput, "sink", mainData[numStreams].videoOutput, "src"); 
  gst_element_link_many(mainData[numStreams].videoInput, mainData[numStreams].videoOutput, NULL);
  gst_element_link_many(mainData[numStreams].audioInput, mainData[numStreams].audioOutput, NULL);

  return 0;
}

static int
shell()
{
  char *input; 
  system("clear");
  for(;;)
  {
    input = readline("s2s-cli: ");

    add_history(input);

    for(uint i=0; i < sizeof(commands)/sizeof(COMMAND) ; i++)
    {
      if(strncmp(commands[i].name, input, 10) == 0)
      {
        printf("executing %s\n",input);
	((*(commands[i].func)) (input));	
	free(input);
        input = NULL;
	break;
      }
      else
      {
	//printf("%s: command not found\n",input);
        //break;
      }
     //break;
    }
     
       
  }
  return 0;
}

/* shell command functions */
int stream_add (arg)
char *arg; 
{
  char *userInput = NULL; 

  /* set name and number of the stream */
  printf("Adding stream number: %u\n",numStreams);
  mainData[numStreams].streamName = readline("enter name for the stream: ");
  mainData[numStreams].streamNumber = numStreams;
 
  /* set inputs for the stream */
  while(mainData[numStreams].videoInput == NULL || mainData[numStreams].audioInput == NULL)
  {
 //   system("clear");
    printf("select input\n");
    if (mainData[numStreams].videoInput == NULL) {printf("no video input set\n");}
    if (mainData[numStreams].audioInput == NULL) {printf("no audio input set\n");}
    for(uint i=0; i < sizeof(inputs)/sizeof(INPUT); i++)
    {
      printf("%u) %s.\n", i, inputs[i].name);
    }
    userInput = readline("");  
    ((*(inputs[atoi(userInput)].func)) ((inputs[atoi(userInput)].name)));
    g_free(userInput);
  }
  /* set outputs for stream */ 
//  system("clear");
  while(mainData[numStreams].videoOutput == NULL || mainData[numStreams].audioOutput == NULL)
  {
   printf("select output\n");
   if (mainData[numStreams].videoOutput == NULL) {printf("no video output set\n");}
   if (mainData[numStreams].audioOutput == NULL) {printf("no audio output set\n");}
   for(uint i=0; i < sizeof(outputs)/sizeof(OUTPUT); i++)
   {
     printf("%u) %s.\n", i, outputs[i].name);
   }
    userInput = readline("");
    ((*(outputs[atoi(userInput)].func)) ((outputs[atoi(userInput)].name)));
    g_free(userInput);
  }

  link_in_out();

  /* increment number of streams */
  numStreams+=1;
  return 0;
}
 
int stream_remove(arg)
char *arg; 
{

  sscanf(arg, "removeStream %s", arg);
  int streamNumber = atoi(arg);
/*
  if(streamNumber == 0)
  {
    printf("please enter the number of the stream you want to remove\n");
    return 1;
  }
*/  
  printf("removing stream %u\n",streamNumber);
  gst_element_set_state ( mainData[streamNumber].pipeline, GST_STATE_NULL );
  gst_object_unref(mainData[streamNumber].pipeline);
  mainData[streamNumber].pipeline = NULL;
  mainData[streamNumber].videoInput = NULL;
  mainData[streamNumber].videoOutput = NULL;
  mainData[streamNumber].audioInput = NULL;
  mainData[streamNumber].videoOutput = NULL;
  numStreams--;

  return 0;
}

int stream_start(arg)
char *arg;
{

  sscanf(arg, "startStream %s", arg);
  int streamNumber = atoi(arg);
/*
  if(streamNumber == NULL)
  {
    printf("please enter the number of the stream you want to start\n");
    return 1;
  }
*/
  printf("Starting stream %u\n",streamNumber);

  gst_element_set_state ( mainData[streamNumber].pipeline, GST_STATE_PLAYING );
//  g_signal_connect( inputRtpData[streamNumber].rtpbin, "on-timeout", G_CALLBACK( stream_timeout ), &inputRtpData[numStreams]);

  return 0;
}

int stream_stop(arg)
char *arg;
{
//  printf("stop %u\n",arg);
  return 0;
}

int stream_status(arg)
char *arg;
{
  printf("Number of streams %u\n",numStreams);
  for(int i = 0 ; i < numStreams ; i++)
  {
     printf("-------------Stream %d: %s --------------\n",i,mainData[i].streamName);
     printf("Video input  : %s\n",gst_element_get_name(mainData[i].videoInput)); 
     printf("Audio input  : %s\n",gst_element_get_name(mainData[i].audioInput));      
     printf("Video output : %s\n",gst_element_get_name(mainData[i].videoOutput));
     printf("Audio output : %s\n\n",gst_element_get_name(mainData[i].audioOutput));
 
  }
  return 0;
}

int stream_set_input(arg)
char *arg;
{
//  printf("set input %u\n",arg);
  return 0;
}

int stream_set_output(arg)
char *arg;
{
///  printf("set output %u\n",arg);
  return 0;
}

int help()
{
  printf("Available commands:\n");
 for(uint i=0; i < sizeof(commands)/sizeof(COMMAND) ; i++)
    {
      printf("%s - %s\n",commands[i].name, commands[i].doc);
    }
  return 0;
}

int quit()
{
  printf("bye bye\n");
  exit(0);
}
/* --------------------------------- STREAM I/O FUNCTIONS --------------------------------- */
#include "s2s-input.c"
#include "s2s-output.c"

void intHandler()
{
  printf("type quit to quit.\n");
  shell();
}

/* main function */
int 
main (int argc, char *argv[]) 
{
  printf("Welcome to the s2s cli tool, an atempt to make sense of s2s\n");
  gst_init (&argc, &argv);
  //get_fortune_src();
  signal(SIGINT, intHandler);


  shell();
}

