#include "common.h"


extern GstElement *
create_rtmpbin()
{
  GstElement *rtmpbin, *decodebin, *videoconvert, *videoenc, *flvmux, *audioconvert, *audioenc, *rtmpsink;
  GstElement *q1, *q2;

  rtmpbin =  gst_bin_new("rtmpbin");

//  decodebin = gst_element_factory_make("decodebin", "decodebin");
  videoconvert = gst_element_factory_make("videoconvert", "videoconvert");
  videoenc = gst_element_factory_make("x264enc", "videoencoder"); 
  flvmux = gst_element_factory_make("flvmux", "flvmux");
  audioconvert = gst_element_factory_make("audioconvert", "audioconvert");
  audioenc = gst_element_factory_make("voaaenc", "audioencoder");
  rtmpsink = gst_element_factory_make("rtmpsink", "rtmpsink");
  q1 = gst_element_factory_make("queue", "queue1");
  q2 = gst_element_factory_make("queue", "queue2");

  g_object_set(videoenc, "bitrate", 600, NULL);
  g_object_set(videoenc, "speed-preset", 5, NULL);

  g_object_set(rtmpsink, "location", "rtmp://blackhole.squesh.net:1955/live/PINKTEST", NULL);

  gst_bin_add_many(GST_BIN(rtmpbin), videoconvert, videoenc, q1, flvmux, q1, rtmpsink); 

  gst_element_link_many(videoconvert, videoenc, q1, flvmux, q1, rtmpsink);
  
  return rtmpbin; 

}
