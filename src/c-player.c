/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include "common.h"

#ifdef PL
#include "playlist.h"
#endif

#include "rtmpbin.h"

#ifdef SNOWBIN
#include "teebin.h"
#include "snowbin.h"
#endif


// player things
/*
static const char *playuri = NULL;
static char *feedname = NULL;
static int preview = 0;
static GstStateChangeReturn ret;
static GstBus *bus;
// snowbin or auto
static gchar *sink = "snowbin";
*/

//Gtk things
#ifdef GTK
/*
static GtkWidget *feed_label;
static GtkWidget *clock_label;
static char *markup = NULL;
static GtkWidget *video_window, *main_hbox;
*/
#ifdef TT
//threadtest
GtkWidget *label_time;
GThread   *thread_time;
GError    *error = NULL;
#endif

#endif


extern void player_cleanup( PlayerData * data) {
  // this needs to be called from the main thread!
  
  //global pData
  g_print ("Cleaning up\n");

  g_print ("Deleting pipeline\n");
  gst_element_set_state (data->playbin2, GST_STATE_NULL);
  gst_object_unref (GST_OBJECT (data->playbin2));
//  g_source_remove (bus_watch_id);

#ifdef GTK
  gtk_main_quit();
#else
  g_main_loop_unref (data->loop);
#endif

  free(data);
  exit(0);
}




#ifdef TT
static GSourceFunc set_time (char * markup_time)
{
  
  gtk_label_set_text(GTK_LABEL(label_time), markup_time ); 
  printf("test: %s", markup_time);

}

static gpointer thread_time_func( gpointer data )
{
    auto int i;
    
    auto gchar * markup_time;
    while(1)
    {
        sleep(1);
        
        i = i + 1;
        
        markup_time = g_markup_printf_escaped ("i:%u", i);
// > that is depreciated        gdk_threads_enter();
       gdk_threads_add_idle ((GSourceFunc)set_time,markup_time); 
// > that is depreciated        gdk_threads_leave();

        g_free(markup_time);
    }
}
#endif


/* This function is called when the GUI toolkit creates the physical window that will hold the video.
 * At this point we can retrieve its handler (which has a different meaning depending on the windowing system)
 * and pass it to GStreamer through the XOverlay interface. */

#ifdef GTK  
  static void
realize_cb (GtkWidget * widget, PlayerData * data)
{
  auto GdkWindow *window = gtk_widget_get_window (widget);
  static guintptr window_handle;

  if (!gdk_window_ensure_native (window))
    g_error ("Couldn't create native window needed for GstXOverlay!");

  /* Retrieve window handler from GDK */
#if defined (GDK_WINDOWING_WIN32)
  window_handle = (guintptr) GDK_WINDOW_HWND (window);
#elif defined (GDK_WINDOWING_QUARTZ)
  window_handle = gdk_quartz_window_get_nsview (window);
#elif defined (GDK_WINDOWING_X11)
  window_handle = GDK_WINDOW_XID (window);
#endif
  /* Pass it to playbin2, which implements XOverlay and will forward it to the video sink */

#ifdef GST1
  gst_video_overlay_set_window_handle (GST_VIDEO_OVERLAY (data->playbin2),
      window_handle);
#else
  gst_x_overlay_set_window_handle (GST_X_OVERLAY (data->playbin2),
      window_handle);
#endif

}
#endif

/* This function is called when the PLAY button is clicked */
#ifdef GTK
  static void
play_cb (GtkButton * button, PlayerData * data)
{

  gst_element_set_state (data->playbin2, GST_STATE_PLAYING);

}

/* This function is called when the PAUSE button is clicked */
  static void
pause_cb (GtkButton * button, PlayerData * data)
{

  gst_element_set_state (data->playbin2, GST_STATE_PAUSED);

}

/* This function is called when the STOP button is clicked */
  static void
stop_cb (GtkButton * button, PlayerData * data)
{
  gst_element_set_state (data->playbin2, GST_STATE_READY);
}
#endif

  extern void
player_load_uri (const gchar * uri, PlayerData * data)
{
  // this would stop playback, so don't do that
  //gst_element_set_state (playerData.playbin2, GST_STATE_READY);
  // GstStateChangeReturn ret;
  g_object_set (data->playbin2, "uri", uri, NULL);

#ifdef GTK
  char *markup; 
  markup =
    g_markup_printf_escaped
    ("<span foreground=\"red\" size=\"x-small\">%s</span>", uri);
  gtk_label_set_markup (GTK_LABEL (data->gui->loaded_label), markup);
  free(markup);
#endif
  //  gst_element_set_state (pData->playbin2, GST_STATE_PLAYING);
}

#ifdef GTK
  static void
preview_cb (GtkCheckButton * preview_check, PlayerData * data)
{
    if (data->preview == 0)
    {
//      g_print ("turning on preview checkcb \n");
      teebin_preview_on(data);
      data->preview = 1;
      return;
    }
    if (data->preview == 1)
    {
//      g_print ("turning off preview checkcb \n");
      teebin_preview_off(data);
      data->preview = 0;
      return;
    }
}
#endif

#ifdef GTK
/* This function is called everytime the video window needs to be redrawn (due to damage/exposure,
 * rescaling, etc). GStreamer takes care of this in the PAUSED and PLAYING states, otherwise,
 * we simply draw a black rectangle to avoid garbage showing up. */
  static gboolean
expose_cb (GtkWidget * widget, GdkEventExpose * event, PlayerData * data)
{

  if (data->state < GST_STATE_PAUSED)
  {
    GtkAllocation allocation;
    GdkWindow *window = gtk_widget_get_window (widget);

    cairo_t *cr;

    // Cairo is a 2D graphics library which we use here to clean the video window.
    // It is used by GStreamer for other reasons, so it will always be available to us. 
    gtk_widget_get_allocation (widget, &allocation);
    cr = gdk_cairo_create (window);
    cairo_set_source_rgb (cr, 0, 0, 0);
    cairo_rectangle (cr, 0, 0, allocation.width, allocation.height);
    cairo_fill (cr);
    cairo_destroy (cr);
  }

  return FALSE;
}
#endif


#ifdef GTK
/* This function is called when the slider changes its position. We perform a seek to the
 * new position here. */
  static void
slider_cb (GtkRange * range, PlayerData * data)
{
  auto gdouble value = gtk_range_get_value (GTK_RANGE (data->slider));
  gst_element_seek_simple (data->playbin2, GST_FORMAT_TIME,
      GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT,
      (gint64) (value * GST_SECOND));
}
#endif

#ifdef GTK
/* This creates all the GTK+ widgets that compose our application, and registers the callbacks */
  extern void
player_create_ui (PlayerData * data)
{
  char *markup;
  PlayerGui *gui;
  /* now in PlayerGui
  GtkWidget *main_window;	 //The uppermost window, containing all other windows 
  GtkWidget *main_box;		//  VBox to hold main_hbox and the controls 
  GtkWidget *controls;		//  HBox to hold the buttons and the slider 
  GtkWidget *options;		 // HBox to hold the options and current uri 
  GtkWidget *play_button, *pause_button, *stop_button, *open_button, *add_button;	// Buttons 
  GtkWidget *test_button, *previous_button, *next_button;
  */
  
  //  GtkWidget *main_hbox;		/* HBox to hold the video_window and the stream info text widget */
  //  static GtkWidget *preview_check;	/* Checkbuttons */
  // this should be done by playlist
  //  GtkWidget *loop_check;	/* Checkbuttons */

  // initialize data->gui
  data->gui = (PlayerGui*)malloc(sizeof(struct PlayerGui));
  gui = data->gui;
 
  gui->video_window = gtk_drawing_area_new ();
  gtk_widget_set_size_request (gui->video_window, 250, 350);
  gtk_widget_set_double_buffered (gui->video_window, FALSE);
  g_signal_connect (gui->video_window, "realize", G_CALLBACK (realize_cb), data);
  g_signal_connect (gui->video_window, "expose_event", G_CALLBACK (expose_cb),
      data);

  /* make the feed label that will change color based on playing state */

  gui->feed_label = gtk_label_new(NULL); 
  markup =
    g_markup_printf_escaped
    ("<span foreground=\"red\" size=\"x-large\">F: %u --</span>", data->feedid);
  gtk_label_set_markup (GTK_LABEL(gui->feed_label), markup);
  g_free (markup);

  gui->loaded_label = gtk_label_new(NULL); 
  markup =
    g_markup_printf_escaped
    ("<span foreground=\"red\" size=\"x-small\">currently loaded uri</span>");
  gtk_label_set_markup (GTK_LABEL(gui->loaded_label), markup);
  g_free (markup);

  markup =
    g_markup_printf_escaped
    ("<span foreground=\"pink\" size=\"x-large\">clock</span>");
  gui->clock_label = gtk_label_new ("clock");
  gtk_label_set_markup (GTK_LABEL (gui->clock_label), markup);
  g_free (markup);



  gui->play_button = gtk_button_new_from_stock (GTK_STOCK_MEDIA_PLAY);
  g_signal_connect (G_OBJECT (gui->play_button), "clicked", G_CALLBACK (play_cb),
      data);

  gui->pause_button = gtk_button_new_from_stock (GTK_STOCK_MEDIA_PAUSE);
  g_signal_connect (G_OBJECT (gui->pause_button), "clicked", G_CALLBACK (pause_cb),
      data);

  gui->stop_button = gtk_button_new_from_stock (GTK_STOCK_MEDIA_STOP);
  g_signal_connect (G_OBJECT (gui->stop_button), "clicked", G_CALLBACK (stop_cb),
      data);

  data->preview_check = gtk_check_button_new_with_label ("preview current video");
  g_signal_connect (G_OBJECT (data->preview_check), "toggled",
      G_CALLBACK (preview_cb), data);

//  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(preview_check), TRUE);

  //  uri_entrybox = gtk_entry_new ();

  data->slider = gtk_hscale_new_with_range (0, 1000, 1);
  gtk_scale_set_draw_value (GTK_SCALE (data->slider), 0);
  data->slider_update_signal_id =
    g_signal_connect (G_OBJECT (data->slider), "value-changed",
        G_CALLBACK (slider_cb), data);

#ifdef TT
label_time = gtk_label_new("Loading ...");
thread_time = g_thread_create( thread_time_func, (gpointer)label_time, FALSE, &error );
#endif

  /* what is this for??? */
  data->streams_list = gtk_text_view_new ();
  gtk_text_view_set_editable (GTK_TEXT_VIEW (data->streams_list), FALSE);

  /* pack all the windows in boxes */
  /* controls */
  gui->controls = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (gui->controls), gui->feed_label, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (gui->controls), gui->play_button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (gui->controls), gui->pause_button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (gui->controls), gui->stop_button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (gui->controls), data->preview_check, FALSE, FALSE, 2);

  /* options */
  gui->options = gtk_vbox_new (FALSE, 0);

  gtk_box_pack_start (GTK_BOX (gui->options), gui->video_window, TRUE, TRUE, 2);
  gtk_box_pack_start (GTK_BOX (gui->options), data->slider, TRUE, TRUE, 2);
  gtk_box_pack_start (GTK_BOX (gui->options), gui->clock_label, TRUE, TRUE, 2);
  gtk_box_pack_start (GTK_BOX (gui->options), gui->loaded_label, FALSE, FALSE, 2);
#ifdef TT
  gtk_box_pack_start (GTK_BOX (gui->options), gui->label_time, FALSE, FALSE, 2);
#endif

  // this should be done by playlist
  //  gtk_box_pack_start (GTK_BOX (options), uri_entrybox, FALSE, FALSE, 2);

  /* main hbox */
  gui->main_hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (gui->main_hbox), data->streams_list, FALSE, FALSE,
      2);

  /* main box */
  gui->main_box = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (gui->main_box), gui->main_hbox, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (gui->main_box), gui->options, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (gui->main_box), gui->controls, FALSE, FALSE, 0);

#ifdef PL
  /* init playlist */
  playlist_box = playlist_init_gtk ();

  // pack the playlist
  gtk_box_pack_start (GTK_BOX (gui->main_box), playlist_box, TRUE, TRUE, 0);

  player_load_uri (playlist_get_current (), data);
#endif
  //return gui->main_box;
}
#endif

#ifdef GTK
/* This function is called periodically to refresh the GUI */
  extern int
player_refresh_ui (PlayerData * data)
{
  auto GstFormat fmt = GST_FORMAT_TIME;
  auto gint64 current = -1;
  auto char *nicetime;
  auto int sec, min, hour, time;
  char *markup;
  //  printf("refresh_ui state: %u paused=%u  \n", data->state, GST_STATE_PAUSED);
  /* We do not want to update anything unless we are in the PAUSED or PLAYING states */
  if (data->state < GST_STATE_PAUSED)
    return TRUE;
    /* If we didn't know it yet, query the stream duration */
    if (!GST_CLOCK_TIME_IS_VALID (data->duration))
    {
#ifdef GST1
      if (!gst_element_query_duration
          (data->playbin2, fmt, &data->duration))
#else
        if (!gst_element_query_duration
            (data->playbin2, &fmt, &data->duration))
#endif

        {
          g_printerr ("Could not query current duration.\n");
        }
        else
        {
          /* Set the range of the slider to the clip duration, in SECONDS */
          // g_printf header is not included!
          //g_printf ("slider range:: %li \n", data->duration / GST_SECOND);
          gtk_range_set_range (GTK_RANGE (data->slider), 0,
              (gdouble) data->duration / GST_SECOND);
        }
    }
#ifdef GST1
  if (gst_element_query_position (data->playbin2, fmt, &current))
#else
    if (gst_element_query_position (data->playbin2, &fmt, &current))
#endif
    {
      /* Block the "value-changed" signal, so the slider_cb function is not called
       * (which would trigger a seek the user has not requested) */
      g_signal_handler_block (data->slider, data->slider_update_signal_id);
      /* Set the position of the slider to the current pipeline positoin, in SECONDS */
      gtk_range_set_value (GTK_RANGE (data->slider),
          (gdouble) current / GST_SECOND);


      /* Re-enable the signal */
      g_signal_handler_unblock (data->slider, data->slider_update_signal_id);
      //char *nicetime = "00:00:00";
            time = current / GST_SECOND;
      hour = time / 3600;
      min = time % 3600 / 60;
      sec = time % 60;
      //sprintf(nicetime, "%u:%u:%u", hour, min, sec);
      asprintf(&nicetime, "%u:%u:%u", hour, min, sec);
      //printf("%s", nicetime);
      markup =
        g_markup_printf_escaped
        ("<span foreground=\"black\" size=\"x-large\">%s</span>", nicetime);
      gtk_label_set_markup (GTK_LABEL (data->gui->clock_label), markup);


    }
  return TRUE;
}
#endif
  extern int 
player_reset_and_play (PlayerData *data) 
{
  //FIXME: this should use the PlayerData that it gets, not pData (but then the playlist needs fixing)
  /* stop playing */
  gst_element_set_state (pData->playbin2, GST_STATE_READY);

#ifdef PL
//  printf("current uri: %s", playlist_get_current());
  player_load_uri(playlist_get_current(), pData);
#endif
  int ret = 0;
  /* Start playing */
  ret = gst_element_set_state (pData->playbin2, GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE)
  {
    g_printerr ("state change failure in player_reset_and_play\n");
//    gst_object_unref (pData->playbin2);
//     return -1;
    return 1;
  }
  return 0;
}


/* This function is called when new metadata is discovered in the stream */
  static void
tags_cb (GstElement * playbin2, gint stream, PlayerData * data)
{
  /* We are possibly in a GStreamer working thread, so we notify the main
   * thread of this event through a message in the bus */

  // c-player.c:423:10: warning: not enough variable arguments to fit a sentinel [-Wformat=]
  //           NULL)));
  //
  gst_element_post_message (playbin2,
      gst_message_new_application (GST_OBJECT
        (playbin2),
        gst_structure_new 
        ("tags-changed",
         NULL)));
}

/* This function is called when an error message is posted on the bus */
  static void
error_cb (GstBus * bus, GstMessage * msg, PlayerData * data)
{
  auto GError *err;
  auto gchar *debug_info;

  /* Print error details on the screen */
  gst_message_parse_error (msg, &err, &debug_info);
  g_printerr ("Error received from element %s: %s\n",
      GST_OBJECT_NAME (msg->src), err->message);
  g_printerr ("Debugging information: %s\n",
      debug_info ? debug_info : "none");
  g_clear_error (&err);
  g_free (debug_info);

  /* Set the pipeline to READY (which stops playback) */
  gst_element_set_state (data->playbin2, GST_STATE_READY);
}

/* This function is called when the stream is almost finished
 * next item in playlist will be loaded now 
 * and something should flash*/
  static void
aeos_cb (GstBus * bus, GstMessage * msg, PlayerData * data)
{
  g_print ("Almost finished.\n");

  //load next playlist item
  //  load_uri (playlist_get_next ());

}

/* this function is called after source element is setup */
  static void
source_cb (GstBus * bus, GstMessage * msg, PlayerData * data)
{

  g_print ("Source setup.\n");


}


/* This function is called when an End-Of-Stream message is posted on the bus.
 * We just set the pipeline to READY (which stops playback) */
  static void
eos_cb (GstBus * bus, GstMessage * msg, PlayerData * data)
{
  g_print ("End-Of-Stream reached.\n");
  gst_element_set_state (data->playbin2, GST_STATE_READY);
  // loop should be done in playlist
  //if (loop == 0) load_uri (playlist_get_next ());
  player_reset_and_play(data);

  //gst_element_set_state (data->playbin2, GST_STATE_PLAYING);

}


/* This function is called when the pipeline changes states. We use it to
 * keep track of the current state. */
  static void
state_changed_cb (GstBus * bus, GstMessage * msg, PlayerData * data)
{
  auto GstState old_state, new_state, pending_state;
  auto GstFormat fmt = GST_FORMAT_TIME;
  char feedname[10] = "F:00"; 
  snprintf(&feedname, sizeof(feedname), "F:%u", data->feedid);
  char *markup;
  gst_message_parse_state_changed (msg, &old_state, &new_state,
      &pending_state);
#ifdef GTK
  player_refresh_ui(data);
#endif
  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (data->playbin2))
  {
    data->state = new_state;

    //   g_print ("State set to %s\n", gst_element_state_get_name (new_state));

    /* update play state indicators here ..... */

    switch (new_state)
    {

      case GST_STATE_READY:
#ifdef GTK
        markup =
          g_markup_printf_escaped
          ("<span foreground=\"orange\" size=\"x-large\">%s</span>",
           feedname);
        gtk_label_set_markup (GTK_LABEL (data->gui->feed_label), markup);
#endif
        break;

      case GST_STATE_PLAYING:
#ifdef GTK
        markup =
          g_markup_printf_escaped
          ("<span foreground=\"green\" size=\"x-large\">%s</span>",
           feedname);
        gtk_label_set_markup (GTK_LABEL (data->gui->feed_label), markup);
#endif
        break;

      case GST_STATE_VOID_PENDING:
#ifdef GTK
        markup =
          g_markup_printf_escaped
          ("<span foreground=\"purple\" size=\"x-large\">%s</span>",
           feedname);
        gtk_label_set_markup (GTK_LABEL (data->gui->feed_label), markup);
#endif
        break;

      case GST_STATE_NULL:
#ifdef GTK
        markup =
          g_markup_printf_escaped
          ("<span foreground=\"black\" size=\"x-large\">%s</span>",
           feedname);
        gtk_label_set_markup (GTK_LABEL (data->gui->feed_label), markup);
#endif
        break;



      case GST_STATE_PAUSED:

        /*  query the stream duration, but this is not the perfect place for it */
#ifdef GST1
        if (!gst_element_query_duration
            (data->playbin2, fmt, &data->duration))
#else
          if (!gst_element_query_duration
              (data->playbin2, &fmt, &data->duration))
#endif

          {
            g_printerr ("Could not query current duration.\n");
          }
          else
          {
#ifdef GTK
            /* Set the range of the slider to the clip duration, in SECONDS */
            //g_printf ("state_changed_cb: slider range:: %i \n", data->duration / GST_SECOND);

            gtk_range_set_range (GTK_RANGE (data->slider), 0,
                (gdouble) data->duration / GST_SECOND);
#endif
          }
#ifdef GTK
        markup =
          g_markup_printf_escaped
          ("<span foreground=\"blue\" size=\"x-large\">%s</span>",
           feedname);
        gtk_label_set_markup (GTK_LABEL (data->gui->feed_label), markup);
#endif
        break;

    }			//switch

    /* refresh ui to see if that makes position bar work ??? */
#ifdef GTK
    player_refresh_ui (data);
#endif
    if (old_state == GST_STATE_READY && new_state == GST_STATE_PAUSED)
    {
      /* For extra responsiveness, we refresh the GUI as soon as we reach the PAUSED state */
#ifdef GTK
      player_refresh_ui (data);
#endif
      /* autoplay on loading new file? */
      gst_element_set_state (data->playbin2, GST_STATE_PLAYING);
    }


  }
}

/* Extract metadata from all the streams and write it to the text widget in the GUI */
  static void
analyze_streams (PlayerData * data)
{
  auto gint i;
  auto GstTagList *tags;
  auto gchar *str, *total_str;
  auto guint rate;
  auto gint n_video, n_audio, n_text;

#ifdef GTK
  auto GtkTextBuffer *text;
  /* Clean current contents of the widget */
  text = gtk_text_view_get_buffer (GTK_TEXT_VIEW (data->streams_list));
  gtk_text_buffer_set_text (text, "", -1);
#endif
  /* Read some properties */
  g_object_get (data->playbin2, "n-video", &n_video, NULL);
  g_object_get (data->playbin2, "n-audio", &n_audio, NULL);
  g_object_get (data->playbin2, "n-text", &n_text, NULL);

  for (i = 0; i < n_video; i++)
  {
    tags = NULL;
    /* Retrieve the stream's video tags */
    g_signal_emit_by_name (data->playbin2, "get-video-tags", i, &tags);
    if (tags)
    {
      total_str = g_strdup_printf ("video stream %d:\n", i);
#ifdef GTK
      gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif
      g_free (total_str);
      gst_tag_list_get_string (tags, GST_TAG_VIDEO_CODEC, &str);
      total_str =
        g_strdup_printf ("  codec: %s\n", str ? str : "unknown");
#ifdef GTK
      gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif
      g_free (total_str);
      g_free (str);
#ifdef GST1
      gst_tag_list_unref (tags);
#else
      gst_tag_list_free (tags);
#endif

    }
  }

  for (i = 0; i < n_audio; i++)
  {
    tags = NULL;
    /* Retrieve the stream's audio tags */
    g_signal_emit_by_name (data->playbin2, "get-audio-tags", i, &tags);
    if (tags)
    {
      total_str = g_strdup_printf ("\naudio stream %d:\n", i);
#ifdef GTK
      gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif
      g_free (total_str);
      if (gst_tag_list_get_string (tags, GST_TAG_AUDIO_CODEC, &str))
      {
        total_str = g_strdup_printf ("  codec: %s\n", str);
#ifdef GTK
        gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif
        g_free (total_str);
        g_free (str);
      }
      if (gst_tag_list_get_string (tags, GST_TAG_LANGUAGE_CODE, &str))
      {
        total_str = g_strdup_printf ("  language: %s\n", str);
#ifdef GTK
        gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif
        g_free (total_str);
        g_free (str);
      }
      if (gst_tag_list_get_uint (tags, GST_TAG_BITRATE, &rate))
      {
        total_str = g_strdup_printf ("  bitrate: %d\n", rate);
#ifdef GTK
        gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif

        g_free (total_str);
      }
#ifdef GST1
      gst_tag_list_unref (tags);
#else
      gst_tag_list_free (tags);
#endif
    }
  }

  for (i = 0; i < n_text; i++)
  {
    tags = NULL;
    /* Retrieve the stream's subtitle tags */
    g_signal_emit_by_name (data->playbin2, "get-text-tags", i, &tags);
    if (tags)
    {
      total_str = g_strdup_printf ("\nsubtitle stream %d:\n", i);
#ifdef GTK
      gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif
      g_free (total_str);
      if (gst_tag_list_get_string (tags, GST_TAG_LANGUAGE_CODE, &str))
      {
        total_str = g_strdup_printf ("  language: %s\n", str);
#ifdef GTK
        gtk_text_buffer_insert_at_cursor (text, total_str, -1);
#endif
        g_free (total_str);
        g_free (str);
      }
#ifdef GST1
      gst_tag_list_unref (tags);
#else
      gst_tag_list_free (tags);
#endif
    }
  }
}

/* This function is called when an "application" message is posted on the bus.
 * Here we retrieve the message posted by the tags_cb callback */
  static void
application_cb (GstBus * bus, GstMessage * msg, PlayerData * data)
{
  /* with gst1
   * player-0.10.c: In function ‘application_cb’:
   * player-0.10.c:714:45: error: ‘GstMessage’ has no member named ‘structure’
   *    if (g_strcmp0 (gst_structure_get_name (msg->structure), "tags-changed") ==
   *  */

#ifdef GST1
 /* 
  if (g_strcmp0 (gst_structure_get_name (msg->structure), "tags-changed") ==
      0)
  {
    // If the message is the "tags-changed" (only one we are currently issuing), update
    // the stream info GUI 
    analyze_streams (data);
  }
*/
#endif
}

extern void player_init_media(PlayerData *data) 
{
  GstBus *bus;
  pData->duration = GST_CLOCK_TIME_NONE;
  pData->state = GST_STATE_NULL;
 
  /* Initialize GStreamer */
  gst_init (NULL, NULL);


 /* Create the elements */
#ifdef GST1
  pData->playbin2 = gst_element_factory_make ("playbin", "playbin2");
#else
  pData->playbin2 = gst_element_factory_make ("playbin2", "playbin2");
#endif

//  g_print ("sink: %s \n", sink);
  GstElement *videosink, *audiosink;
  videosink = gst_element_factory_make("autovideosink", "xvimagesink");
//  videosink = gst_element_factory_make("fakesink", "fakesink");


#ifdef SNOWBIN
    /*set snowvideobin as sink */
    g_object_set (pData->playbin2, "video-sink",
    		    create_teebin(videosink, get_snowvideobin(pData->snow)), NULL);

    /*set snowaudiobin as sink */
    audiosink = get_snowaudiobin(pData->snow);      
#else
    g_print ("autosink \n");
    audiosink = gst_element_factory_make ("autoaudiosink", "autoaudiosink");
    g_print (" SNOWBIN is not defined, so we are not using it! \n ");
    
#endif

    g_object_set (pData->playbin2, "audio-sink", audiosink, NULL);
 

  if (!pData->playbin2)
  {
    g_printerr ("Not all elements could be created.\n");
    //return -1;
  }

  /* Set the URI to play */
  //g_object_set (globalData.playbin2, "uri", "http://docs.gstreamer.com/media/sintel_trailer-480p.webm", NULL);

  /* Connect to interesting signals in playbin2 */
  g_signal_connect (G_OBJECT (pData->playbin2), "video-tags-changed",
      (GCallback) tags_cb, pData);
  g_signal_connect (G_OBJECT (pData->playbin2), "audio-tags-changed",
      (GCallback) tags_cb, pData);
  g_signal_connect (G_OBJECT (pData->playbin2), "text-tags-changed",
      (GCallback) tags_cb, pData);
  g_signal_connect (G_OBJECT (pData->playbin2), "about-to-finish",
      (GCallback) aeos_cb, pData);
  g_signal_connect (G_OBJECT (pData->playbin2), "source-setup",
      (GCallback) source_cb, pData);

  /* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
  bus = gst_element_get_bus (pData->playbin2);
  gst_bus_add_signal_watch (bus);
  g_signal_connect (G_OBJECT (bus), "message::error", (GCallback) error_cb,
      pData);
  g_signal_connect (G_OBJECT (bus), "message::eos", (GCallback) eos_cb,
      pData);
  g_signal_connect (G_OBJECT (bus), "message::state-changed",
      (GCallback) state_changed_cb, pData);
  g_signal_connect (G_OBJECT (bus), "message::application",
      (GCallback) application_cb, pData);
  g_signal_connect (G_OBJECT (bus), "message::about-to-finish",
      (GCallback) application_cb, pData);
  gst_object_unref (bus);

}


