/*  rptiobin.h - header file for creating rx and tx rtpbins.
 * 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author yids
 */

extern GstElement *get_tx_rtpbin();
extern GstElement *get_rx_rtpbin();
/*
// rx rtp data structre
typedef struct rxRtpData {

  int streamNumber;

  int baseport;
  GstElement *rx_rtp_bin;
  GstElement *pipeline, *vudpsrc_rtp, *vudpsrc_rtcp, *vudpsink_rtcp, *audpsrc_rtp, *audpsrc_rtcp, *audpsink_rtcp;
  GstElement *rtpbin, *rtph264depay, *rtpopusdepay, *queue1, *queue2, *vdecoder, *adecoder;
  GstElement *video, *audio;
  GstPad* srcpad;
  GstCaps *vsrccaps, *asrccaps;

} rxRtpData;
*/

// tx rtp data structure
typedef struct txRtpData {
  int streamNumber;
  GstBus     *bus;
  GstElement *pipeline;
  GstElement *vsource, *asource,  *vqueue, *aqueue, *videoscale;
  GstElement *decodebin, *vencoder, *aencoder, *audioresample,  *rtph264pay, *rtpopuspay;
  GstElement *rtpbin, *vudpsink_rtp, *vudpsink_rtcp,*vudpsrc_rtcp, *audpsink_rtp, *audpsink_rtcp,*audpsrc_rtcp;
  GMainLoop  *loop;
} txRtpData;


