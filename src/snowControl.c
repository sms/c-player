/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <gtk/gtk.h>

int sockfd, portno, n;
struct sockaddr_in serv_addr;
struct hostent *server;
GtkWidget *currentFeedLabel;

static void
buttonCb (GtkButton * button, gpointer * feedNum)
{
  gtk_label_set_text (GTK_LABEL (currentFeedLabel), (char *) feedNum);
  char *tmp = NULL;
  asprintf (&tmp, "l%s\n", (char *) feedNum);
  sendCommand (tmp);
}

static void
createUI ()
{
  GtkWidget *mainWindow;
  GtkWidget *buttonHbox;
  GtkWidget *infoHbox;
  GtkWidget *mainVbox;
  GtkWidget *feed1Button, *feed2Button, *feed3Button, *feed4Button,
    *feed5Button, *feed6Button, *feed7Button, *feed8Button;
  GtkWidget *feedLabel;

  mainWindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  feedLabel = gtk_label_new ("current feed: ");
  currentFeedLabel = gtk_label_new ("1");

  gtk_window_set_resizable (GTK_WINDOW (mainWindow), FALSE);
  feed1Button = gtk_button_new_with_label ("feed 1");
  g_signal_connect (G_OBJECT (feed1Button), "clicked", G_CALLBACK (buttonCb),
		    "1");

  feed2Button = gtk_button_new_with_label ("feed 2");
  g_signal_connect (G_OBJECT (feed2Button), "clicked", G_CALLBACK (buttonCb),
		    "2");

  feed3Button = gtk_button_new_with_label ("feed 3");
  g_signal_connect (G_OBJECT (feed3Button), "clicked", G_CALLBACK (buttonCb),
		    "3");

  feed4Button = gtk_button_new_with_label ("feed 4");
  g_signal_connect (G_OBJECT (feed4Button), "clicked", G_CALLBACK (buttonCb),
		    "4");

  feed5Button = gtk_button_new_with_label ("feed 5");
  g_signal_connect (G_OBJECT (feed5Button), "clicked", G_CALLBACK (buttonCb),
		    "5");

  feed6Button = gtk_button_new_with_label ("feed 6");
  g_signal_connect (G_OBJECT (feed6Button), "clicked", G_CALLBACK (buttonCb),
		    "6");

  feed7Button = gtk_button_new_with_label ("feed 7");
  g_signal_connect (G_OBJECT (feed7Button), "clicked", G_CALLBACK (buttonCb),
		    "7");

  feed8Button = gtk_button_new_with_label ("feed 8");
  g_signal_connect (G_OBJECT (feed7Button), "clicked", G_CALLBACK (buttonCb),
		    "8");

  buttonHbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed1Button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed2Button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed3Button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed4Button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed5Button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed6Button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed7Button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (buttonHbox), feed8Button, FALSE, FALSE, 2);

  infoHbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (infoHbox), feedLabel, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (infoHbox), currentFeedLabel, FALSE, FALSE, 2);

  mainVbox = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (mainVbox), infoHbox, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (mainVbox), buttonHbox, FALSE, FALSE, 2);


  gtk_container_add (GTK_CONTAINER (mainWindow), mainVbox);
  gtk_widget_show_all (mainWindow);
}

void
error (const char *msg)
{
  perror (msg);
  exit (0);
}

int
sendCommand (char *command)
{

  sockfd = socket (AF_INET, SOCK_STREAM, 0);
  char buffer[256] = "l3";

  if (sockfd < 0)
    error ("ERROR opening socket");
  bzero ((char *) &serv_addr, sizeof (serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy ((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr,
	 server->h_length);
  serv_addr.sin_port = htons (portno);
  if (connect (sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) <
      0)
    error ("ERROR connecting");
  strncpy (buffer, command, 5);
  n = write (sockfd, buffer, strlen (buffer));
  if (n < 0)
    error ("ERROR writing to socket");
  close (sockfd);
  return 0;
}

int
main (int argc, char *argv[])
{
  if (argc < 3)
    {
      fprintf (stderr, "usage %s hostname port\n", argv[0]);
      exit (0);
    }
  server = gethostbyname (argv[1]);
  if (server == NULL)
    {
      fprintf (stderr, "ERROR, no such host\n");
      exit (0);
    }


  portno = atoi (argv[2]);
  sendCommand ("l2\n");
  /* Initialize GTK */
  gtk_init (&argc, &argv);
  createUI ();
  gtk_main ();
}
