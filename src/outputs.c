//#include "s2s-cli.h"
//std libs
#include <stdio.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

// readline 
#include <readline/readline.h>
#include <readline/history.h>


//glib stuff
#include <glib.h>
#include <glib/gprintf.h>

// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>


Extern GstElement * 
fortunesrc()
{
  GstElement *fortunebin = gst_bin_new("FortuneBin");
  GstElement *videotestsrc = gst_element_factory_make("videotestsrc", "videotestsrc");
  GstElement *textoverlay = gst_element_factory_make("textoverlay", "textoverlay");
  char text[5000];
  int  rndNum;

  /* create random number */
  time_t seconds;
  time(&seconds);
  srand((unsigned int) seconds);
  rndNum = rand() % 25 + 0;

  /* get text from fortune */
  FILE *fp;
  char path[1035];
  fp = popen("/usr/games/fortune", "r");
  if (fp == NULL) 
  {
    printf("Failed to run command\n" );
    exit(1);
  }

  while (fgets(path, sizeof(path)-1, fp) != NULL) 
  {;
    strncat(text, path, 5000);
  }

  pclose(fp);

  /* set element properties */
  g_object_set(G_OBJECT(videotestsrc), "pattern", rndNum, NULL);
  g_object_set(G_OBJECT(videotestsrc), "is-live", TRUE, NULL);
  g_object_set(G_OBJECT(textoverlay), "text", text, NULL);

  /* add elements to bin and link them */
  gst_bin_add_many(GST_BIN(fortunebin), videotestsrc, textoverlay, NULL);
  gst_element_link(videotestsrc, textoverlay); 

  /* create pads for the bin and return the bin */
  GstPad *vpad, *vghostpad;
  vpad = gst_element_get_static_pad (textoverlay, "src");
  vghostpad = gst_ghost_pad_new ("src", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (fortunebin, vghostpad);

  mainData[numStreams].videoInput = fortunebin;

  return fortunebin;
  

}

