#!/bin/bash 
# Deliver mixer1 output to dummy.
set -e

SM="`git rev-parse --show-toplevel`/../snowmix"
export SM


# IMPORTANT >>>>>You need to get port, ip and feed_id right<<<<<
port=9999
ip=127.0.0.1

# Set video feed 
feed_id=$1
audio_feed_id=$1

# Check for SM variable and the snowmix and gstreamer settings
if [ X$SM = X -o ! -f $SM/scripts/gstreamer-settings -o ! -f $SM/scripts/snowmix-settings ] ; then
  #echo "You need to se the environment variable SM to the base of the Snowmix directory"
  exit 1
fi

# Load the Snowmix and GStreamer settings
. $SM/scripts/gstreamer-settings
. $SM/scripts/snowmix-settings
# This will set
# a) feed_rate
# b) feed_channels
# c) feed_control_pipe
# d) feed_width
# e) feed_height
# f) ctrsocket
# g) system_width
# h) system_height
# i) ratefraction
# j) snowmix
# k) channels
# l) rate

if [ X$ctrsocket = X -o X$system_width = X -o X$system_height = X ] ; then
#  echo Failed to get control pipe or width or height from running snowmix
  exit 2
fi

VIDEOFORMAT=$VIDEOBGRA', width=(int)'$system_width', height=(int)'$system_height', framerate=(fraction)'$ratefraction
echo host: $ip
echo port: $port
echo feed_id: $feed_id
echo feed_rate: $feed_rate
echo feed_channels: $feed_channels
echo feed_control_pipe: $feed_control_pipe
echo feed_width: $feed_width
echo feed_height: $feed_height
echo ctrsocket: $ctrsocket
echo system_width: $system_width
echo system_height: $system_height
echo ratefraction: $ratefraction
echo snowmix: $snowmix
echo channels: $channels
echo rate: $rate
echo videoformat: $VIDEOFORMAT

#make vartest
#./vartest

#env
