#include <stdio.h>
#include "switchs.h"

int main(int argc, char **argv) {
     switchs(argv[1]) {
        cases("foo")
        cases("bar")
            printf("foo or bar (case sensitive)\n");
            break;

        icases("pi")
            printf("pi or Pi or pI or PI (case insensitive)\n");
            break;

        cases_re("^D.*",0)
            printf("Something that start with D (case sensitive)\n");
            break;

        cases_re("^E.*",REG_ICASE)
            printf("Something that start with E (case insensitive)\n");
            break;

        cases("1")
            printf("1\n");

        cases("2")
            printf("2\n");
            break;

        defaults
            printf("No match\n");
            break;
    } switchs_end;
 
    return 0;
}
