/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */



/* stuff for te playlist */


#include "common.h"
#include "c-player.h"

GError *err = NULL;

GtkTreeRowReference *now_playing;
GtkWidget *file_window, *uri_entrybox;
/* this should be handled by the playlist */
int loop_current = 0;
int loop_all = 0;

// to change
PlayerData *data;




  /*playlist stuff */
GtkListStore *playlist_store;
GtkTreePath *playlist_path;
//  GtkTreeIter  playlist_iter;

GtkWidget *playlist_view;
GtkCellRenderer *uri_renderer;

GtkTreeViewColumn *uri_column;
GtkTreeViewColumn *duration_column;
GtkTreeViewColumn *seekable_column;
GtkTreeViewColumn *datetime_column;
GtkTreeViewColumn *acodec_column;
GtkTreeViewColumn *vcodec_column;


enum
{
  COLUMN_URI,
  COLUMN_DURATION,
  COLUMN_SEEKABLE,
  COLUMN_DATETIME,
  COLUMN_ACODEC,
  COLUMN_VCODEC,
  COLUMN_PLAYABLE,
  COLUMN_PLAYING,
  N_COLUMNS
};


/* Print a tag in a human-readable format (name: value) */
static void
print_tag_foreach (const GstTagList * tags, const gchar * tag,
		   gpointer user_data)
{
  GValue val = { 0, };
  gchar *str;
  gint depth = GPOINTER_TO_INT (user_data);

  gst_tag_list_copy_value (&val, tags, tag);

  if (G_VALUE_HOLDS_STRING (&val))
    str = g_value_dup_string (&val);
  else
    str = gst_value_serialize (&val);

  g_print ("%*s%s: %s\n", 2 * depth, " ", gst_tag_get_nick (tag), str);
  g_free (str);

  g_value_unset (&val);
}

/* Print information regarding a stream */
static void
print_stream_info (GstDiscovererStreamInfo * info, gint depth)
{
  gchar *desc = NULL;
  GstCaps *caps;
  const GstTagList *tags;

  caps = gst_discoverer_stream_info_get_caps (info);

  if (caps)
    {
      if (gst_caps_is_fixed (caps))
	desc = gst_pb_utils_get_codec_description (caps);
      else
	desc = gst_caps_to_string (caps);
      gst_caps_unref (caps);
    }

  g_print ("%*s%s: %s\n", 2 * depth, " ",
	   gst_discoverer_stream_info_get_stream_type_nick (info),
	   (desc ? desc : ""));

  if (desc)
    {
      g_free (desc);
      desc = NULL;
    }

  tags = gst_discoverer_stream_info_get_tags (info);
  if (tags)
    {
      g_print ("%*sTags:\n", 2 * (depth + 1), " ");
      gst_tag_list_foreach (tags, print_tag_foreach,
			    GINT_TO_POINTER (depth + 2));
    }
}

/* Print information regarding a stream and its substreams, if any */
static void
print_topology (GstDiscovererStreamInfo * info, gint depth)
{
  GstDiscovererStreamInfo *next;

  if (!info)
    return;

  print_stream_info (info, depth);

  next = gst_discoverer_stream_info_get_next (info);
  if (next)
    {
      print_topology (next, depth + 1);
      gst_discoverer_stream_info_unref (next);
    }
  else if (GST_IS_DISCOVERER_CONTAINER_INFO (info))
    {
      GList *tmp, *streams;

      streams =
	gst_discoverer_container_info_get_streams
	(GST_DISCOVERER_CONTAINER_INFO (info));
      for (tmp = streams; tmp; tmp = tmp->next)
	{
	  GstDiscovererStreamInfo *tmpinf =
	    (GstDiscovererStreamInfo *) tmp->data;
	  print_topology (tmpinf, depth + 1);
	}
      gst_discoverer_stream_info_list_free (streams);
    }
}

static void
update_playlist (const gchar * uri, const GstTagList * tags,
		 GstDiscovererInfo * dinfo, GstDiscovererResult result)
{

  gboolean valid;
  gint row_count = 0;
  GtkTreeIter iter;
  g_return_if_fail (playlist_store != NULL);
//  g_print ("update_playlist \n");
  /* Get first row in list store */
  valid =
    gtk_tree_model_get_iter_first (GTK_TREE_MODEL (playlist_store), &iter);

  while (valid)
    {
      gchar *luri;
      // gchar *duration;

      // Make sure you terminate calls to gtk_tree_model_get() with a “-1” value
      gtk_tree_model_get (GTK_TREE_MODEL (playlist_store), &iter,
			  COLUMN_URI, &luri, -1);
//COLUMN_DURATION, &duration,

      // Do something with the data
      //g_print ("Row %d: (%s,%s)\n",
      //         row_count, luri);

      if (strcmp (uri, luri) == 0)
	{
	  //g_print ("updating: %s \n", uri);
	  //dinfo = GstDiscovererInfo
	  //sinfo = GstDiscovererStreamInfo
	  //duration = GstClockTime = guint64 

	  //seekable gboolean
	  gboolean seekable;
	  seekable = gst_discoverer_info_get_seekable (dinfo);

	  gtk_list_store_set (playlist_store, &iter, COLUMN_SEEKABLE,
			      seekable, -1);

//        g_print ("seekable: %i \n", seekable);



	  GstClockTime duration;

	  //g_print ("duration in seconds : %u \n",
	  //               gst_discoverer_info_get_duration (dinfo) / GST_SECOND);
	  duration = gst_discoverer_info_get_duration (dinfo);

	  gtk_list_store_set (playlist_store, &iter, COLUMN_DURATION,
			      duration, -1);

	  //g_print("discoverer result: %i for: %s \n", result, uri);

	  gtk_list_store_set (playlist_store, &iter, COLUMN_PLAYABLE,
			      result, -1);


	  // gtk_list_store_set(playlist_store, &iter, COLUMN_DURATION, sprintf("%u",duration) ); 


	  /* 
	     GList *vlist;
	     GstDiscovererVideoInfo *vinfo;
	     vlist = gst_discoverer_info_get_video_streams(dinfo);
	   */
/* this doesn't work :(
 *
          vinfo = g_list_first(vlist);
         
          // vinfo = 
          //width / height = guint
          g_print("width: %u", gst_discoverer_video_info_get_width(vinfo));
          g_print("height: %u", gst_discoverer_video_info_get_height(vinfo));
 
          g_free(vlist);
          gst_discoverer_stream_info_list_free(vlist);

*/

	}
      else
	{
	  //g_print ("luri: %s is not uri: %s \n", luri, uri);

	}

      //  g_free (uri);
      //  g_free (duration);


      valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (playlist_store),
					&iter);
      row_count++;
    }

}


/* This function is called every time the discoverer has information regarding
 * one of the URIs we provided.*/
static void
on_discovered_cb (GstDiscoverer * discoverer, GstDiscovererInfo * info,
		  GError * err, PlayerData * data)
{
  GstDiscovererResult result;
  const gchar *uri;
  const GstTagList *tags;
  GstDiscovererStreamInfo *sinfo;
  uri = gst_discoverer_info_get_uri (info);
  tags = gst_discoverer_info_get_tags (info);
  
  result = gst_discoverer_info_get_result (info);
  /* update the playlist item */
  update_playlist (uri, tags, info, result);

  /* If we got no error, show the retrieved information */
  /* 
     g_print ("\nDuration: %" GST_TIME_FORMAT "\n", GST_TIME_ARGS (gst_discoverer_info_get_duration (info)));

     tags = gst_discoverer_info_get_tags (info);
     if (tags) {
     g_print ("Tags:\n");
     gst_tag_list_foreach (tags, print_tag_foreach, GINT_TO_POINTER (1));
     }

     g_print ("Seekable: %s\n", (gst_discoverer_info_get_seekable (info) ? "yes" : "no"));

     g_print ("\n");

     sinfo = gst_discoverer_info_get_stream_info (info);
     if (!sinfo)
     return;

     g_print ("Stream information:\n");

     print_topology (sinfo, 1);

     gst_discoverer_stream_info_unref (sinfo);

     g_print ("\n");
   */
}

static void
on_discover_finished_cb (PlayerData * data)
{
  //printf ("discover finished \n");
}

static void
discoverer_init ()
{
  /* Instantiate the Discoverer */
  pData->discoverer = gst_discoverer_new (5 * GST_SECOND, &err);

  if (!pData->discoverer)
    {
      g_print ("Error creating discoverer instance: %s\n", err->message);
      g_clear_error (&err);

//          return -1;
    }
  /* Connect to the interesting signals */
  g_signal_connect (pData->discoverer, "discovered",
		    G_CALLBACK (on_discovered_cb), pData);
  g_signal_connect (pData->discoverer, "finished",
		    G_CALLBACK (on_discover_finished_cb), pData);

  /* Start the discoverer process (nothing to do yet) */
  gst_discoverer_start (pData->discoverer);

}

static void
discover_uri (const gchar * uri)
{

  //g_print ("discovering: %s \n", uri);
  if (!gst_discoverer_discover_uri_async (pData->discoverer, uri))
    {
      g_print ("Failed to start discovering URI \n");
      g_object_unref (pData->discoverer);

      //    return -1;
    }

}

extern void
playlist_add_item (const gchar * uri)
{
  GtkTreeIter iter;
  gtk_list_store_append (playlist_store, &iter);

  gtk_list_store_set (playlist_store, &iter, COLUMN_URI, uri, -1);


  discover_uri (uri);

}

const gchar *
get_uri (GtkTreeRowReference * rowref)
{
  GtkTreePath *path;
  gchar *uri;
  GtkTreeIter iter;

//  path = gtk_tree_row_reference_get_path(rowref);

  path = gtk_tree_row_reference_get_path (now_playing);

  if (gtk_tree_model_get_iter (GTK_TREE_MODEL (playlist_store), &iter, path))
    {
      gtk_tree_model_get (GTK_TREE_MODEL (playlist_store), &iter,
			  COLUMN_URI, &uri, -1);
      return uri;
      //return "f";
    }
  else
    {

      return "oops://";
    }
}


extern const gchar *
playlist_get_first ()
{
  GtkTreeIter iter;
  GtkTreePath *path;

  if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (playlist_store), &iter))
    {
      g_print ("got first iter \n");
      path = gtk_tree_model_get_path (GTK_TREE_MODEL (playlist_store), &iter);
      now_playing =
	gtk_tree_row_reference_new (GTK_TREE_MODEL (playlist_store), path);
      return get_uri (now_playing);
    }
  else
    {
      g_print ("Maybe the playlist is empty? \n");
      return "empty://";
    }
}

extern const gchar *
playlist_get_previous ()
{
  gchar *uri;
  GtkTreePath *path;
  GtkTreeModel *model;
  path = gtk_tree_row_reference_get_path (now_playing);
  gtk_tree_path_prev (path);

  // check here properly if we are at end of playlist (invalid path) and then jump to start if playlist repeat is on 
  if (gtk_tree_row_reference_new (GTK_TREE_MODEL (playlist_store), path) !=
      NULL)
    {

      now_playing =
	gtk_tree_row_reference_new (GTK_TREE_MODEL (playlist_store), path);

      //g_print ("now_playing: %s \n", gtk_tree_path_to_string (path));

      return get_uri (now_playing);

    }
  else
    {
      g_print ("playlist empty returning: beginofplaylist:// \n");
      return playlist_get_first ();

    }
}

extern const gchar *
playlist_get_next ()
{
  gchar *uri;
  GtkTreePath *path;
  GtkTreeModel *model;
  path = gtk_tree_row_reference_get_path (now_playing);

 
  gtk_tree_path_next (path);

  // check here properly if we are at end of playlist (invalid path) and then jump to start if playlist repeat is on 
  if (gtk_tree_row_reference_new (GTK_TREE_MODEL (playlist_store), path) !=
      NULL)
    {

      now_playing =
	gtk_tree_row_reference_new (GTK_TREE_MODEL (playlist_store), path);

      //g_print ("now_playing: %s \n", gtk_tree_path_to_string (path));

      return get_uri (now_playing);

    }
  else
    {
      g_print ("playlist empty returning: endofplaylist:// \n");
      return playlist_get_first ();
    }
}

extern const gchar *
playlist_get_current ()
{
  if(now_playing == NULL){ playlist_get_first();}
  
  return get_uri (now_playing);

}
/*
extern void
playlist_get_current ()
{
  GtkTreeIter *iter;

  GtkTreePath *path;
  path = gtk_tree_row_reference_get_path (now_playing);


  g_print ("now_playing: %s \n", gtk_tree_path_to_string (path));


}
*/

/* playlist selection cb */
static void
playlist_selection_changed_cb (GtkTreeSelection * selection, gpointer data)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  gchar *uri;
  gchar *markup;
  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gtk_tree_model_get (model, &iter, COLUMN_URI, &uri, -1);

      //g_print ("You selected playlist item %s\n", uri);

      GtkTreePath *path;

      path = gtk_tree_model_get_path (model, &iter);

      GtkTreeRowReference *rowreference;
      rowreference = gtk_tree_row_reference_new (model, path);

      GtkTreePath *newpath;
      newpath = gtk_tree_row_reference_get_path (rowreference);

      //g_print ("current: %s \n", gtk_tree_path_to_string (newpath));

      gtk_tree_path_next (newpath);

      //g_print ("next: %s \n", gtk_tree_path_to_string (newpath));
      playlist_get_current ();
      g_free (uri);
      // change label color here?
/*      if (playabool == TRUE)	{
	{
	   printf("playable uri selected\n");
 	   markup =
           g_markup_printf_escaped
           ("<span foreground=\"green\" size=\"x-large\">currently loaded uri</span>");
           gtk_label_set_markup (GTK_LABEL (loaded_label), markup); 
	}
*/
    }

}

extern void
playlist_refresh (PlayerData * data)
{
  g_print ("refreshing playlist, recheck non playable streams here \n");
}

void
duration_data_function (GtkTreeViewColumn * col,
			GtkCellRenderer * renderer,
			GtkTreeModel * model,
			GtkTreeIter * iter, gpointer user_data)
{
  guint64 duration;
  gchar buf[50];
  int hours;
  int minutes;
  int seconds;
  gboolean seekable;
  gint playable = 15;

  gtk_tree_model_get (model, iter, COLUMN_DURATION, &duration,
		      COLUMN_SEEKABLE, &seekable, COLUMN_PLAYABLE, &playable,
		      -1);

  hours = GST_TIME_AS_SECONDS (duration) / 3600;
  seconds = GST_TIME_AS_SECONDS (duration) % 60;
  minutes = (GST_TIME_AS_SECONDS (duration) / 60) % 60;


  // check if playable and why not
  if (playable == 0)
    {
      g_object_set (renderer, "cell-background", "Green",
		    "cell-background-set", TRUE, NULL);
      //printf("IS PLAYABBLE \n");
      playabool = TRUE;
      if (seekable)
	{
	  g_snprintf (buf, sizeof (buf), "%u:%u:%u", hours, minutes, seconds);
	}
      else
	{
	  g_snprintf (buf, sizeof (buf), "LIVE");
	}
    }
  else
    {
      g_object_set (renderer, "cell-background", "Red", "cell-background-set",
		    TRUE, NULL);
      playabool = FALSE;
      // printf("NOT PLAYABLE \n");
    }


  switch (playable)
    {
    case GST_DISCOVERER_URI_INVALID:
      // g_print ("Invalid URI '%s'\n", uri);
      g_snprintf (buf, sizeof (buf), "Invalid URI");
      break;
    case GST_DISCOVERER_ERROR:
      // g_print ("Discoverer error: %s\n", err->message);
      //  g_snprintf (buf, sizeof (buf), "ERR: %s", err->message);
      g_snprintf (buf, sizeof (buf), "other error");
      break;
    case GST_DISCOVERER_TIMEOUT:
      // g_print ("Timeout\n");
      g_snprintf (buf, sizeof (buf), "Timeout");
      break;
    case GST_DISCOVERER_BUSY:
      // g_print ("Busy\n");
      g_snprintf (buf, sizeof (buf), "Busy");
      break;
    case GST_DISCOVERER_MISSING_PLUGINS:
      {
	const GstStructure *s;
	gchar *str;

//      s = gst_discoverer_info_get_misc (info);
//      str = gst_structure_to_string (s);
	g_snprintf (buf, sizeof (buf), "Missing Plugin");
//      g_print ("Missing plugins: %s\n", str);
	//g_free (str);
	break;
      }
    case GST_DISCOVERER_OK:
      //    g_print ("Discovered '%s'\n", uri);

      break;
    }


  // check if it is currently playing

  GtkTreePath *playingpath;
  GtkTreePath *thispath;
  playingpath = gtk_tree_row_reference_get_path (now_playing);

  thispath = gtk_tree_model_get_path (model, iter);

  if (gtk_tree_path_compare (playingpath, thispath) == 0)
    {
      // g_print("same");
      g_object_set (renderer, "cell-background", "Pink",
		    "cell-background-set", TRUE, NULL);

    }

  // g_print ("now_playing: %s this one: %s \n", gtk_tree_path_to_string (playingpath), gtk_tree_path_to_string(thispath));




  // set the renderer  
  g_object_set (renderer, "text", buf, NULL);

  // g_print(buf);
}

static void
delete_cb ()
{
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (playlist_view));
  GtkTreeIter iter;
  GList *slist;
  slist = gtk_tree_selection_get_selected_rows (selection, NULL);

  GList *l;
  for (l = slist; l != NULL; l = l->next)
    {
      g_print ("delete: %s \n", gtk_tree_path_to_string (l->data));
      gtk_tree_model_get_iter (GTK_TREE_MODEL (playlist_store), &iter,
			       l->data);
      gtk_list_store_remove (GTK_LIST_STORE (playlist_store), &iter);
    }
}

/*add item to playlist */
 
static void
add_cb (GtkButton * button, PlayerData * data)
{
  printf ("add_cb \n");
  playlist_add_item (gtk_entry_get_text (GTK_ENTRY (uri_entrybox)));

}


 /* This function is called when the OPEN button is clicked */
static void
open_cb (GtkButton * button, PlayerData * data)
{

 // printf ("open_cb: not doing anything, fix me! \n");

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button)) == TRUE) 
  {
	//gtk_widget_show(main_hbox);
	gtk_widget_show(file_window);
  //      gtk_widget_hide(video_window);
  }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button)) == FALSE) 
  { 
//	gtk_widget_hide(main_hbox);
	gtk_widget_hide(file_window);
//	gtk_widget_show(video_window);
  }

//  g_object_set (pData->playbin2, "current-audio", 0, NULL);

}

 /* This function is called when the OPEN uri button is clicked */
static void
open_uri_cb (GtkButton * button, PlayerData * data)
{
//  printf ("open_uri_cb: not doing anything, fix me! \n");
  char *playuri; 
  playuri = gtk_entry_get_text (GTK_ENTRY (uri_entrybox));
  playlist_add_item (playuri);
 
/*
  printf ("URI:%s\n", playuri);
  load_uri (playuri);
*/
}


 /* This function is called when the loop checbutton is toggled, this should be done by playlist */

static void
loop_current_cb (GtkCheckButton * loop_current_check, PlayerData * data)
{
  while (TRUE)
    {
      if (loop_current == 0)
	{
	  loop_current = 1;
	  break;
	}
      if (loop_current == 1)
	{
	  loop_current = 0;
	  break;
	}
    }
  printf ("loop_current is %i\n", loop_current);

}

static void 
playlist_load_file (char * file) {
//  printf("playlist_load_file not doing anything");
  return 0;
}

static void
filechooser_file_activated_cb()
{
  char *filename;
  GtkFileChooser *chooser = GTK_FILE_CHOOSER (file_window);
  filename = gtk_file_chooser_get_filename (chooser);
  char *tmp = NULL;
  asprintf (&tmp, "file://%s", filename);
  printf ("\nacitvated filename: %s\n", tmp);
  playlist_add_item(tmp);
  g_free(tmp);

}

static void
filechooser_selection_changed_cb ()
{
  char *filename;
  //g_print ("selection changed");
  GtkFileChooser *chooser = GTK_FILE_CHOOSER (file_window);
  filename = gtk_file_chooser_get_filename (chooser);
  char *tmp = NULL;
  asprintf (&tmp, "file://%s", filename);
  printf ("filename: %s\n", tmp);
  gtk_entry_set_text (GTK_ENTRY (uri_entrybox), tmp);
  g_free (tmp);
  playlist_load_file(tmp);

}


static void
previous_cb ()
{
  playlist_get_previous();
  player_reset_and_play();
}

static void
next_cb ()
{
  playlist_get_next();
  player_reset_and_play();
}



extern GtkWidget *
playlist_init_gtk ()
{

  playlist_store = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING,	//uri
				       G_TYPE_UINT64,	//duration
				       G_TYPE_BOOLEAN,	//seekable
				       G_TYPE_STRING,	//datetime
				       G_TYPE_STRING,	//vcodec
				       G_TYPE_STRING,	//acodec
				       G_TYPE_INT,	//playable
				       G_TYPE_BOOLEAN);	//playing 
  /* some test items in playlist */

/* 
  gtk_list_store_append (playlist_store, &playlist_iter);
  gtk_list_store_set ( playlist_store, &playlist_iter, COLUMN_URI, "file:///home/user/sw.mp4",
                                                       COLUMN_DURATION, "1 UUR",
                                                       COLUMN_SEEKABLE, "YES",
                                                       COLUMN_DATETIME, "thedatandtime",
                                                       COLUMN_VCODEC, "mp4",
                                                       COLUMN_ACODEC, "mp3",
                                                       -1);
 
  gtk_list_store_append (playlist_store, &playlist_iter);
  gtk_list_store_set ( playlist_store, &playlist_iter, COLUMN_URI, "file:///home/user/blaat.mp4", -1);
 
  //playlist_add_item("file:///home/user/sw.mp4");
*/
  /* display the playlist */
  playlist_view = gtk_tree_view_new ();


  GtkWidget *test_button, *loop_current_check, *delete_button, *previous_button, *next_button, *open_button, *add_button, *open_uri_button;
  uri_entrybox = gtk_entry_new ();



  previous_button = gtk_button_new_from_stock (GTK_STOCK_MEDIA_PREVIOUS);
  g_signal_connect (G_OBJECT (previous_button), "clicked",
		    G_CALLBACK (previous_cb), data);

  next_button = gtk_button_new_from_stock (GTK_STOCK_MEDIA_NEXT);
  g_signal_connect (G_OBJECT (next_button), "clicked", G_CALLBACK (next_cb),
		    data);
  open_button = gtk_toggle_button_new_with_label("filebrowser");
  g_signal_connect (G_OBJECT (open_button), "toggled",
		    G_CALLBACK (open_cb), data);

/* to add stuff to the playlist, this should be done by playlist*/
  add_button = gtk_button_new_from_stock (GTK_STOCK_ADD);
  g_signal_connect (G_OBJECT (add_button), "clicked", G_CALLBACK (add_cb),
		    data);


  open_uri_button = gtk_button_new_with_label ("add URI");
  g_signal_connect (G_OBJECT (open_uri_button), "clicked",
		    G_CALLBACK (open_uri_cb), data);

  loop_current_check = gtk_check_button_new_with_label ("loop current video");
  g_signal_connect (G_OBJECT (loop_current_check), "toggled", G_CALLBACK (loop_current_cb),
		    data);

  /*filechooser */
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;

  file_window = gtk_file_chooser_widget_new (action);
  
  g_signal_connect (G_OBJECT (file_window), "selection-changed",
		    G_CALLBACK (filechooser_selection_changed_cb), data);

  g_signal_connect (G_OBJECT (file_window), "file-activated",
		    G_CALLBACK (filechooser_file_activated_cb), data);



  test_button = gtk_button_new_with_label ("test");

  //delete_button = gtk_button_new_with_label("delete");

  /* to add stuff to the playlist */
  delete_button = gtk_button_new_with_mnemonic ("_delete");
  g_signal_connect (G_OBJECT (delete_button), "clicked",
		    G_CALLBACK (delete_cb), data);


  static GtkWidget *playlist_box; 
  static GtkWidget *button_box;
  static GtkWidget *filebrowser_playlist_box;

  playlist_box = gtk_vbox_new(0,FALSE);
  button_box = gtk_hbox_new(0, FALSE);
  filebrowser_playlist_box = gtk_hbox_new(0, FALSE);

  GtkWidget *vscroll_window;
  vscroll_window =  gtk_scrolled_window_new(NULL, NULL);

  gtk_box_pack_start(GTK_BOX (playlist_box), button_box, FALSE,TRUE,0);
  gtk_box_pack_start(GTK_BOX (playlist_box), uri_entrybox, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX (playlist_box), filebrowser_playlist_box, TRUE,TRUE,0);

  gtk_container_add(GTK_CONTAINER(vscroll_window), GTK_WIDGET(playlist_view));


  gtk_box_pack_start (GTK_BOX (filebrowser_playlist_box), file_window, TRUE, TRUE, 0);
  //gtk_box_pack_start (GTK_BOX (button_box), test_button, FALSE, TRUE, 2);
  gtk_box_pack_start (GTK_BOX (button_box), delete_button, FALSE, TRUE, 2);
  gtk_box_pack_start (GTK_BOX (button_box), open_button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (button_box), add_button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (button_box), previous_button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (button_box), next_button, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (button_box), loop_current_check, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (button_box), open_uri_button, FALSE, FALSE, 2);


  gtk_box_pack_start (GTK_BOX (filebrowser_playlist_box), vscroll_window, TRUE, TRUE, 0);


	gtk_widget_hide(file_window);

  gtk_tree_view_set_reorderable (GTK_TREE_VIEW (playlist_view), TRUE);

  GtkCellRenderer *renderer;
  uri_renderer = gtk_cell_renderer_text_new ();
  //duration_renderer = gtk_cell_renderer_new();

// FIXME: the names of the columns dont seem to make any sense!
  seekable_column =
    gtk_tree_view_column_new_with_attributes ("playing", uri_renderer,
					      "text", COLUMN_PLAYING, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (playlist_view),
			       seekable_column);

  uri_column =
    gtk_tree_view_column_new_with_attributes ("URI", uri_renderer, "text",
					      COLUMN_URI, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (playlist_view), uri_column);

  //  duration_column = gtk_tree_view_column_new_with_attributes("duration", uri_renderer, "text", COLUMN_DURATION, NULL); 

  duration_column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (duration_column, "duration");
  gtk_tree_view_append_column (GTK_TREE_VIEW (playlist_view),
			       duration_column);

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (duration_column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func (duration_column, renderer,
					   duration_data_function, NULL,
					   NULL);


  seekable_column =
    gtk_tree_view_column_new_with_attributes ("seekable", uri_renderer,
					      "text", COLUMN_SEEKABLE, NULL);

  gtk_tree_view_append_column (GTK_TREE_VIEW (playlist_view),
			       seekable_column);

  seekable_column =
    gtk_tree_view_column_new_with_attributes ("playable", uri_renderer,
					      "text", COLUMN_PLAYABLE, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (playlist_view),
			       seekable_column);


  datetime_column = gtk_tree_view_column_new_with_attributes("datetime", uri_renderer, "text", COLUMN_DATETIME, NULL); 
  gtk_tree_view_append_column(GTK_TREE_VIEW(playlist_view), datetime_column);

  vcodec_column = gtk_tree_view_column_new_with_attributes("vcodec", uri_renderer, "text", COLUMN_VCODEC, NULL); 
  gtk_tree_view_append_column(GTK_TREE_VIEW(playlist_view), vcodec_column);

  acodec_column = gtk_tree_view_column_new_with_attributes("acodec", uri_renderer, "text", COLUMN_ACODEC, NULL); 
  gtk_tree_view_append_column(GTK_TREE_VIEW(playlist_view), acodec_column);

  gtk_tree_view_set_model (GTK_TREE_VIEW (playlist_view),
			   GTK_TREE_MODEL (playlist_store));

  /* playlist callbacks */
  GtkTreeSelection *playlist_select;

  playlist_select =
    gtk_tree_view_get_selection (GTK_TREE_VIEW (playlist_view));

  gtk_tree_selection_set_mode (playlist_select, GTK_SELECTION_SINGLE);
  g_signal_connect (G_OBJECT (playlist_select), "changed",
		    G_CALLBACK (playlist_selection_changed_cb), NULL);
  /*discoverer */
  discoverer_init ();
#ifdef FP
  /* put some stuff in playlist */
  playlist_add_item ("http://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_480p_stereo.ogg");
  playlist_add_item ("https://archive.org/download/ElephantsDream_628/ElephantsDream_720p_DivXPlus_512kb.mp4");
  playlist_add_item ("https://archive.org/download/marcelo_bronxnet_SegB_201509/segB.m4v"); 

 
#endif

  return playlist_box;
}
