#include "common.h"
//#include "snowbin.h"

  extern GstElement *
create_teebin (GstElement *firstsink, GstElement *secondsink, PlayerData *data)
{
  GstElement *tee, *pipeline;
  GstElement *firstvalve, *secondvalve;
  GstElement *teebin, *q1, *q2;
  GstPad *vpad, *vghostpad;
  GstPad *tee_preview_src, *tee_snowbin_src, *tee_preview_pad,
         *tee_snowbin_pad;
  GstPadTemplate *tee_src_pad_template;

  teebin = gst_bin_new ("teebin");

  tee = gst_element_factory_make ("tee", "tee");

  q1 = gst_element_factory_make ("queue", "q1");
  q2 = gst_element_factory_make ("queue", "q2");
  firstvalve = gst_element_factory_make("valve", "firstvalve");
  secondvalve = gst_element_factory_make("valve", "secondvalve");

  gst_bin_add_many (GST_BIN (teebin), tee, firstsink, secondsink, q1, q2,
      firstvalve, secondvalve,
      NULL);
  gst_element_link_many (tee, q1, firstvalve, firstsink, NULL);
  gst_element_link_many (tee, q2, secondvalve, secondsink , NULL);

  //  g_object_set(q1, "leaky", 1, NULL);
  //  g_object_set(q2, "leaky", 1, NULL); 

  g_object_set(firstvalve, "drop", FALSE, NULL);
  g_object_set(secondvalve, "drop", FALSE, NULL);


  vpad = gst_element_get_static_pad (tee, "sink");
  vghostpad = gst_ghost_pad_new ("sink", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (teebin, vghostpad);
/* how does this work?
  gst_object_unref (firstvalve);
  gst_object_unref (secondvalve);
  gst_object_unref (q1);
  gst_object_unref (q2);
  gst_object_unref (vpad);
  gst_object_unref (vghostpad);
*/
  data->valve = firstvalve;
  return teebin;
}

  extern void 
teebin_preview_on(PlayerData *data)
{
  GstElement *valve;
  printf("turning on preview \n");
//  valve =  gst_bin_get_by_name_recurse_up (GST_BIN(data->playbin2), "firstvalve");
  gst_element_set_state (data->playbin2, GST_STATE_PAUSED);

  g_object_set(data->valve, "drop", FALSE, NULL);

GST_DEBUG_BIN_TO_DOT_FILE(data->playbin2,GST_DEBUG_GRAPH_SHOW_ALL ,"c-player_dump");
  #ifdef GTK
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->preview_check), TRUE); 
  #endif
  gst_element_set_state (data->playbin2, GST_STATE_PLAYING);
  //gst_object_unref(valve);


}

  extern void 
teebin_preview_off(PlayerData *data)
{
  GstElement *valve;
  printf("turning off preview \n");
  valve =  gst_bin_get_by_name_recurse_up (GST_BIN(data->playbin2), "firstvalve");
  gst_element_set_state (data->playbin2, GST_STATE_PAUSED);
  g_object_set(data->valve, "drop", TRUE, NULL);
  gst_element_set_state (data->playbin2, GST_STATE_PLAYING);
  //gst_object_unref(valve);
  #ifdef GTK
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->preview_check), FALSE); 
  #endif

}

