/* s2s, A tool to stream to stream 
 *
 *
 
* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* options:
 * -s send mode 
 * -r revieving mode
 * -p baseport
 */


#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>

//glib stuff
#include <glib.h>
#include <glib/gprintf.h>

// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>

//rtsp 
#ifdef RTSP
#include <gst/rtsp-server/rtsp-server.h>
#endif

#include "s2s.h"
#include "snowbin.h"
#include "teebin.h"
#include "common.h"
#define DEFAULT_LATENCY_MS              50


static GMainLoop *loop = NULL;
gchar *outputType = "xvimagesink"; // output defaults to xvimagesink
char *hostname = "127.0.0.1";
int initPort = 10000; // first portnumber of first stream
gboolean multicast = FALSE;
gboolean guiEnabled = FALSE;
char *multicastAddr = "239.0.0.1";
gchar *input = NULL;
struct rtpbinRecieveData addStream(int streamNumber, bool add_to_end, int baseport);
GstElement *videotestsrc;

typedef struct rtpbinSendData {
  GstBus     *bus;  
  GstElement *pipeline;
  GstElement *vsource, *asource,  *vqueue, *aqueue, *videoscale;
  GstElement *decodebin, *vencoder, *aencoder, *audioresample,  *rtph264pay, *rtpopuspay;
  GstElement *rtpbin, *vudpsink_rtp, *vudpsink_rtcp,*vudpsrc_rtcp, *audpsink_rtp, *audpsink_rtcp,*audpsrc_rtcp;
  GMainLoop  *loop;
} rtpbinSendData; 

typedef struct rtpbinRecieveData {
  gchar *name;
  int streamNumber;

  int baseport;
  GstElement *bin;
  GstElement *pipeline, *vudpsrc_rtp, *vudpsrc_rtcp, *vudpsink_rtcp, *audpsrc_rtp, *audpsrc_rtcp, *audpsink_rtcp;
  GstElement *rtpbin, *rtph264depay, *rtpopusdepay, *queue1, *queue2, *vdecoder, *adecoder, *vsink, *asink;
  GstElement *vrtpqueue, *vrtcpqueue, *artpqueue, *artcpqueue, *vrtptee, *vrtcptee, *artptee, *artcptee, 
	     *vrtpMulticastSink, *vrtcpMulticastSink, *artpMulticastSink, *artcpMulticastSink;
//  GMainLoop  *loop;
  GstPad* srcpad;
  GstCaps *vsrccaps, *asrccaps;

  struct rtpbinRecieveData *next;

} rtpbinRecieveData;
/* 
typedef struct globalData {
  GstElement *videoInput;
  GstElement *audioInput;
} globalData;
*/

static gboolean
bus_call( GstBus *bus,GstMessage *msg, gpointer data )
{
  GMainLoop *loop = ( GMainLoop* )data;
  switch( GST_MESSAGE_TYPE( msg )) {
   case GST_MESSAGE_EOS:
     g_print ( "End-of-stream\n" );
     g_main_loop_quit( loop );
     break;
   case GST_MESSAGE_ERROR: {
     gchar *debug;
     GError* err;
     gst_message_parse_error( msg,&err,&debug );
     g_print ( "Error: %s\n", err ->message );
     g_error_free( err );
     g_main_loop_quit ( loop );
     break;
     }
   default:
    break;
   }
  return TRUE;
}

static void 
on_pad_added ( GstElement *element, GstPad *pad, gpointer data )
{
  GstPad *sinkpad;
  GstElement* elem = ( GstElement* ) data;
//  g_print ( "Dynamic pad created, linking parser/decoder\n" );
  sinkpad = gst_element_get_static_pad( elem , "sink" );
  if (GST_PAD_IS_SRC(pad)) {gst_pad_link ( pad, sinkpad );}
  gst_object_unref( sinkpad );
}

static void 
new_decoded_pad (GstElement *element , GstPad * pad,  rtpbinSendData * data)
{
  GstCaps *caps;
  gchar *str;
  caps = gst_pad_get_current_caps(pad);
  str = gst_caps_to_string(caps);
  printf("pad added\n");
  if (g_str_has_prefix(str,"application/x-rtp")){
    printf("new decoded video pad with caps %s\n", str);

    GstPad *sinkpad;
    sinkpad = gst_element_get_static_pad( data->vqueue , "sink" );
    gst_pad_link ( pad, sinkpad );
    gst_object_unref( sinkpad );

  }
  
  if (g_str_has_prefix(str,"audio/")){
    printf("new decoded audio pad with caps %s\n", str);
    GstPad *sinkpad;
    sinkpad = gst_element_get_static_pad( data->aqueue , "sink" );
    gst_pad_link ( pad, sinkpad );
    gst_object_unref( sinkpad );
 }
}

static void
new_rtpbin_pad(GstElement *element, GstPad *pad, gpointer data)
{
  GstCaps *caps;
  gchar *capsString;
  caps = gst_pad_get_current_caps(pad);
  capsString = gst_caps_to_string(caps);
  printf("pad added with caps %s\n", capsString);
}

static void
stream_timeout( GstElement *rtpbin, guint session, guint ssrc, rtpbinRecieveData *data)
{
  printf("stream number [%d] on baseport %u timed out\n", data->streamNumber, data->baseport);

  int tmpPort = data->baseport;
  int tmpStreamNumber = data->streamNumber;
  removeStream(data->streamNumber,data);
//  struct rtpbinRecieveData tmpData;
//  tmpData.baseport = tmpPort;
//  tmpData.streamNumber = tmpStreamNumber;
  addStream(tmpStreamNumber,true,tmpPort);

//  gst_object_unref( GST_OBJECT ( data->pipeline ) );
//  rtpbinReciever(tmpData);
/*
  sleep(1000);  
  gst_element_set_state ( data->pipeline, GST_STATE_NULL );
  sleep(1000);
  gst_element_set_state ( data->pipeline, GST_STATE_READY );
  sleep(1000); 
  gst_element_set_state ( data->pipeline, GST_STATE_PLAYING );
*/
}
/*
static void
stateChangedCb(GstBus * bus, GstMessage * msg, rtpbinSendData *data)
{
  GstState old_state, new_state, pending_state;
  gst_message_parse_state_changed (msg, &old_state, &new_state,
				   &pending_state);
  GstFormat fmt = GST_FORMAT_TIME;
  GstPad *vpayPad, *apayPad;
  GstCaps *vcaps, *acaps;
  char *vcapsString, *acapsString;


//  printf("\n");
  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (data->pipeline))
    {
   //   data->state = new_state;

      //   g_print ("State set to %s\n", gst_element_state_get_name (new_state));



      switch (new_state)
	{

	case GST_STATE_READY:
//	  printf("state changed to ready");
	  break;

	case GST_STATE_PLAYING:
//	 printf("state playing\n");

	 
         vpayPad = gst_element_get_static_pad(data->rtpbin, "send_rtp_src_0");
         vcaps = gst_pad_get_current_caps(vpayPad);
//         if(vcaps != NULL) 
//	 {
	   vcapsString = gst_caps_to_string(vcaps);
	   printf("videocaps: %s\n", vcapsString);
//         }

         apayPad = gst_element_get_static_pad(data->rtpbin, "send_rtp_src_1");
         acaps = gst_pad_get_current_caps(apayPad);
//         if(acaps != NULL) 
// 	 {
	   acapsString = gst_caps_to_string(acaps);
           printf("audiocaps: %s\n", acapsString);  
//	 }

	 break;
	}
    }
}
*/
static void
transmitV4l()
{
  GstElement *v4l2src;
  GstElement *alsasrc; 

  v4l2src = gst_element_factory_make("v4l2src","v4l2src");
  alsasrc = gst_element_factory_make("alsasrc","alsasrc");

  rtpbinTransmitter(v4l2src,alsasrc);
}
/*
static void 
transmitUri()
{
  GstElement *playbin;
  GstElement *vsink;
  GstElement *asink;

  playbin = gst_element_factory_make("playbin","playbin");
  vsink   = gst_element_factory_make("appsrc", "video-src");
  asink   = gst_element_factory_make("appsrc", "audio-src"); 

  g_object_set(G_OBJECT(playbin), "video-sink", vsink);
  g_object_set(G_OBJECT(playbin), "audio-sink", asink);
}
*/
static void 
transmitGphoto(GMainLoop *loop)
{
  printf("photo init\n");
  char gphotoPath[] = "/usr/bin/";
  char gphotoCmd[] = "gphoto2";
  const char gphotoArgs1[] = "--stdout";
  const char gphotoArgs2[] = "--capture-movie";

  GstElement *gphotobin, *gphotoPipeline;
  GstElement *fdsrc;
  GstElement *alsasrc;
  GstElement *jpegdec;
//  GstElement *vsink, *asink;
//  GstElement *appsrc;
  GstElement *vqueue;
  GstElement *capsfilter;
  GstCaps    *caps; 
//  gphotoPipeline = gst_pipeline_new("gphotopipe");
 
  gphotobin = gst_bin_new("gphotobin");
  vqueue    = gst_element_factory_make("queue","vqueue");
//  aqueue    = gst_element_factory_make("queue","asrcqueue");
  fdsrc     = gst_element_factory_make("fdsrc","fdsrc");
  alsasrc   = gst_element_factory_make("alsasrc","alsasrc");
  jpegdec   = gst_element_factory_make("jpegdec","jpegdec");
//  appsrc    = gst_element_factory_make("xvimagesink","appsrc");
  capsfilter = gst_element_factory_make ("capsfilter", "capsfilter"); 

  caps= gst_caps_new_simple("image/jpeg", 
			    "parsed",G_TYPE_BOOLEAN,"true", 
			    "format",G_TYPE_STRING,"UYVY", 
			    "width",G_TYPE_INT,640, 
			    "height",G_TYPE_INT,480, 
	 	  	    "framerate",GST_TYPE_FRACTION,1,1,
			    NULL);

  g_object_set (G_OBJECT (capsfilter), "caps", caps, NULL); 
 
  g_object_set(G_OBJECT(jpegdec),"idct-method",2,NULL);
  g_object_set (G_OBJECT (alsasrc), "buffer-time",18000,NULL);
  g_object_set (G_OBJECT (alsasrc), "do-timestamp",true, NULL);

  int filedes[2];
  if (pipe(filedes) == -1) 
  {
    perror("pipe");
    exit(1);
  }

  pid_t pid = fork();
  if (pid == -1) 
  {
    perror("fork");
    //exit(1);
  } 
  else if (pid == 0) 
  {
    while ((dup2(filedes[1], STDOUT_FILENO) == -1) && (errno == EINTR)) {}
    close(filedes[1]);
    close(filedes[0]);
    printf("executing gphoto\n");
    execl("/usr/bin/gphoto2", "gphoto2", "--stdout", "--capture-movie", (char*)0);
    perror("execl");
    //_exit(1);
  }
  
  close(filedes[1]);

  printf("fd: %i\n", filedes[0]);

  g_object_set( G_OBJECT (fdsrc), "fd", filedes[0], NULL);
/*
  gst_bin_add_many(GST_BIN(gphotoPipeline), fdsrc, jpegdec,appsrc, NULL);
  if( !gst_element_link_many(fdsrc,jpegdec,appsrc,NULL)) { printf ("failed to link gphoto shit\n");}
  gst_element_set_state ( gphotoPipeline, GST_STATE_PLAYING );
  g_main_loop_run(loop);
*/
  gst_bin_add_many(GST_BIN(gphotobin), capsfilter, fdsrc, vqueue, jpegdec, NULL);
  if( !gst_element_link_many(fdsrc, capsfilter, jpegdec,NULL)) { printf ("failed to link gphoto shit\n");}
  GstPad *vpad, *vghostpad;
  vpad = gst_element_get_static_pad (jpegdec, "src");
  vghostpad = gst_ghost_pad_new ("src", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (gphotobin, vghostpad);


  rtpbinTransmitter(gphotobin,alsasrc);
}

static void
transmitDisplay()
{
  GstElement *displaybin;
  GstElement *ximagesrc;
  GstElement *videoconvert,*videoscale, *capsfilter, *videomixer;
  GstElement *alsasrc;
  //GstElement *vsink;
  GstCaps    *caps;
  printf("transmit display\n");
  
  displaybin   = gst_bin_new("displaybin");
  ximagesrc    = gst_element_factory_make("ximagesrc", "ximagesrc");
  videoscale   = gst_element_factory_make("videoscale", "videoscale");
  videoconvert = gst_element_factory_make("videoconvert","videoconvert"); 
//  vsink	       = gst_element_factory_make("xvimagesink", "xvimagesink");  
  capsfilter   = gst_element_factory_make("capsfilter", "capsfilter");
  videomixer   = gst_element_factory_make("videomixer", "videomixer");

  g_object_set( G_OBJECT (ximagesrc),"display-name", ":0", NULL);
  g_object_set( G_OBJECT (ximagesrc),"remote", true, NULL);
  g_object_set( G_OBJECT (ximagesrc),"typefind", true, NULL);

  caps = gst_caps_new_simple("video/x-raw",
			     "width",G_TYPE_INT,640,
			     "height",G_TYPE_INT,480, 
			     "framerate",GST_TYPE_FRACTION, 15,1,
			     NULL);

  g_object_set (G_OBJECT (capsfilter), "caps", caps, NULL);

  gst_bin_add_many(GST_BIN(displaybin),ximagesrc, videomixer, videoscale, videoconvert, capsfilter, NULL);
  gst_element_link_many(ximagesrc, videomixer, videoscale, videoconvert, capsfilter, NULL);

  GstPad *vpad, *vghostpad;
  vpad = gst_element_get_static_pad (capsfilter, "src");
  vghostpad = gst_ghost_pad_new ("src", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (displaybin, vghostpad);


  rtpbinTransmitter(displaybin,NULL);
}

static int 
rtpbinTransmitter(GstElement *vsource, GstElement *asource)
{
  printf("Initializing transmisson... \n");
  printf("Host: %s\nPort:%u\n", hostname, initPort);
  gboolean ret =  FALSE;

  rtpbinSendData data; 

  gst_init ( NULL, NULL );
//  data.loop = g_main_loop_new( NULL,FALSE );
  loop = g_main_loop_new( NULL,FALSE );

  data.pipeline      = gst_pipeline_new( "video-send" );
  data.bus	     = gst_element_get_bus(data.pipeline); 
  if (NULL == vsource)
  {
    data.vsource       = gst_element_factory_make ( "videotestsrc", "videotestsrc");
    g_object_set( G_OBJECT ( data.vsource ), "is-live", true, NULL);  
  }
  else {data.vsource = vsource;}

  if (NULL == asource)
  {
    data.asource	  = gst_element_factory_make ( "audiotestsrc", "audiotestsrc");
    g_object_set( G_OBJECT ( data.asource ), "is-live", true, NULL);
  }
  else {data.asource = asource;}
  
//  data.decodebin     = gst_element_factory_make ( "decodebin", "decodebin" );
  data.vqueue        = gst_element_factory_make ( "queue", "vqueue" );
  data.aqueue        = gst_element_factory_make ( "queue", "aqueue" );
  data.vencoder      = gst_element_factory_make ( "x264enc", "vencoder" );
  data.audioresample = gst_element_factory_make ( "audioresample", "audioresample");
  data.aencoder      = gst_element_factory_make ( "opusenc", "aencoder" );
  data.rtph264pay    = gst_element_factory_make ( "rtph264pay", "rtph264pay" );
  data.rtpopuspay    = gst_element_factory_make ( "rtpopuspay", "rtpopuspay" );
  data.rtpbin	     = gst_element_factory_make ( "rtpbin", "rtpbin" );
  data.vudpsink_rtp  = gst_element_factory_make ( "udpsink", "vudpsink-send data" );
  data.vudpsink_rtcp = gst_element_factory_make ( "udpsink", "vudpsink-rtcp" );
  data.vudpsrc_rtcp  = gst_element_factory_make ( "udpsrc", "vudpsrc-rtcp" );
  data.audpsink_rtp  = gst_element_factory_make ( "udpsink", "audpsink-send data" );
  data.audpsink_rtcp = gst_element_factory_make ( "udpsink", "audpsink-rtcp" );
  data.audpsrc_rtcp  = gst_element_factory_make ( "udpsrc", "audpsrc-rtcp" );
  
  if( !data.pipeline || !data.vqueue || !data.aqueue || !data.audioresample ||
      !data.vencoder ||  !data.aencoder || !data.rtph264pay || !data.rtpopuspay || !data.rtpbin || 
      !data.vudpsink_rtp || !data.vudpsink_rtcp || !data.vudpsrc_rtcp ||
      !data.audpsink_rtp || !data.audpsink_rtcp || !data.vudpsrc_rtcp ) { 
	g_print ( "One element could not be created \n" );
  	return -1;
  }
  
//  g_object_set( G_OBJECT ( data.source ), "location", "/home/yids/src/cpu/testclips/sound_in_sync_test.mp4", NULL);
// g_object_set( G_OBJECT ( data.source ), "location", "/home/yids/Downloads/Better.Call.Saul.S01E05.HDTV.x264-LOL[ettv]/better.call.saul.105.hdtv-lol.mp4", NULL);



//  g_object_set( G_OBJECT ( data.rtph264pay), "pt", 100, NULL); 
  g_object_set( G_OBJECT ( data.vencoder ), "bitrate", 512, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "byte-stream", false, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "pass", 5, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "quantizer", 45, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "speed-preset", 8, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "tune", 0x00000004, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "sliced-threads", true, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "rc-lookahead", 20, NULL);
  g_object_set( G_OBJECT ( data.vencoder ), "psy-tune", 1, NULL);

  g_object_set( G_OBJECT ( data.vudpsink_rtp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtp ), "port", initPort, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtcp ), "host",hostname, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtcp ), "port", initPort+1, NULL );
  g_object_set( G_OBJECT ( data.vudpsink_rtcp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.vudpsrc_rtcp ), "port", initPort+2, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtp ), "port", initPort+3, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtcp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtcp ), "port", initPort+4, NULL );
  g_object_set( G_OBJECT ( data.audpsink_rtcp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( data.audpsrc_rtcp ), "port", initPort+5, NULL ); 

//  gst_bus_add_watch( gst_pipeline_get_bus( GST_PIPELINE( data.pipeline ) ),
//  		bus_call, &data );

  gst_bin_add_many( GST_BIN( data.pipeline ), data.vsource, data.asource, data.vqueue, data.aqueue,
  	data.vencoder, data.aencoder, data.audioresample, data.rtph264pay, data.rtpopuspay, data.rtpbin, 
        data.vudpsink_rtp, data.vudpsink_rtcp, data.vudpsrc_rtcp,      
        data.audpsink_rtp, data.audpsink_rtcp, data.audpsrc_rtcp, NULL);
//  ret = gst_element_link_many( source, ffmpegcs, queue, tee, NULL );
//  ret = gst_element_link_many( tee,  queuesave, /*decoder,*/ffmpegcsd, sink, NULL );
 
//  gst_bin_add_many( GST_BIN( pipeline ), rtph264pay,rtpbin,udpsink_rtp, udpsink_rtcp, udpsrc_rtcp, NULL );
//  if ( !gst_element_link_many( data.vsource, data.vencoder, NULL )) {printf("failed to link  source to decodebin\n");}
//  g_signal_connect( data.rtpbin, "pad-added", G_CALLBACK( new_rtpbin_pad ), &data );

  if ( !gst_element_link_many (data.vsource, data.vqueue, data.vencoder, data.rtph264pay, NULL)) {printf("failed to link encoder to h264 payloader\n");}
  if ( !gst_element_link_pads_filtered( data.rtph264pay, "src", data.rtpbin, "send_rtp_sink_0", NULL )) {printf("failed to link payloader to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtp_src_0", data.vudpsink_rtp, "sink", NULL )) {printf("failed to link rtpbin to rtp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtcp_src_0", data.vudpsink_rtcp,"sink", NULL )) {printf("failed to link rtpbin to rtcp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.vudpsrc_rtcp, "src", data.rtpbin, "recv_rtcp_sink_0", NULL )) {printf("failed to link rtcp udpsrc to rtpbin\n");}
  
//  g_signal_connect( data.rtpbin, "pad-added", G_CALLBACK( new_decoded_pad ), aencoder  );
  if ( !gst_element_link_many (data.asource, data.audioresample ,data.aencoder, data.rtpopuspay, NULL)) {printf("failed to link encoder to opus payloader");} 
  if ( !gst_element_link_pads_filtered( data.rtpopuspay, "src", data.rtpbin, "send_rtp_sink_1", NULL )) {printf("failed to link payloader to rtpbin\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtp_src_1", data.audpsink_rtp, "sink", NULL )) {printf("failed to link rtpbin to rtp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.rtpbin, "send_rtcp_src_1", data.audpsink_rtcp,"sink", NULL )) {printf("failed to link rtpbin to rtcp udpsink\n");}
  if ( !gst_element_link_pads_filtered( data.audpsrc_rtcp, "src", data.rtpbin, "recv_rtcp_sink_1", NULL )) {printf("failed to link rtcp udpsrc to rtpbin\n");}

/* add bus signal watch */ 
  gst_bus_add_signal_watch(data.bus);

/* connect signals */
  printf("\n");
//  g_signal_connect (G_OBJECT (data.bus), "message::state-changed", (GCallback) stateChangedCb, &data);

/* start playback */
 
  g_print ( "Setting pipeline to playing...\n" );
  gst_element_set_state ( data.pipeline, GST_STATE_PLAYING );
//  gst_element_set_locked_state( data.vudpsink_rtcp, TRUE );
  
//  gchar *dump_name = g_strconcat ("gst-launch.", "lala" , NULL);
//  GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (data.pipeline), GST_DEBUG_GRAPH_SHOW_ALL, dump_name); 

 
//  g_main_loop_run( data.loop );
  g_main_loop_run( loop );  
  
  
  g_print ( "Finished ,stopping playback\n" );
  gst_element_set_state( data.pipeline, GST_STATE_NULL );
  
  g_print ( "Deleting pipeline\n" );
  gst_object_unref( GST_OBJECT ( data.pipeline ) );
  
  return 0;  
}


extern int 
rtpbinReciever(struct rtpbinRecieveData *data)
{
  gboolean ret =  FALSE;
  gchar* name;
  char buff[1024];
 FILE* fp = NULL;
  int len = 0;
  
  printf("starting reciever on baseport %u\n",data->baseport); 

//  rtpbinRecieveData data;

  gchar *vcaps = "application/x-rtp,media=(string)video,clock-rate=(int)90000,encoding-name=(string)H264"; 
  gchar *acaps = "application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00"; 
  
//  data->loop = g_main_loop_new( NULL,FALSE );
 
  data->bin	       = gst_bin_new("currentBin");
 
  data->pipeline       = gst_pipeline_new( "video-recv" );//dshowvideosrc
  data->vudpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp video" );
  data->vudpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp video" );
  data->vudpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp video " );
  data->audpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp audio" );
  data->audpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp audio" );
  data->audpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp audio " );
  data->rtpbin         = gst_element_factory_make ( "rtpbin", "gstrtpbin" );
  data->rtph264depay   = gst_element_factory_make ( "rtph264depay", "rtph264depay" );
  data->rtpopusdepay   = gst_element_factory_make ( "rtpopusdepay", "rtpopusdepay" );
  data->queue1         = gst_element_factory_make ( "queue", "queue1" );
  data->queue2         = gst_element_factory_make ( "queue", "queue2" );
  data->vdecoder       = gst_element_factory_make ( "avdec_h264", "vdecoder" );
  data->adecoder       = gst_element_factory_make ( "opusdec", "adecoder" );
  data->asink	       = gst_element_factory_make ( "alsasink", "alsasink" );


/* to send it to screen */
  if (strncmp(outputType,"xvimagesink",strlen(outputType)) == 0){
     data->vsink = gst_element_factory_make ( "xvimagesink", "xvimagesink" );//directdrawsink
     g_object_set( G_OBJECT ( data->vsink), "sync", false, NULL);
  }

/* to send it to snowmix  */
  if (strncmp(outputType,"snowbin",strlen(outputType)) == 0){ 
    char tmp[6];
    sprintf(tmp, "feed%d", data->streamNumber+1);  
    printf("IN MAIN control pipe: %s\n",tmp);
    data->vsink	        = get_snowvideobin(tmp);
  //  g_free(tmp);
  }
 

  g_object_set( G_OBJECT ( data->vudpsrc_rtp ),   "port", data->baseport, NULL );
  g_object_set( G_OBJECT ( data->vudpsrc_rtcp ),  "port", data->baseport+1, NULL );
  g_object_set( G_OBJECT ( data->vudpsink_rtcp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( data->vudpsink_rtcp ), "port", data->baseport+2, NULL );

  g_object_set( G_OBJECT ( data->audpsrc_rtp ),   "port", data->baseport+3, NULL );
  g_object_set( G_OBJECT ( data->audpsrc_rtcp ),  "port", data->baseport+4, NULL );
  g_object_set( G_OBJECT ( data->audpsink_rtcp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( data->audpsink_rtcp ), "port", data->baseport+5, NULL );
 
  g_object_set( G_OBJECT ( data->rtpbin ), "autoremove", TRUE, NULL );
  g_object_set( G_OBJECT ( data->rtpbin ), "do-sync-event", TRUE, NULL ); 
  g_object_set( G_OBJECT ( data->rtpbin ), "async-handling", TRUE, NULL );


/* setup multicast if enabled */
  if (multicast){
    printf("multicast init\n");
    data->vrtptee	      = gst_element_factory_make( "tee", "vrtptee");
    data->vrtcptee            = gst_element_factory_make( "tee", "vrtcptee");
    data->artptee	      = gst_element_factory_make( "tee", "artptee");
    data->artcptee            = gst_element_factory_make( "tee", "artcptee");
    data->vrtpqueue	      = gst_element_factory_make( "queue", "vrtpqueue");
    data->vrtcpqueue          = gst_element_factory_make( "queue", "vrtcpqueue");
    data->artpqueue	      = gst_element_factory_make( "queue", "artpqueue");
    data->artcpqueue          = gst_element_factory_make( "queue", "artcpqueue");
    data->vrtpMulticastSink   = gst_element_factory_make( "udpsink", "vmulticastrtpudpsink");
    data->vrtcpMulticastSink  = gst_element_factory_make( "udpsink", "vmulticastrtcpudpsink");
    data->artpMulticastSink   = gst_element_factory_make( "udpsink", "amulticastrtpudpsink");
    data->artcpMulticastSink  = gst_element_factory_make( "udpsink", "amulticastrtcpudpsink");

    gst_bin_add_many( GST_BIN( data->pipeline ), data->vudpsrc_rtp, data->vudpsrc_rtcp, data->vudpsink_rtcp,
					      data->audpsrc_rtp, data->audpsrc_rtcp, data->audpsink_rtcp,
				              data->rtpbin, data->rtph264depay, data->rtpopusdepay, data->queue1, 
					      data->queue2, data->vdecoder, data->adecoder, data->vsink, data->asink, NULL);
						
    gst_bin_add_many(GST_BIN(data->pipeline), data->vrtptee, data->vrtcptee, data->artptee, data->artcptee,
					      data->vrtpqueue, data->vrtcpqueue, data->artpqueue, data->artcpqueue,
                                              data->vrtpMulticastSink, data->vrtcpMulticastSink, 
					      data->artpMulticastSink, data->artcpMulticastSink, NULL);

    g_object_set(G_OBJECT(data->vrtpMulticastSink), "host", "239.100.0.1", NULL);
    g_object_set(G_OBJECT(data->vrtpMulticastSink), "port", 10010, NULL);
    g_object_set(G_OBJECT(data->vrtcpMulticastSink), "host", "239.100.0.1", NULL);
    g_object_set(G_OBJECT(data->vrtcpMulticastSink), "port", 10011, NULL);

    g_object_set(G_OBJECT(data->artpMulticastSink), "host", "239.100.0.1", NULL);
    g_object_set(G_OBJECT(data->artpMulticastSink), "port", 10012, NULL);
    g_object_set(G_OBJECT(data->artcpMulticastSink), "host", "239.100.0.1", NULL);
    g_object_set(G_OBJECT(data->artcpMulticastSink), "port", 10013, NULL);

 
    if(!gst_element_link(data->vudpsrc_rtp, data->vrtptee)){printf("failed to link vudpsrc to vrtptee\n");}
    if(!gst_element_link(data->vudpsrc_rtcp, data->vrtcptee)){printf("failed to link vudpsrc to vrtcptee\n");}
      
    if(!gst_element_link(data->audpsrc_rtp, data->artptee)){printf("failed to link audpsrc to artptee\n");}
    if(!gst_element_link(data->audpsrc_rtcp, data->artcptee)){printf("failed to link audpsrc to artcptee\n");}
    

    if(!gst_element_link_many (data->vrtptee, data->vrtpqueue, data->vrtpMulticastSink, NULL)){printf("failed to link vrtptee, vq or vmultisink\n");}
    if(!gst_element_link_many (data->vrtcptee, data->vrtcpqueue, data->vrtcpMulticastSink, NULL)){printf("failed to link vrtcptee, vq or vmultisink\n");}
    
    if(!gst_element_link_many (data->artptee, data->artpqueue, data->artpMulticastSink, NULL)){printf("failed to link artcptee, aq or amultisink\n");}
    if(!gst_element_link_many (data->artcptee, data->artcpqueue, data->artcpMulticastSink, NULL)){printf("failed to link artcptee, aq or amultisink\n");}
 
    if ( !gst_element_link_pads_filtered( data->vrtptee, "src_%u", data->rtpbin, "recv_rtp_sink_%u", NULL )) { printf("failed to link rtp vtee to rtpbin\n");}
    if ( !gst_element_link_pads_filtered( data->vrtcptee, "src_%u", data->rtpbin, "recv_rtcp_sink_%u", NULL )) { printf("failed to link rtcp vtee to rtpbin\n");}


    if ( !gst_element_link_pads_filtered( data->artptee, "src_%u", data->rtpbin, "recv_rtp_sink_%u", NULL )) { printf("failed to link rtp atee to rtpbin\n");}
    if ( !gst_element_link_pads_filtered( data->artcptee, "src_%u", data->rtpbin, "recv_rtcp_sink_%u", NULL )) { printf("failed to link rtcp atee to rtpbin\n");}

}  // if multicast end

  if(!multicast){
    gst_bin_add_many( GST_BIN( data->pipeline ), data->vudpsrc_rtp, data->vudpsrc_rtcp, data->vudpsink_rtcp,
					      data->audpsrc_rtp, data->audpsrc_rtcp, data->audpsink_rtcp,
				              data->rtpbin, data->rtph264depay, data->rtpopusdepay, data->queue1, 
					      data->queue2, data->vdecoder, data->adecoder, data->vsink, data->asink, NULL);
  }	  

  if( !data->pipeline || !data->vudpsrc_rtp || !data->vudpsrc_rtcp || !data->vudpsink_rtcp ||
      !data->audpsrc_rtp || !data->audpsrc_rtcp || !data->audpsink_rtcp || !data->rtpbin || 
      !data->rtph264depay || !data->rtpopusdepay || !data->queue1 || !data->queue2 || !data->vdecoder ||  
      !data->adecoder || !data->vsink || !data->asink  ) {
  	g_print ( "One element could not be created \n" );
  	return -1;
  }
  
//  printf("rtpport: %d, rtc srcport: %d, rtcp sinkport: %d\n", baseport, baseport+1, baseport+2);

//  srcpad = gst_element_get_static_pad( udpsrc_rtp, "src" );
//  srccaps = gst_pad_get_caps( srcpad );
//  name = gst_caps_to_string( srccaps );
/*
  fp = fopen( "video.caps", "rb" );
  if( !fp ) {
  	printf( "can't open video.caps\n" );
  	return -1;
  }
  
  len = fread( buff, sizeof( char ), sizeof( buff ), fp );
  buff[0x108] = '\0';
*/
  data->vsrccaps = gst_caps_from_string( vcaps );
  printf("video caps: %s\n", vcaps);
  g_object_set( G_OBJECT ( data->vudpsrc_rtp ), "caps", GST_CAPS(data->vsrccaps) , NULL );
  
  data->asrccaps = gst_caps_from_string( acaps );
  printf("audio caps: %s\n", acaps);
  g_object_set( G_OBJECT ( data->audpsrc_rtp ), "caps", GST_CAPS(data->asrccaps) , NULL );
/*  
  gst_bin_add_many( GST_BIN( data->pipeline ), data->vudpsrc_rtp, data->vudpsrc_rtcp, data->vudpsink_rtcp,
					      data->audpsrc_rtp, data->audpsrc_rtcp, data->audpsink_rtcp,
				              data->rtpbin, data->rtph264depay, data->rtpopusdepay, data->queue1, 
					      data->queue2, data->vdecoder, data->adecoder, data->vsink, data->asink, NULL);
*/
  if(!multicast){
    if ( !gst_element_link_pads_filtered( data->vudpsrc_rtp, "src", data->rtpbin, "recv_rtp_sink_%u", NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}
    if ( !gst_element_link_pads_filtered( data->vudpsrc_rtcp, "src", data->rtpbin, "recv_rtcp_sink_%u", NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}
  }
  if ( !gst_element_link_pads_filtered( data->rtpbin, "send_rtcp_src_%u", data->vudpsink_rtcp, "sink", NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}
  g_signal_connect( data->rtpbin, "pad-added", G_CALLBACK( on_pad_added ), data->rtph264depay  );

  if(!multicast){
    if ( !gst_element_link_pads_filtered( data->audpsrc_rtp, "src", data->rtpbin, "recv_rtp_sink_%u", NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}
    if ( !gst_element_link_pads_filtered( data->audpsrc_rtcp, "src", data->rtpbin, "recv_rtcp_sink_%u", NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}
  }
  if ( !gst_element_link_pads_filtered( data->rtpbin, "send_rtcp_src_%u", data->audpsink_rtcp, "sink", NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}
  g_signal_connect( data->rtpbin, "pad-added", G_CALLBACK( on_pad_added ), data->rtpopusdepay  );
  
  if ( !gst_element_link_many( data->rtph264depay, data->queue1, data->vdecoder,  data->vsink, NULL )) { printf("failed to link rtph264depay, queue, vdecoder,  vsink\n");}
  if ( !gst_element_link_many( data->rtpopusdepay, data->queue2, data->adecoder,  data->asink, NULL )) { printf("failed to link rtpopusdepay, queue, adecoder,  asink\n");}

  g_signal_connect( data->rtpbin, "on-timeout", G_CALLBACK( stream_timeout ), data);

//   g_print ( "Setting to recv video\n" );

  gst_element_set_state ( data->pipeline, GST_STATE_PAUSED );
  gst_element_set_state ( data->pipeline, GST_STATE_PLAYING );

//  g_main_loop_run( data->loop );
  
//  g_print ( "returned ,stopping playback\n" );

//  g_print ( "Deleting pipeline\n" );
//  gst_object_unref( GST_OBJECT ( data->pipeline ) );



return 0;

}

/* ------------------------------------ linked list test ----------------------------------------------*/

static struct rtpbinRecieveData *head = NULL;
static struct rtpbinRecieveData *curr = NULL;

struct rtpbinRecieveData createList(int streamNumber, int baseport)
{
  printf("\n starting recieving pipeline number [%d], on baseport [%d]\n",streamNumber,baseport);
  struct rtpbinRecieveData *ptr = (struct rtpbinRecieveData*)malloc(sizeof(struct rtpbinRecieveData));
  if(NULL == ptr)
  {
      printf("\n Node creation failed \n");
      //return NULL;
  }
  ptr->streamNumber = streamNumber;
  ptr->baseport = baseport;
  
  rtpbinReciever(ptr);
  ptr->next = NULL;

  head = curr = ptr;
  return *ptr;
}

struct rtpbinRecieveData addStream(int streamNumber, bool add_to_end, int baseport)
{
  if(NULL == head)
  {
      return (createList(streamNumber, baseport));
  }

  if(add_to_end)
      printf("\n starting recieving pipeline number [%d], on baseport [%d]\n",streamNumber,baseport);
  else
      printf("\n Adding node to beginning of list with streamNumberue [%d]\n",streamNumber);

  struct rtpbinRecieveData *ptr = (struct rtpbinRecieveData*)malloc(sizeof(struct rtpbinRecieveData));
  if(NULL == ptr)
  {
      printf("\n pipeline creation failed \n");
      //return NULL;
  }
  ptr->streamNumber = streamNumber;
  ptr->baseport = baseport;
//  printf("baseport: %d\n", baseport);
  rtpbinReciever(ptr);
  ptr->next = NULL;
/*
  if(add_to_end)
  {
      curr->next = ptr;
      curr = ptr;
  }
  else
  {
      ptr->next = head;
      head = ptr;
  }
*/
  return *ptr;
}

struct rtpbinRecieveData* searchStream(int streamNumber, struct rtpbinRecieveData **prev)
{
  struct rtpbinRecieveData *ptr = head;
  struct rtpbinRecieveData *tmp = NULL;
  bool found = false;

  printf("\n Searching the list for streamNumberue [%d] \n",streamNumber);

  while(ptr != NULL)
  {
      if(ptr->streamNumber == streamNumber)
      {
          found = true;
          break;
      }
      else
      {
          tmp = ptr;
          ptr = ptr->next;
      }
  }

  if(true == found)
  {
      if(prev)
          *prev = tmp;
      return ptr;
  }
  else
  {
      return NULL;
  }
}

int removeStream(int streamNumber, rtpbinRecieveData *data)
{
  struct rtpbinRecieveData *prev = NULL;
  struct rtpbinRecieveData *del = NULL;

  printf("\n Removing stream [%d] \n",streamNumber);

  del = searchStream(streamNumber,&prev);
  gst_element_set_state ( data->pipeline, GST_STATE_PAUSED );
  gst_object_unref(del->pipeline); 
  if(del == NULL)
  {
      return -1;
  }
  else
  {
      if(prev != NULL)
          prev->next = del->next;

      if(del == curr)
      {
          curr = prev;
      }
      else if(del == head)
      {
          head = del->next;
      }
  }

  free(del);
//  del = NULL;

  return 0;
}

void listStreams(void)
{
  struct rtpbinRecieveData *ptr = head;

  printf("\n -------Printing list Start------- \n");
  while(ptr != NULL)
  {
      printf("\n [%d] \n",ptr->streamNumber);
      printf("baseport\n [%d] \n",ptr->baseport);
      ptr = ptr->next;
  }
  printf("\n -------Printing list End------- \n");

  return;
}

static void 
launchPipeplines(int numberOfPipelines)
{
    int i = 0; 
//    struct rtpbinRecieveData *ptr = NULL;

    for(i = 0; i<numberOfPipelines; i++)
     addStream(i,true,initPort+i*6);

//      addStream(1,true,10006);
//    listStreams();


}

/* ------------------------------------ linked list test end ----------------------------------------------*/

/* ------------------------------------ rtsp server client ----------------------------------------------*/
#ifdef RTSP
#define DEFAULT_RTSP_PORT "8554"

static char *port = (char *) DEFAULT_RTSP_PORT;

static GOptionEntry entries[] = {
  {"port", 'p', 0, G_OPTION_ARG_STRING, &port,
      "Port to listen on (default: " DEFAULT_RTSP_PORT ")", "PORT"},
  {NULL}
};

static void
startServer()
{
  printf("starting rtsp server\n");

  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMountPoints *mounts;
  GstRTSPMediaFactory *factory;
  GOptionContext *optctx;
  GError *error = NULL;
  
  optctx = g_option_context_new ("<launch line> - Test RTSP Server, Launch\n\n"
      "Example: \"( decodebin name=depay0 ! autovideosink )\"");
  g_option_context_add_main_entries (optctx, entries, NULL);
  g_option_context_add_group (optctx, gst_init_get_option_group ());
/*  if (!g_option_context_parse (optctx, &argc, &argv, &error)) {
    g_printerr ("Error parsing options: %s\n", error->message);
    return -1;
  }
*/ 
  g_option_context_free (optctx);
  
  loop = g_main_loop_new (NULL, FALSE);
  
  /* create a server instance */
  server = gst_rtsp_server_new ();
  
  /* get the mount points for this server, every server has a default object
   * that be used to map uri mount points to media factories */
  mounts = gst_rtsp_server_get_mount_points (server);
  
  /* make a media factory for a test stream. The default media factory can use
   * gst-launch syntax to create pipelines.
   * any launch line works as long as it contains elements named depay%d. Each
   * element with depay%d names will be a stream */
  factory = gst_rtsp_media_factory_new ();
  gst_rtsp_media_factory_set_transport_mode (factory,
      GST_RTSP_TRANSPORT_MODE_RECORD);
  gst_rtsp_media_factory_set_launch (factory, "decodebin ! xvimagesink");
  gst_rtsp_media_factory_set_latency (factory, 2000);
  
  /* attach the test factory to the /test url */
  gst_rtsp_mount_points_add_factory (mounts, "/test", factory);
  
  /* don't need the ref to the mapper anymore */
  g_object_unref (mounts);
  
  /* attach the server to the default maincontext */
  gst_rtsp_server_attach (server, NULL);
  
  /* start serving */
  g_print ("stream ready at rtsp://127.0.0.1:%s/test\n", port);
  g_main_loop_run (loop);
  

  
}

static void
uripadadded()
{
  printf("uripad added\n");
}

static void
startClient()
{
  printf("starting rtsp client\n");
//  GstRTSPConnection *connection;
//  GstRTSPUrl *url;
//  gchar *urlPlaintext = "rtsp://127.0.0.1:8554/test";
 
//  gst_rtsp_url_parse(urlPlaintext, &url);
  
//  gst_rtsp_connection_create(url,&connection);
//  GstElement *pipeline, *vsource, *asource, *venc, *aenc, *apay, *vpay, *uridecodebin;

//  pipeline     = gst_pipeline_new("pipeline");
//  uridecodebin = gst_element_factory_make("uridecodebin", "uridecodebin"); 
/*  vsource      = gst_element_factory_make("videotestsrc", "videotestsrc");
  asource      = gst_element_factory_make("audiotestsrc", "audiotestsrc");
  venc         = gst_element_factory_make("x264enc", "x264enc");
  aenc	       = gst_element_factory_make("opusenc", "opusenc");
*/
//  g_object_set(uridecodebin, "uri", "rtsp://127.0.0.1:8554/test",NULL);
//  g_signal_connect(uridecodebin, "pad-added",G_CALLBACK(uripadadded), NULL);
  
  
}
#endif 
/* ------------------------------------ GUI stuff ----------------------------------------------- */
static void
guiInit()
{

}

/* ------------------------------------ GUI stuff end ----------------------------------------------- */

static void
usage()
{
  printf("\nInvalid option received  \n -s start in server mode \n -n number of streams to recieve \n -o output (xvimagesink or snowbin) \n -m enable multicast \n -p baseport\n\n");
  printf(" -c client mode \n -h hostname \n -p baseport\n\n");
  printf("-r rtsp mode (not working\n");
  exit(1);
} 

int
main (int argc, char *argv[])
{
  bool server = FALSE;
  bool rtsp = FALSE;
   printf("starting s2s, delivering your streams recursively \n");
/* Initialize GTK */
//  gtk_init (&argc, &argv);

  /* Initialize GStreamer */
  gst_init (&argc, &argv);
  loop = g_main_loop_new( NULL,FALSE );
  
  //globalData global; 
  //global.videoInput = gst_element_factory_make ("videotestsrc", "videotestsrc");
  //global.audioInput = gst_element_factory_make ("audiotestsrc", "audiotestsrc"); 
//  test_list();
  struct rtpbinRecieveData current_data, new_data;
  videotestsrc = gst_element_factory_make("videotestsrc", "videotestsrc"); 
//  int baseport = 10000;

  int opt=0;
  int numPipes; 

//  if(argc<2){usage();}

  while ((opt = getopt (argc, argv, "csh:p:o:n:mrgi:t")) != -1)
    {
      switch (opt)
        {
        case 'c':
          printf("starting in sending mode\n");
          break;
        case 's':
  	  server = TRUE;  
          break;
       case 'h':
	  printf("Host: %s\n", optarg);
          hostname = optarg;
          break;
        case 'p':
	  printf("Baseport: %s\n", optarg);
          initPort = atoi(optarg);
          break;
        case 'o':
	  printf("Output: %s\n", optarg);
	  outputType = optarg;
	  break;
        case 'm':
	  printf("Enabling multicast.\n");
	  multicast = TRUE;
	  break;
        case 'n':
	  printf("Number of pipelines: %s \n", optarg);
	  numPipes = atoi(optarg);
 	  break;
	case 'r':
	  rtsp = TRUE;
	  break;
        case 'g':
	  guiEnabled = TRUE;
          break;
        case 'i':
          input = optarg;
	  break ;
	case 't':
	  printf("testcase\n");
	  exit(1);
        case '?':
	  usage();
          break;
        }                       //switch
    }  

  if (input != NULL)
  {
    if(strcmp(input,"v4l") == 0) 
    {
      transmitV4l();
    }
    if(strcmp(input,"gphoto") == 0) 
    {
      transmitGphoto(loop);
    }
    if(strcmp(input,"display") == 0)
    {
      transmitDisplay();
    }
  }

  if(guiEnabled)
  {
    guiInit();
  }
#ifdef RTSP
  if(rtsp){ 
/* rtsp server */
     if(server){startServer();}
     if(!server){startClient();}
  }
#endif  
  if(!rtsp){
/* crap server */
     if(server && numPipes>0)
     {
       launchPipeplines(numPipes);
       g_main_loop_run(loop);      
     }
     else {rtpbinTransmitter(videotestsrc,NULL);}
     g_main_loop_run(loop);
  }
  return 0;
}
