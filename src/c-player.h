  extern int 
player_reset_and_play (); 
extern void player_init_media(PlayerData *);
extern void player_create_gui(PlayerData *);
extern int player_refresh_ui(PlayerData *);
extern void player_cleanup(PlayerData *);
extern void player_load_uri(char *, PlayerData *);
