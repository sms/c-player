/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


// don't define it here user gcc -DGST1
//#define GST1

// asprintf,getopt is gnu
//#define _GNU_SOURCE
// for getopt
//#define _XOPEN_SOURCE
#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <regex.h>

// switch for strings
#include "switchs.h"

// sockets
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>


//glib stuff
#include <glib-2.0/glib.h>
#include <glib-2.0/glib/gprintf.h>
#include <glib-unix.h>

//gtk stuff

#ifdef GTK
#ifndef CLI
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#if defined (GDK_WINDOWING_X11)
#include <gdk/gdkx.h>
#elif defined (GDK_WINDOWING_WIN32)
#include <gdk/gdkwin32.h>
#elif defined (GDK_WINDOWING_QUARTZ)
#include <gdk/gdkquartz.h>
#endif
#endif
#endif
//gstreamer stuff

#define GST_DISABLE_DEPRECATED

#ifdef GST1
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>
#else
#include <gst/gst.h>
#include <gst/interfaces/xoverlay.h>
#include <gst/pbutils/pbutils.h>
#endif
// some macros

#define asprintf(...) if (asprintf (__VA_ARGS__) == -1) printf("asprintf failed\n");


#include "wraps.h"

#define gst_element_factory_make(element, name) gst_element_factory_make_debug (element, name);

enum {
  AUTO,
  JACK,
  SNOWMIX,
  FAKE,
  RTMP
};

typedef struct snowMixer snowMixer;
struct snowMixer {
  //general
  char *host; //host
  char  *port; //port
  char *control_socket; //ctrsocket
  char *state; //snowmix
  //video
  int width; //system_width
  int height; //system_height
  char *ratefraction; //ratefraction
  //audio
  int audio_rate; //rate
  int audio_channels; //channels
};

typedef struct snowFeed snowFeed;
struct snowFeed {
  //general
  int id;
  char *control_pipe; //feed_control_pipe
  // video
  int width; //feed_width
  int height; //feed_height
  //framerate == mixer framerate 
  // audio
  //  int audio_backend; // jack??
  int audio_channels; //feed_channels
  int audio_rate;
};

typedef struct snowData snowData;
struct snowData {
  snowFeed *currentFeed;
  snowMixer *mixer;
  int audiosink;
};


typedef struct PlayerGui PlayerGui;
struct PlayerGui {
#ifdef GTK
  GtkWidget *feed_label;
  GtkWidget *clock_label;
  char *markup;
  GtkWidget *video_window, *main_hbox;
//
//
  GtkWidget *main_window;	/* The uppermost window, containing all other windows */


  GtkWidget *main_box;		/* VBox to hold main_hbox and the controls */
  //  GtkWidget *main_hbox;		/* HBox to hold the video_window and the stream info text widget */
  GtkWidget *controls;		/* HBox to hold the buttons and the slider */
  GtkWidget *options;		/* HBox to hold the options and current uri */
  GtkWidget *play_button, *pause_button, *stop_button, *open_button, *add_button;	/* Buttons */
  GtkWidget *test_button, *previous_button, *next_button;
  GtkWidget *label_time;
GtkWidget *loaded_label;
#endif
};


/* Structure to contain all our information, so we can pass it around */
typedef struct PlayerData PlayerData;
struct PlayerData{
  // things that used to be global vars
  char *playuri; 
  int feedid;
  int sink;
  int preview;
  //mainloop
  GMainLoop *loop;
  //gstreamer
  GstElement *playbin2;		/* Our one and only pipeline */
  GstDiscoverer *discoverer;
  GstState state;		/* Current state of the pipeline */
  gint64 duration;		/* Duration of the clip, in nanoseconds */
  GstElement *valve;
 
//gtk
#ifdef GTK
  PlayerGui *gui;
  GtkWidget *slider;		/* Slider widget to keep track of current position */
  GtkWidget *streams_list;	/* Text widget to display info about the streams */
  gulong slider_update_signal_id;	/* Signal ID for the slider update signal */
  GtkWidget *preview_check;
#endif
 snowData *snow;
};

//CustomData globalData;

PlayerData *pData;
//CustomData *data;

// depreciated globall vars 
#ifdef GTK
GtkWidget *playlist_view;
GtkWidget *playlist_box;
#endif
gboolean playabool;


