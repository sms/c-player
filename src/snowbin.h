/* [name], A player for snowmix
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//extern GstBin *get_audiobin ();

extern GstBin *get_snowvideobin (snowData *);
extern GstBin *get_snowaudiobin (snowData *);
extern snowData  *snowbin_init_snowData (PlayerData *);
extern void snowbin_get_vars(PlayerData *);
extern void print_snowFeed(snowFeed *);
extern void print_snowMixer(snowMixer *);
