#include "common.h"
#include "c-player.h"


#ifdef PL
#ifdef GTK
#include "playlist.h"
#endif
#endif

#ifdef GTK
// create gui that has to contain everything

GtkWidget *main_window;

/* This function is called when the main window is closed */
  static void
delete_event_cb (GtkWidget * widget, GdkEvent * event, PlayerData * data)
{
  //stop_cb (NULL, data);
  player_cleanup (data);
}



static void create_main_window()
{



  main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (G_OBJECT (main_window), "delete-event",
      G_CALLBACK (delete_event_cb), pData);

// init player
  player_create_ui (pData);
  

  gtk_container_add (GTK_CONTAINER (main_window), pData->gui->main_box);

  gtk_window_set_default_size (GTK_WINDOW (main_window), 640, 480);

  gtk_widget_show_all (main_window);
  gtk_widget_hide(pData->gui->main_hbox);



}


#endif

static int signal_handler(PlayerData * data) {

    printf(" \n Signal caught: %u \n ", 10);
    player_cleanup(data);
}

  void print_version()
  {
    printf("This is the version: ???");
    exit(0);
  }



  void opthelp(char * argv_blaat)
  {
    printf("This is the help");
    printf
      (" \n -a audiosink [snowmix|jack|auto|fake]  \n -i playuri \n -f feedid \n -s sink (not working) \n");
    exit(0);
  }

  int
main (int argc, char *argv[])
{
  /* deal with command line arguments */
  // feed, filename, 
  auto int opt = 0;
//  auto int feedid = 1;
  auto int audiosink = FAKE;
 g_unix_signal_add(SIGUSR1, (GSourceFunc)signal_handler, pData);
 g_unix_signal_add(SIGTERM, (GSourceFunc)signal_handler, pData);
 g_unix_signal_add(SIGINT, (GSourceFunc)signal_handler, pData);


  /* Initialize our global data structures */
  pData = (PlayerData*)malloc(sizeof(struct PlayerData));

#ifdef SNOWBIN
  snowbin_init_snowData(pData);
#endif



  // stuff for getopts long
  static const struct option opts[] = {
    {"version",   no_argument,    0, 'v'},
    {"help",   no_argument,    0, 'h'},
    {"uri",   required_argument,    0, 'i'},
    {"feed",      required_argument,    0, 'f'},
    {"sink", required_argument, 0, 's'},
    {"audiosink", required_argument, 0, 'a'},
    {0,      0,                   0,  0 }   /* Sentiel */
  };
  int optidx;
  char c;

  /* <option> and a ':' means it's marked as required_argument, make sure to do that.
   * or optional_argument if it's optional.
   * You can pass NULL as the last argument if it's not needed.  */
  while ((c = getopt_long(argc, argv, "vhi:f:s:a:u", opts, &optidx)) != -1) {
    switch (c) {
      case 'v': print_version(); break;
      case 'h': opthelp(argv[0]); break;
                // this only gets loaded without playlist
#ifndef PL
      case 'i':
                pData->playuri = optarg;
                break;
#endif
      case 'f':
                pData->feedid = atoi(optarg);
                break;
      case 's':
                printf("case s \n");
 
                if (strncmp(optarg,"snowmix", 4) == 0)
                {
                  pData->sink = SNOWMIX;

                } else if (strncmp(optarg,"rtmp", 4) == 0)
                {
                  pData->sink = RTMP;
                } else if (strncmp(optarg,"fake", 4) == 0)
                {
                  pData->sink = FAKE;
                }
                break;
#ifdef SNOWBIN
      case 'a':
                printf("case a \n");
                if (strncmp(optarg,"snowmix", 4) == 0)
                {
                  printf("a = snowmix \n");
                  pData->snow->audiosink = SNOWMIX;

                } else if (strncmp(optarg,"jack", 4) == 0)
                {
                  pData->snow->audiosink = JACK;
                } else if (strncmp(optarg,"auto", 4) == 0)
                {
                  pData->snow->audiosink = AUTO;
                } else if (strncmp(optarg,"fake", 4) == 0)
                {
                  pData->snow->audiosink = FAKE;
                }
                break;
#endif


            case '?': opthelp(argv[0]); return 1;                /* getopt already thrown an error */
            default:
                if (optopt == 'a' | optopt == 'f' | optopt == 's' | optopt == 'i')
                    fprintf(stderr, "Option -%c requires an argument.\n",
                        optopt);
                    
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option -%c.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character '\\x%x'.\n",
                        optopt);
               return 1;
        }
    }
    /* Loop through other arguments ("leftovers").  */
    while (optind < argc) {
        /* whatever */;
        ++optind;
    }

#ifdef SNOWBIN
    snowbin_get_vars(pData);
#endif
#ifdef GTK
#ifdef TT
   if( ! g_thread_supported() )
    g_thread_init( NULL );
    gdk_threads_init();
    gdk_threads_enter();
#endif

  /* Initialize GTK */
  gtk_init (&argc, &argv);
#endif

/* initialize player */
player_init_media(pData);



/* Create the GUI */
#ifdef GTK

create_main_window();
#endif



#ifndef PL
 //load url
 player_load_uri(pData->playuri, pData);
  gst_element_set_state (pData->playbin2, GST_STATE_PLAYING);

#endif

  /* Register a function that GLib will call every second */
#ifdef GTK
  g_timeout_add_seconds (1, (GSourceFunc)player_refresh_ui, pData);
  /* register a function that will check all items in the playlist for availablitity */
#ifdef PL
  g_timeout_add_seconds (15, (GSourceFunc) playlist_refresh, pData);
#endif

#endif

  gst_element_set_state (pData->playbin2, GST_STATE_PLAYING);

  /* Start the GTK main loop. We will not regain control until gtk_main_quit is called. */
#ifdef GTK
  gtk_main ();

#ifdef TT
  gdk_threads_leave();
#endif
#else
//  printf("using this without gtk is on the todo list");
  pData->loop = g_main_loop_new (NULL, FALSE);
  g_print ("Running...\n");
  g_main_loop_run (pData->loop);


#endif

//  cleanup();
  return 0;
}
