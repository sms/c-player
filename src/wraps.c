
#ifdef GST1 
#include <gstreamer-1.0/gst/gst.h>
#else
#include <gstreamer-0.10/gst/gst.h>
#endif


#include <strings.h>
#include <stdio.h>


extern void 
e_exit (char * msg)
{
  printf("EXITING: %s \n", msg);
  exit(10);
}

extern GstElement *
gst_element_factory_make_debug( gchar *factoryname, gchar *name)
{
  GstElement *element;

  element = gst_element_factory_make(factoryname,name);

  if (element == NULL) {

    printf ("\n Failed to create element - type: %s name: %s \n", factoryname, name);
    e_exit(factoryname);
    return element;
  } else {
    return element;
  }
}


//dump_dot()
//{
   /*
    *   GST_DEBUG_GRAPH_SHOW_MEDIA_TYPE             show caps-name on edges
    *   GST_DEBUG_GRAPH_SHOW_CAPS_DETAILS           show caps-details on edges
    *   GST_DEBUG_GRAPH_SHOW_NON_DEFAULT_PARAMS     show modified parameters on elements
    *   GST_DEBUG_GRAPH_SHOW_STATES                 show element states
    *   GST_DEBUG_GRAPH_SHOW_ALL                    show all details
    */ 


//}
//#define GST_DEBUG_BIN_TO_DOT_FILE(bin, details, file_name) _gst_debug_bin_to_dot_file (bin, details, file_name)
//#define GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS(bin, details, file_name) _gst_debug_bin_to_dot_file_with_ts (bin, details, file_name)
