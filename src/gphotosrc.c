/* gphotosrc, gstreamer source element that takes input from gphoto
 * source file
 *
 
* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Author yids */ 

// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>

// std libs
#include <stdio.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

// gphoto libs
#include <gphoto2/gphoto2.h>

extern GstElement *
get_gphoto_src()
{
  printf("set gphoto\n");

  char gphotoPath[] = "/usr/bin/";
  char gphotoCmd[] = "gphoto2";
  const char gphotoArgs1[] = "--stdout";
  const char gphotoArgs2[] = "--capture-movie";

  GstElement *gphotobin, *gphotoPipeline;
  GstElement *fdsrc;

  GstElement *jpegdec;
//  GstElement *vsink, *asink;
//  GstElement *appsrc;
  GstElement *vqueue;
  GstElement *capsfilter;
  GstCaps    *caps; 
//  gphotoPipeline = gst_pipeline_new("gphotopipe");
 
  gphotobin = gst_bin_new("GPhoto");
  vqueue    = gst_element_factory_make("queue","vqueue");
  fdsrc     = gst_element_factory_make("fdsrc","fdsrc");
  jpegdec   = gst_element_factory_make("jpegdec","jpegdec");
  capsfilter = gst_element_factory_make ("capsfilter", "capsfilter"); 

  caps= gst_caps_new_simple("image/jpeg", 
			    "parsed",G_TYPE_BOOLEAN,"true", 
			    "format",G_TYPE_STRING,"UYVY", 
			    "width",G_TYPE_INT,640, 
			    "height",G_TYPE_INT,480, 
	 	  	    "framerate",GST_TYPE_FRACTION,1,1,
			    NULL);

  g_object_set (G_OBJECT (capsfilter), "caps", caps, NULL); 
 
//  g_object_set(G_OBJECT(jpegdec),"idct-method",1,NULL);

  int filedes[2];
  if (pipe(filedes) == -1) 
  {
    perror("pipe");
    exit(1);
  }

  pid_t pid = fork();
  if (pid == -1) 
  {
    perror("fork");
    //exit(1);
  } 
  else if (pid == 0) 
  {
    while ((dup2(filedes[1], STDOUT_FILENO) == -1) && (errno == EINTR)) {}
    close(filedes[1]);
    close(filedes[0]);
    printf("executing gphoto\n");
    execl("/usr/bin/gphoto2", "gphoto2", "--stdout", "--capture-movie", (char*)0);
    perror("execl");
    //_exit(1);
  }
  
  close(filedes[1]);

  printf("fd: %i\n", filedes[0]);

  g_object_set( G_OBJECT (fdsrc), "fd", filedes[0], NULL);
/*
  gst_bin_add_many(GST_BIN(gphotoPipeline), fdsrc, jpegdec,appsrc, NULL);
  if( !gst_element_link_many(fdsrc,jpegdec,appsrc,NULL)) { printf ("failed to link gphoto shit\n");}
  gst_element_set_state ( gphotoPipeline, GST_STATE_PLAYING );
  g_main_loop_run(loop);
*/
  gst_bin_add_many(GST_BIN(gphotobin), capsfilter, fdsrc, vqueue, jpegdec, NULL);
  if( !gst_element_link_many(fdsrc, vqueue, jpegdec,NULL)) { printf ("failed to link gphoto shit\n");}
  GstPad *vpad, *vghostpad;
  vpad = gst_element_get_static_pad (jpegdec, "src");
  vghostpad = gst_ghost_pad_new ("src", vpad);

  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (gphotobin, vghostpad);

  return gphotobin;


}
