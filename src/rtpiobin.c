/* rptiobin.c - functions for creating rx and tx rtpbins.
 * 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author yids
 */

//std libs

#include <stdio.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <unistd.h>

// gstreamer stuff
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/pbutils/pbutils.h>
#include <gstreamer-1.0/gst/video/videooverlay.h>

// readline 
#include <readline/readline.h>
#include <readline/history.h>



// pad added to rtpbin callback
static void 
on_pad_added ( GstElement *element, GstPad *pad, gpointer data )
{
  GstPad *sinkpad;
  GstElement* elem = ( GstElement* ) data;
  sinkpad = gst_element_get_static_pad( elem , "sink" );
  if (GST_PAD_IS_SRC(pad)) {gst_pad_link ( pad, sinkpad );}
  gst_object_unref( sinkpad );
}

// rtpbin transmitter
extern GstElement *
get_tx_rtpbin(char *host, int baseport)
{
  printf("tx rtpbin init\n");
  GstElement *tx_rtpbin = gst_bin_new("TX RTP");
  return tx_rtpbin;
}

// rtpbin reviever
extern GstElement *
get_rx_rtpbin(char *host, int baseport)
{
  printf("rx rtpbin init\n");
  GstElement *rx_rtpbin = gst_bin_new("RX RTP");
  GstPad *vpad, *vghostpad;
  GstPad *apad, *aghostpad;

  char *input;
  int port;
  host = "127.0.0.1";
  input = readline("set port (rtp wil use 6 ports)");
  port = atoi(input);
  printf("RTP IN BASEPORT %u\n",port);
  g_free(input);
 
  input = readline("set IP (defaults to 127.0.0.1)");
  if(strncmp(input,"",640) != 0 ) {strncpy(host,input,20);}
  printf("RTP IN IP %s\n",host);
  g_free(input);

  /* create the elements */ 

  GstElement *vudpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp video" );
  GstElement *vudpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp video" );
  GstElement *vudpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp video " );
  GstElement *audpsrc_rtp    = gst_element_factory_make ( "udpsrc", "udpsrc-rtp audio" );
  GstElement *audpsrc_rtcp   = gst_element_factory_make ( "udpsrc", "udpsrc-rtcp audio" );
  GstElement *audpsink_rtcp  = gst_element_factory_make ( "udpsink", "udpsink-rtcp audio " );
  GstElement *rtpbin         = gst_element_factory_make ( "rtpbin", "gstrtpbin" );
  GstElement *rtph264depay   = gst_element_factory_make ( "rtph264depay", "rtph264depay" );
  GstElement *rtpopusdepay   = gst_element_factory_make ( "rtpopusdepay", "rtpopusdepay" );
  GstElement *queue1         = gst_element_factory_make ( "queue2", "queue1" );
  GstElement *queue2         = gst_element_factory_make ( "queue2", "queue2" );
  GstElement *vdecoder       = gst_element_factory_make ( "avdec_h264", "vdecoder" );
  GstElement *adecoder       = gst_element_factory_make ( "opusdec", "adecoder" );
  GstElement *video          = gst_element_factory_make ( "identity", "RTP video");
  GstElement *audio          = gst_element_factory_make ( "identity", "RTP audio" );

  /* set properties */
  g_object_set( G_OBJECT ( vudpsrc_rtp ),   "port", port, NULL );
  g_object_set( G_OBJECT ( vudpsrc_rtcp ),  "port", port+1, NULL );
  g_object_set( G_OBJECT ( vudpsink_rtcp ), "host", host, NULL );
  g_object_set( G_OBJECT ( vudpsink_rtcp ), "port", port+2, NULL );
  g_object_set( G_OBJECT ( audpsrc_rtp ),   "port", port+3, NULL );
  g_object_set( G_OBJECT ( audpsrc_rtcp ),  "port", port+4, NULL );
  g_object_set( G_OBJECT ( audpsink_rtcp ), "host", host, NULL );
  g_object_set( G_OBJECT ( audpsink_rtcp ), "port", port+5, NULL );
  g_object_set( G_OBJECT ( rtpbin ), "autoremove", TRUE, NULL );
  g_object_set( G_OBJECT ( rtpbin ), "do-sync-event", TRUE, NULL ); 
  g_object_set( G_OBJECT ( rtpbin ), "async-handling", TRUE, NULL );

  g_object_set( G_OBJECT ( audpsink_rtcp ), "sync", FALSE, NULL);
  g_object_set( G_OBJECT ( vudpsink_rtcp ), "sync", FALSE, NULL);
  g_object_set( G_OBJECT ( audpsink_rtcp ), "async", FALSE, NULL);
  g_object_set( G_OBJECT ( vudpsink_rtcp ), "async", FALSE, NULL);



  /* add elements to pipeline */ 
  gst_bin_add_many( GST_BIN( rx_rtpbin ), vudpsrc_rtp, vudpsrc_rtcp, vudpsink_rtcp,
  					   audpsrc_rtp, audpsrc_rtcp, audpsink_rtcp,
	                  		   rtpbin, rtph264depay, rtpopusdepay, 
					   queue1, queue2, vdecoder, adecoder, NULL);

  /* set the caps */
  const gchar *vcaps = "application/x-rtp,media=(string)video,clock-rate=(int)90000,encoding-name=(string)H264,payload=(int)96"; 
  GstCaps *vsrccaps;
  const gchar *acaps = "application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00,payload=(int)96"; 
  GstCaps *asrccaps;

  vsrccaps = gst_caps_from_string( vcaps );
  g_object_set( G_OBJECT ( vudpsrc_rtp ), "caps", GST_CAPS(vsrccaps) , NULL );
  
  asrccaps = gst_caps_from_string( acaps );
  g_object_set( G_OBJECT ( audpsrc_rtp ), "caps", GST_CAPS(asrccaps) , NULL );

  /* link the elements */
  if ( !gst_element_link_pads_filtered( vudpsrc_rtp, "src", 
				        rtpbin, "recv_rtp_sink_%u", 
				        NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}

  if ( !gst_element_link_pads_filtered( vudpsrc_rtcp, "src", 
				        rtpbin, "recv_rtcp_sink_%u", 
					NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}

  if ( !gst_element_link_pads_filtered( rtpbin, "send_rtcp_src_%u", 
				        vudpsink_rtcp, "sink", 
					NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}

  if ( !gst_element_link_pads_filtered( audpsrc_rtp, "src", 
                                        rtpbin, "recv_rtp_sink_%u", 
					NULL )) { printf("failed to link rtp udpsrc to rtpbin\n");}

  if ( !gst_element_link_pads_filtered( audpsrc_rtcp, "src", 
				        rtpbin, "recv_rtcp_sink_%u", 
					NULL )) { printf("failed to link rtcp udpsrc to rtpbin\n");}
  
  if ( !gst_element_link_pads_filtered( rtpbin, "send_rtcp_src_%u", 
  				        audpsink_rtcp, "sink", 
					NULL )) { printf("failed to link rtpbin to rtcp udpsink\n");}
  
  if ( !gst_element_link_many( rtph264depay, 
			       queue1, 
                               vdecoder,  
                                
			       NULL )) { printf("failed to link rtph264depay, queue, vdecoder,  vsink\n");}
  if ( !gst_element_link_many( rtpopusdepay, 
			       queue2, 
                               adecoder, 
                                
			       NULL )) { printf("failed to link rtpopusdepay, queue, adecoder,  asink\n");}

  /* connect callbacks */
  g_signal_connect( rtpbin, "pad-added", G_CALLBACK( on_pad_added ), rtpopusdepay  );
  g_signal_connect( rtpbin, "pad-added", G_CALLBACK( on_pad_added ), rtph264depay  );

  /* create pads for the bin and connect them */
  vpad = gst_element_get_static_pad (vdecoder, "src");
  vghostpad = gst_ghost_pad_new ("vsrc", vpad);
  gst_pad_set_active (vghostpad, TRUE);
  gst_element_add_pad (rx_rtpbin, vghostpad);
  gst_object_unref (vpad);

  apad = gst_element_get_static_pad (adecoder, "src");
  aghostpad = gst_ghost_pad_new ("asrc", apad);
  gst_pad_set_active (aghostpad, TRUE);
  gst_element_add_pad (rx_rtpbin, aghostpad);
  gst_object_unref (apad);

  return rx_rtpbin;
}
 
