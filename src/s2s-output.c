/* s2s-output.c outputs for s2s
 * 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/* ------- output functions -------- */
int output_set_screen()
{
  printf("output set to screen\n");
  GstElement *xvimagesink = gst_element_factory_make("xvimagesink", "Screen");
  GstElement *alsasink = gst_element_factory_make("alsasink", "ALSA");

  g_object_set(G_OBJECT(xvimagesink), "sync", FALSE, NULL);

  mainData[numStreams].videoOutput = xvimagesink;
  mainData[numStreams].audioOutput = alsasink;
  return 0;
}

int output_set_rtp()
{
  printf("output set to rtp\n");
  
  char *input;
  int port;
  char *hostname = "127.0.0.1";
  input = readline("set port (rtp wil use 6 ports)");
  port = atoi(input);
  printf("RTP OUT BASEPORT %u\n",port);
  g_free(input);
 
  input = readline("set IP (defaults to 127.0.0.1)");
  if(strncmp(input,"",640) != 0 ) {hostname = input;}
  printf("RTP OUT IP %s\n",hostname);
  g_free(input);


  outputRtpData[numStreams].streamNumber = numStreams; // set the stream number in the rtpdata structure 
  /* create the elements */
  if(mainData[numStreams].pipeline == NULL)
  {
  mainData[numStreams].pipeline           = gst_pipeline_new( "video-send" );
  }

  mainData[numStreams].videoOutput        = gst_element_factory_make ( "identity", "vsrc");
  mainData[numStreams].audioOutput	  = gst_element_factory_make ( "identity", "asrc");
  outputRtpData[numStreams].vqueue        = gst_element_factory_make ( "queue2", "vqueue" );
  outputRtpData[numStreams].aqueue        = gst_element_factory_make ( "queue2", "aqueue" );
  outputRtpData[numStreams].vencoder      = gst_element_factory_make ( "x264enc", "vencoder" );
  outputRtpData[numStreams].audioresample = gst_element_factory_make ( "audioresample", "audioresample");
  outputRtpData[numStreams].aencoder      = gst_element_factory_make ( "opusenc", "aencoder" );
  outputRtpData[numStreams].rtph264pay    = gst_element_factory_make ( "rtph264pay", "rtph264pay" );
  outputRtpData[numStreams].rtpopuspay    = gst_element_factory_make ( "rtpopuspay", "rtpopuspay" );
  outputRtpData[numStreams].rtpbin	  = gst_element_factory_make ( "rtpbin", "rtpbin" );
  outputRtpData[numStreams].vudpsink_rtp  = gst_element_factory_make ( "udpsink", "vudpsink-send data" );
  outputRtpData[numStreams].vudpsink_rtcp = gst_element_factory_make ( "udpsink", "vudpsink-rtcp" );
  outputRtpData[numStreams].vudpsrc_rtcp  = gst_element_factory_make ( "udpsrc", "vudpsrc-rtcp" );
  outputRtpData[numStreams].audpsink_rtp  = gst_element_factory_make ( "udpsink", "audpsink-send data" );
  outputRtpData[numStreams].audpsink_rtcp = gst_element_factory_make ( "udpsink", "audpsink-rtcp" );
  outputRtpData[numStreams].audpsrc_rtcp  = gst_element_factory_make ( "udpsrc", "audpsrc-rtcp" );

  /* set properties */
  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "bitrate", 1512, NULL);
//  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "byte-stream", FALSE, NULL);
//  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "pass", 5, NULL);
//  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "quantizer", 45, NULL);
  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "speed-preset", 1, NULL);
//  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "tune", 0x00000004, NULL);
//  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "sliced-threads", TRUE, NULL);
//  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "rc-lookahead", 20, NULL);
  g_object_set( G_OBJECT ( outputRtpData[numStreams].vencoder ), "psy-tune", 1, NULL);

  g_object_set( G_OBJECT ( outputRtpData[numStreams].vudpsink_rtp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].vudpsink_rtp ), "port", port, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].vudpsink_rtcp ), "host",hostname, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].vudpsink_rtcp ), "port", port+1, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].vudpsrc_rtcp ), "port", port+2, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].audpsink_rtp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].audpsink_rtp ), "port", port+3, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].audpsink_rtcp ), "host", hostname, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].audpsink_rtcp ), "port", port+4, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].audpsrc_rtcp ), "port", port+5, NULL ); 

  g_object_set( G_OBJECT ( outputRtpData[numStreams].vudpsink_rtcp ), "async", FALSE, "sync", FALSE, NULL );
  g_object_set( G_OBJECT ( outputRtpData[numStreams].audpsink_rtcp ), "async", FALSE, "sync", FALSE, NULL );

  /* add elements to pipeline */
//  gst_bin_add( GST_BIN(mainData[numStreams].pipeline),mainData[numStreams].videoOutput);
//  gst_bin_add( GST_BIN(mainData[numStreams].pipeline),mainData[numStreams].audioOutput);

  gst_bin_add_many( GST_BIN( mainData[numStreams].pipeline ), 
   							   mainData[numStreams].videoOutput, mainData[numStreams].audioOutput, 
							   outputRtpData[numStreams].vqueue, outputRtpData[numStreams].aqueue,
						  	   outputRtpData[numStreams].vencoder, outputRtpData[numStreams].aencoder, 
							   outputRtpData[numStreams].audioresample, outputRtpData[numStreams].rtph264pay, 
							   outputRtpData[numStreams].rtpopuspay, outputRtpData[numStreams].rtpbin, 
						           outputRtpData[numStreams].vudpsink_rtp, outputRtpData[numStreams].vudpsink_rtcp, 
							   outputRtpData[numStreams].vudpsrc_rtcp, outputRtpData[numStreams].audpsink_rtp, 
							   outputRtpData[numStreams].audpsink_rtcp, outputRtpData[numStreams].audpsrc_rtcp, NULL);

  /* link elements */

  if ( !gst_element_link_many (mainData[numStreams].videoOutput, 
			       outputRtpData[numStreams].vqueue, 
			       outputRtpData[numStreams].vencoder, 
			       outputRtpData[numStreams].rtph264pay, 
			       NULL)) {printf("failed to link encoder to h264 payloader\n");}

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].rtph264pay, "src", 
				       outputRtpData[numStreams].rtpbin, "send_rtp_sink_0", 
				       NULL )) {printf("failed to link payloader to rtpbin\n");}

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].rtpbin, "send_rtp_src_0",
				       outputRtpData[numStreams].vudpsink_rtp, "sink", 
				       NULL )) {printf("failed to link rtpbin to rtp udpsink\n");}

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].rtpbin, "send_rtcp_src_0",
				       outputRtpData[numStreams].vudpsink_rtcp,"sink", 
				       NULL )) {printf("failed to link rtpbin to rtcp udpsink\n");}

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].vudpsrc_rtcp, "src",
				       outputRtpData[numStreams].rtpbin, "recv_rtcp_sink_0",
				       NULL )) {printf("failed to link rtcp udpsrc to rtpbin\n");}
  
  if ( !gst_element_link_many (mainData[numStreams].audioOutput, 
			       outputRtpData[numStreams].audioresample,
			       outputRtpData[numStreams].aencoder, 
			       outputRtpData[numStreams].rtpopuspay, 
 			       NULL)) {printf("failed to link encoder to opus payloader");} 

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].rtpopuspay, "src", 
				       outputRtpData[numStreams].rtpbin, "send_rtp_sink_1", 
				       NULL )) {printf("failed to link payloader to rtpbin\n");}

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].rtpbin, "send_rtp_src_1", 
				       outputRtpData[numStreams].audpsink_rtp, "sink", 
				       NULL )) {printf("failed to link rtpbin to rtp udpsink\n");}

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].rtpbin, "send_rtcp_src_1",
				       outputRtpData[numStreams].audpsink_rtcp,"sink",
				        NULL )) {printf("failed to link rtpbin to rtcp udpsink\n");}

  if ( !gst_element_link_pads_filtered(outputRtpData[numStreams].audpsrc_rtcp, "src", 
				       outputRtpData[numStreams].rtpbin, "recv_rtcp_sink_1", 
				       NULL )) {printf("failed to link rtcp udpsrc to rtpbin\n");}


  return 0;
} 

int output_set_snowmix()
{
  printf("output set to snowmix\n");
  char tmp[6];
  char *input;
 
  input = readline("set feed number");
//  GstElement *audiotestsrc = gst_element_factory_make("audiotestsrc", "audiotestsrc");
  sprintf(tmp, "feed%s", input);
  printf("IN MAIN control pipe: %s\n",tmp);
  mainData[numStreams].videoOutput = GST_ELEMENT(get_snowvideobin(tmp));
  g_free(input);
//  g_free(tmp);
//  mainData[numStreams].audioOutput = audiotestsrc;
  return 0;
}

int output_set_alsa()
{
  printf("output set to alsa\n");
  GstElement *alsasink = gst_element_factory_make("alsasink","ALSA");
  mainData[numStreams].audioOutput = alsasink;
  return 0;
}

int output_set_jack()
{
  printf("output set to jack\n");
  GstElement *jacksink = gst_element_factory_make("jackaudiosink", "jackaudiosink");
  mainData[numStreams].audioOutput = jacksink;
  return 0;
}

int output_set_rtmp()
{
  printf("output set to rtmp\n");

  char *url;

  url = readline("rtmp url\n");

  printf("rtmp url set to %s\n",url);
  g_free(url);
 
  if(mainData[numStreams].pipeline == NULL)
  {
  mainData[numStreams].pipeline           = gst_pipeline_new( "video-send" );
  }

  mainData[numStreams].videoOutput        = gst_element_factory_make ( "identity", "vsrc");
  mainData[numStreams].audioOutput	  = gst_element_factory_make ( "identity", "asrc");
  outputRtmpData[numStreams].vqueue        = gst_element_factory_make ( "queue2", "vqueue" );
  outputRtmpData[numStreams].aqueue        = gst_element_factory_make ( "queue2", "aqueue" );
  outputRtmpData[numStreams].vencoder      = gst_element_factory_make ( "x264enc", "vencoder" );
  outputRtmpData[numStreams].audioresample = gst_element_factory_make ( "audioresample", "audioresample");
  outputRtmpData[numStreams].aencoder      = gst_element_factory_make ( "lamemp3enc", "aencoder" );
  outputRtmpData[numStreams].flvmux	   = gst_element_factory_make ( "flvmux", "flvmux" ); 
  outputRtmpData[numStreams].rtmpsink	   = gst_element_factory_make ( "rtmpsink", "rtmpsink");

  g_object_set( G_OBJECT(outputRtmpData[numStreams].rtmpsink), "location", "rtmp://10.205.12.70:1935/input/test", NULL);

  gst_bin_add_many( GST_BIN( mainData[numStreams].pipeline ),   mainData[numStreams].videoOutput,
							        mainData[numStreams].audioOutput,
								outputRtmpData[numStreams].vqueue,
								outputRtmpData[numStreams].aqueue,
								outputRtmpData[numStreams].vencoder,
								outputRtmpData[numStreams].audioresample,
								outputRtmpData[numStreams].aencoder,
								outputRtmpData[numStreams].flvmux,
								outputRtmpData[numStreams].rtmpsink, NULL);
  GstPad *vencoderPad;
  GstPad *flvmuxVideoPad;

  vencoderPad = gst_element_get_static_pad(outputRtmpData[numStreams].vencoder, "src");
  flvmuxVideoPad = gst_element_get_request_pad (outputRtmpData[numStreams].flvmux, "video");

  gst_pad_link(vencoderPad, flvmuxVideoPad);
  gst_element_link_many( mainData[numStreams].videoOutput,
                         outputRtmpData[numStreams].vqueue,
                         outputRtmpData[numStreams].vencoder, NULL);


  GstPad *aencoderPad;
  GstPad *flvmuxAudioPad;

  aencoderPad = gst_element_get_static_pad(outputRtmpData[numStreams].aencoder, "src");
  flvmuxAudioPad = gst_element_get_request_pad (outputRtmpData[numStreams].flvmux, "audio");

  gst_pad_link(aencoderPad, flvmuxAudioPad);
  gst_element_link_many( mainData[numStreams].audioOutput,
                         outputRtmpData[numStreams].aqueue,
                         outputRtmpData[numStreams].aencoder, NULL);

  gst_element_link(outputRtmpData[numStreams].flvmux, outputRtmpData[numStreams].rtmpsink);

  return 0;
}

//************************* RTSP SERVER CRAP, NEEDS REARANGING ***************************************/
#ifdef RTSP
typedef struct
{
  gboolean white;
  GstClockTime timestamp;
} MyContext;

/* called when we need to give data to appsrc */
static void
need_data (GstElement * appsrc, guint unused, MyContext * ctx)
{
  GstBuffer *buffer;
  guint size;
  GstFlowReturn ret;

  size = 385 * 288 * 2;

  buffer = gst_buffer_new_allocate (NULL, size, NULL);

  /* this makes the image black/white */
  gst_buffer_memset (buffer, 0, ctx->white ? 0xff : 0x0, size);

  ctx->white = !ctx->white;

  /* increment the timestamp every 1/2 second */
  GST_BUFFER_PTS (buffer) = ctx->timestamp;
  GST_BUFFER_DURATION (buffer) = gst_util_uint64_scale_int (1, GST_SECOND, 2);
  ctx->timestamp += GST_BUFFER_DURATION (buffer);

  g_signal_emit_by_name (appsrc, "push-buffer", buffer, &ret);
}

/* called when a new media pipeline is constructed. We can query the
 * pipeline and configure our appsrc */
static void
media_configure (GstRTSPMediaFactory * factory, GstRTSPMedia * media,
    gpointer user_data)
{
  GstElement *element, *appsrc;
  MyContext *ctx;

  /* get the element used for providing the streams of the media */
  element = gst_rtsp_media_get_element (media);

  /* get our appsrc, we named it 'mysrc' with the name property */
  appsrc = gst_bin_get_by_name_recurse_up (GST_BIN (element), "mysrc");

  /* this instructs appsrc that we will be dealing with timed buffer */
  gst_util_set_object_arg (G_OBJECT (appsrc), "format", "time");
  /* configure the caps of the video */
  g_object_set (G_OBJECT (appsrc), "caps",
      gst_caps_new_simple ("video/x-raw",
          "format", G_TYPE_STRING, "RGB16",
          "width", G_TYPE_INT, 384,
          "height", G_TYPE_INT, 288,
          "framerate", GST_TYPE_FRACTION, 0, 1, NULL), NULL);

  ctx = g_new0 (MyContext, 1);
  ctx->white = FALSE;
  ctx->timestamp = 0;
  /* make sure ther datais freed when the media is gone */
  g_object_set_data_full (G_OBJECT (media), "my-extra-data", ctx,
      (GDestroyNotify) g_free);

  /* install the callback that will be called when a buffer is needed */
  g_signal_connect (appsrc, "need-data", (GCallback) need_data, ctx);
  gst_object_unref (appsrc);
  gst_object_unref (element);
}

int output_set_rtsp()
{
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMountPoints *mounts;
  GstRTSPMediaFactory *factory;



  loop = g_main_loop_new (NULL, FALSE);

  /* create a server instance */
  server = gst_rtsp_server_new ();

  /* get the mount points for this server, every server has a default object
   * that be used to map uri mount points to media factories */
  mounts = gst_rtsp_server_get_mount_points (server);

  /* make a media factory for a test stream. The default media factory can use
   * gst-launch syntax to create pipelines.
   * any launch line works as long as it contains elements named pay%d. Each
   * element with pay%d names will be a stream */
  factory = gst_rtsp_media_factory_new ();
  gst_rtsp_media_factory_set_launch (factory,
      "( appsrc name=mysrc ! videoconvert ! x264enc ! rtph264pay name=pay0 pt=96 )");

  /* notify when our media is ready, This is called whenever someone asks for
   * the media and a new pipeline with our appsrc is created */
  g_signal_connect (factory, "media-configure", (GCallback) media_configure,
      NULL);

  /* attach the test factory to the /test url */
  gst_rtsp_mount_points_add_factory (mounts, "/test", factory);

  /* don't need the ref to the mounts anymore */
  g_object_unref (mounts);

  /* attach the server to the default maincontext */
  gst_rtsp_server_attach (server, NULL);

  /* start serving */
  g_print ("stream ready at rtsp://127.0.0.1:8554/test\n");
  g_main_loop_run (loop);

  return 0;

}
#endif

int output_set_icecast()
{
  printf("set output to icecast\n");

  return 0;
}
